/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(vrevc, FixVREVC)

#else

#ifndef LMP_FIX_VREVC_H
#define LMP_FIX_VREVC_H

#include "fix.h"

namespace LAMMPS_NS {

class FixVREVC : public Fix {
public:
  FixVREVC(class LAMMPS *, int, char **);
  ~FixVREVC();
  int setmask();
  void init();
  class NeighList *list;
  void init_list(int, class NeighList *);

  void pre_exchange();
  void pre_force(int);
  void setup_pre_force(int);
  void end_of_step();

  double memory_usage();
  double init_one(int, int);
  int maxsize_restart();
  int size_restart(int);
  void write_restart(FILE *);
  void restart(char *);

private:
  void allocate();
  void allocate_bond();
  void from_tag_to_bonds();

  /*! @brief resolve excluded volume constraints
   */
  void resolve_excluded_volume_constraints(int);
  /*! @brief resolve excluded volume constraints (OpenMP)
   */
  void resolve_excluded_volume_constraints_omp(int);
  /*! @brief minimal distance between two bonds (with KKT conditions)
   */
  void getMinDist_KKT(double **&, double &, double &, double &, double &,
                      double &, int &, int &, int &, int &);
  /*! @brief sort overlap index (binary insertion sort)
   */
  void sort_overlap_index(int noverlaps, int P, unsigned int *&ioverlaps,
                          int *&loverlaps, bool bsort);
  /*! @brief "deja_vu" collisions
   */
  bool err_a_b(double a, double b, double tol);
  void deja_vu_collisions(int *&loverlaps, double *&od1, double *&od2,
                          bool *&deja_vu, int &noverlaps);
  /*! @brief build the relative velocities from collisions
   */
  void build_U_collisions(int noverlaps, int *&loverlaps, double *&od1,
                          double *&od2, double **&v, double *&nx12,
                          double *&ny12, double *&nz12, double *&Dv,
                          double *&Dv0);
  /*! @brief build collision(s) graph(s)
   */
  int build_collisions_graphs(int noverlaps, int *&loverlaps, double *&od1,
                              double *&od2, int *&pgraph, int *&ligraph);
  /*! @brief resize igraph ?
   */
  void resize_igraph(int lgraph, int *&ligraph, int &size_igraph, int *&igraph,
                     int &operg);
  /*! @brief index of the overlap(s) for each collision graph
   */
  void graph_to_overlap(int lgraph, int noverlaps, int operg, int *&loverlaps,
                        double *&od1, double *&od2, int *&pgraph, int *&igraph,
                        int *&ligraph);
  /*! @brief add overlaps to the constraint matrix J
   */
  void Jo_linear_chain_velocity_based(int noverlaps, int *&loverlaps,
                                      double *&od1, double *&od2, double *&nx12,
                                      double *&ny12, double *&nz12,
                                      double *&Jcoll, int *&Jcoll_nzero, int C);
  /*! @brief for each overlap get the index of each overlap belonging to the
   * same graph
   */
  void next_overlaps(int lgraph, int operg, int *&igraph, int *&ligraph,
                     int *&next_constraint, int *&lnext, int nperc, int C);
  /*! @brief build J*M^-1*J^T for articulated system + collisions
   */
  void build_JinvMJT(int *&next_constraint, int *&lnext, int nperc,
                     double *&Jcoll, int *&Jcoll_nzero, double *&JJT,
                     int *&JJT_cnzero, int *&lJJT_nzero, double *&Lcoll,
                     double *&Bcoll, int C, int noverlaps, double m_t);
  /*! @brief guess solution of J*M^-1*J^T*L=B
   */
  void guess_solution(const int *next_constraint, const int *lnext, int nperc,
                      const double *JJT, const int *lJJT_nzero, double *&Lcoll,
                      const double *bs, int C, int noverlaps);
  /*! @brief add collisions to the rhs of J*M^-1*J^T*L=B
   */
  void add_collisions_to_bs(int noverlaps, int *&loverlaps, double *&od1,
                            double *&od2, double **&v, double *&Dv,
                            double *&nx12, double *&ny12, double *&nz12,
                            double inv_dt, double *&Bcoll, int *&sLcoll, int C);
  /*! @brief Successive-Over-Relaxation for J*M^-1*J^T*L=B (such that L>=0 for
   * collisions)
   */
  int SOR_JinvMJT(double *&JJT, int *&JJT_cnzero, int *&lJJT_nzero,
                  double *&Lcoll, int *&sLcoll, double *&Bcoll, int nperc,
                  int noverlaps, int C, double w_sor, int n_sor);
  /*! @brief Successive-Over-Relaxation for J*M^-1*J^T*L=B (such that L>=0 for
   * collisions)
   */
  int gSOR_JinvMJT(double *&JJT, int *&JJT_cnzero, int *&lJJT_nzero,
                   double *&Lcoll, int *&sLcoll, double *&Bcoll, int nperc,
                   int noverlaps, double wsor, int nsor, int lgraph,
                   const int *igraph, const int *ligraph, int operg);
  /*! @brief get the articulated system + collisions feedback forces
   */
  void JTlambda(double *&Lcoll, double *&Jcoll, int *&Jcoll_nzero, double *&JTl,
                int noverlaps, int C);
  /*! @brief add J^T*lambda to velocities
   */
  void add_JTl_to_velocities(double dt, double mass, double **&v, double *&JTl);
  /*! @brief add J^T*lambda to forces
   */
  void add_JTl_to_forces(int P, double **&f, double *&JTl);
  /*! @brief check relative velocity from overlaps
   */
  void check_relative_velocity_collisions(
      int noverlaps, int *&loverlaps, double *&od1, double *&od2, double *&nx12,
      double *&ny12, double *&nz12, double **&v, double *&Dv, double *&Dv0,
      double rel_tol, double rel_tol_change, double rel_tol0, bool &b0,
      bool &b1, bool &b2, bool &b3);
  /*! @brief clear graphs
   */
  void clear_igraph(int lgraph, int *&ligraph, int *&igraph, int c_per_graph);
  /*! @brief clear J*invM*J^T
   */
  void clear_JinvMJT(double *&JJT, int *&JJT_cnzero, int *&lJJT_nzero,
                     int *&lnext, int nperc, int noverlaps, int C);
  /*! @brief clear arrays used by the overlaps
   */
  void clear_overlaps();

  int vrevc_irequest;
  int noverlaps;
  int *loverlaps;
  int *overlap01;
  unsigned int *ioverlaps;
  int *segment0; // list of overlapping bonds
  int *segment1; // list of overlapping bonds
  int **obeads0; // list of overlapping beads
  int **obeads1; // list of overlapping beads
  int **compute_ij, ncompute_ij = 0, mcompute_ij = 1000;
  double **store_ij;
  int *next_bond;
  int *previous_bond;
  double *od1;
  double *od2;
  double *omd;
  double *nx12;
  double *ny12;
  double *nz12;
  double *Dv;
  double *Dv0;
  bool *deja_vu;
  double *Jcoll;
  int *Jcoll_nzero;
  int *next_constraint;
  int *lnext;
  double *Bcoll;
  double *Lcoll;
  int *sLcoll;
  double *JJT;
  int *JJTc_nzero;
  int *lJJT_nzero;
  double *JTl;
  int *ligraph;
  int *pgraph;
  int *igraph;
  int idebug = 0;

  double *r0;
  double **vrevc;
  double bcut;
  double oexit = 0.25;
  double psor = 1e-6;
  double vtol = 1e-3;
  double maxl = 0.0;
  class Fix *f_vrevc;
  char *fix_id;
  int maxnall = 1, maxbond = 0;
  int s0 = -1;
  int s1 = -1;
  int s00 = -1;
  int s01 = -1;
  int s10 = -1;
  int s11 = -1;
  int **bond_to_tags;
  int **tag_to_bonds;
  int *ntag_to_bonds;
  int max_ntag_to_bonds = 4;

  int C = 0; // unused for now
  int nperc = 1;
  int operg = 1;
  int size_igraph = 1;
  int lgraph = 1;
  int MO = 100;          // max number of overlaps
  int Mo = 1;            // unused for now
  int isor, nsor = 1000; // number of successive over-relaxation
  double wsor = 1.3;     // over-relaxation rate
  double rbond;

  class RanMars *random;

protected:
  inline int sbmask(int j) const { return j >> SBBITS & 3; }
};
} // namespace LAMMPS_NS

#endif
#endif

    /* ERROR/WARNING messages:

    */
