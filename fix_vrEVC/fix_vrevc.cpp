/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
   ---------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Pascal Carrivain (ENS Lyon)
   ---------------------------------------------------------------------- */

#include "fix_vrevc.h"
#include "atom.h"
#include "atom_vec.h"
#include "comm.h"
#include "domain.h"
#include "error.h"
#include "force.h"
#include "memory.h"
#include "modify.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "neighbor.h"
#include "random_mars.h"
#include "update.h"
#include <cmath>
#include <cstring>
#include <mpi.h>

using namespace LAMMPS_NS;
using namespace FixConst;

#define SMALL 1.0e-10

/* ---------------------------------------------------------------------- */

FixVREVC::FixVREVC(LAMMPS *lmp, int narg, char **arg)
    : Fix(lmp, narg, arg), random(NULL) {
  if (idebug == 1)
    printf("start FixVREVC\n");
  // settings
  force_reneighbor = 1;
  nevery = 1;
  peratom_freq = 1;
  time_integrate = 0;
  create_attribute = 0;
  comm_border = 2;

  // restart settings
  restart_global = 1;
  restart_peratom = 1;
  restart_pbc = 1;

  // per-atom array width 2
  peratom_flag = 1;
  size_peratom_cols = 2;

  // extends pack_exchange()
  atom->add_callback(0);
  atom->add_callback(1); // restart
  atom->add_callback(2);

  if (narg < 17 || narg > 27)
    error->all(FLERR, "Illegal fix command");

  if (atom->tag_enable == 0)
    error->all(FLERR, "fix vrevc requires atom IDs");

  if (!force->newton_bond || !force->newton_pair)
    error->all(FLERR, "fix vrevc requires newton on");

  int iarg = 3;
  while (iarg < narg) {
    if (strcmp(arg[iarg], "wsor") == 0) {
      if (iarg + 2 > narg)
        error->all(FLERR, "Illegal fix vrevc command");
      wsor = atof(arg[iarg + 1]);
      if (wsor < .0 || wsor > 2.)
        error->all(FLERR, "Illegal wsor value for vrevc");
      iarg += 2;
    } else {
      if (strcmp(arg[iarg], "nsor") == 0) {
        if (iarg + 2 > narg)
          error->all(FLERR, "Illegal fix vrevc command");
        nsor = (int)atof(arg[iarg + 1]);
        if (nsor < 0)
          error->all(FLERR, "Illegal nsor value for vrevc");
        iarg += 2;
      } else {
        if (strcmp(arg[iarg], "psor") == 0) {
          if (iarg + 2 > narg)
            error->all(FLERR, "Illegal fix vrevc command");
          psor = atof(arg[iarg + 1]);
          if (psor < 0 || psor > 1.)
            error->all(FLERR, "Illegal psor value for vrevc");
          iarg += 2;
        } else {
          if (strcmp(arg[iarg], "vtol") == 0) {
            if (iarg + 2 > narg)
              error->all(FLERR, "Illegal fix vrevc command");
            vtol = atof(arg[iarg + 1]);
            if (vtol < 0 || vtol > 1.)
              error->all(FLERR, "Illegal vtol value for vrevc");
            iarg += 2;
          } else {
            if (strcmp(arg[iarg], "rbond") == 0) {
              if (iarg + 2 > narg)
                error->all(FLERR, "Illegal fix vrevc command");
              rbond = (double)atof(arg[iarg + 1]);
              if (rbond < .0)
                error->all(FLERR, "Illegal radius value for vrevc");
              iarg += 2;
            } else {
              if (strcmp(arg[iarg], "bcut") == 0) {
                if (iarg + 2 > narg)
                  error->all(FLERR, "Illegal fix vrevc command");
                bcut = (double)atof(arg[iarg + 1]);
                if (bcut < .0)
                  error->all(FLERR, "Illegal bcut value for vrevc");
                iarg += 2;
              } else {
                if (strcmp(arg[iarg], "exit") == 0) {
                  if (iarg + 2 > narg)
                    error->all(FLERR, "Illegal fix vrevc command");
                  oexit = force->numeric(FLERR, arg[iarg + 1]);
                  if (oexit < 0.0 || oexit > 1.0)
                    error->all(FLERR, "Illegal exit value for vrevc, has to be "
                                      ">= 0 and <= 1");
                  iarg += 2;
                } else {
                  if (strcmp(arg[iarg], "debug") == 0) {
                    if (iarg + 2 > narg)
                      error->all(FLERR, "Illegal fix vrevc command");
                    idebug = (int)atof(arg[iarg + 1]);
                    if (idebug != 0 && idebug != 1)
                      error->all(FLERR, "Illegal idebug value for vrevc");
                    iarg += 2;
                  } else {
                    if (strcmp(arg[iarg], "segments") == 0) {
                      if (iarg + 3 > narg)
                        error->all(FLERR, "Illegal fix vrevc command");
                      s0 = (int)atof(arg[iarg + 1]);
                      s1 = (int)atof(arg[iarg + 2]);
                      iarg += 3;
                    } else {
                      if (strcmp(arg[iarg], "beads") == 0) {
                        if (iarg + 5 > narg)
                          error->all(FLERR, "Illegal fix vrevc command");
                        s00 = (int)atof(arg[iarg + 1]);
                        s01 = (int)atof(arg[iarg + 2]);
                        s10 = (int)atof(arg[iarg + 3]);
                        s11 = (int)atof(arg[iarg + 4]);
                        iarg += 5;
                      } else
                        error->all(FLERR, "Illegal fix vrevc command");
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if (bcut > neighbor->cutneighmax) {
    neighbor->cutneighmax = bcut;
    neighbor->cutneighmaxsq = neighbor->cutneighmax * neighbor->cutneighmax;
  }
  // neighbor->binsizeflag = 2;
  // neighbor->binsize_user = 1.2 * bcut;
  f_vrevc = (FixVREVC *)modify->fix[modify->find_fix_by_style("vrevc")];
  // Power method: initialization of the eigenvector
  int seed = 1;
  random = new RanMars(lmp, seed + comm->me);
  if (idebug == 1)
    printf("ok FixVREVC\n");
}

/* -------------------------------------------------------------------------
   free
   ------------------------------------------------------------------------- */
FixVREVC::~FixVREVC() {
  // unregister callbacks to this fix from Atom class
  atom->delete_callback(id, 0);
  atom->delete_callback(id, 1);
  atom->delete_callback(id, 2);
  memory->destroy(Bcoll);
  memory->destroy(bond_to_tags);
  memory->destroy(compute_ij);
  memory->destroy(deja_vu);
  memory->destroy(Dv);
  memory->destroy(Dv0);
  memory->destroy(igraph);
  memory->destroy(ioverlaps);
  memory->destroy(next_bond);
  memory->destroy(previous_bond);
  memory->destroy(tag_to_bonds);
  memory->destroy(ligraph);
  memory->destroy(loverlaps);
  memory->destroy(overlap01);
  memory->destroy(segment0);
  memory->destroy(segment1);
  memory->destroy(obeads0);
  memory->destroy(obeads1);
  memory->destroy(od1);
  memory->destroy(od2);
  memory->destroy(omd);
  memory->destroy(nx12);
  memory->destroy(ny12);
  memory->destroy(nz12);
  memory->destroy(Jcoll);
  memory->destroy(Jcoll_nzero);
  memory->destroy(next_constraint);
  memory->destroy(lnext);
  memory->destroy(Lcoll);
  memory->destroy(sLcoll);
  memory->destroy(JJT);
  memory->destroy(JJTc_nzero);
  memory->destroy(lJJT_nzero);
  memory->destroy(JTl);
  memory->destroy(pgraph);
  memory->destroy(r0);
  memory->destroy(store_ij);
  delete random;
}

/* ---------------------------------------------------------------------- */

int FixVREVC::setmask() {
  // https://lammps.sandia.gov/doc/Modify_fix.html
  int mask = 0;
  mask |= PRE_EXCHANGE;
  mask |= PRE_FORCE;
  // mask |= END_OF_STEP;
  return mask;
}

/* ---------------------------------------------------------------------- */

void FixVREVC::init() {
  // new neighbor request ?
  for (int i = 0; i < modify->nfix; i++)
    if (strncmp(modify->fix[i]->style, "rigid", 5) == 0)
      error->all(FLERR, "fix vrevc is not compatible with rigid fixes.");
  if (neighbor->nrequest == 0)
    error->all(FLERR, "pair vrevc is not defined.");
  vrevc_irequest = neighbor->request(this, instance_me);
  neighbor->requests[vrevc_irequest]->pair = 0;
  neighbor->requests[vrevc_irequest]->fix = 1;
  neighbor->requests[vrevc_irequest]->compute = 0;
  neighbor->requests[vrevc_irequest]->command = 0;
  // neighbor->requests[vrevc_irequest]->neigh      = 1;
  neighbor->requests[vrevc_irequest]->ghost = 0;
  neighbor->requests[vrevc_irequest]->bond = 0;
  neighbor->requests[vrevc_irequest]->half = 1;
  neighbor->requests[vrevc_irequest]->full = 0;
  neighbor->requests[vrevc_irequest]->occasional = 1;
  neighbor->requests[vrevc_irequest]->newton = 1;
  // neighbor->requests[vrevc_irequest]->unique = 1;
  // neighbor->requests[vrevc_irequest]->cut    = 1;
  // neighbor->requests[vrevc_irequest]->cutoff = bcut;
  // printf("fix: %f(%f)+%f, %i
  // %f\n",neighbor->cutneighmax,neighbor->cutneighmin,neighbor->skin,neighbor->binsizeflag,neighbor->binsize_user);

  // allocate
  allocate();
  allocate_bond();
}

void FixVREVC::init_list(int /*id*/, NeighList *ptr) { list = ptr; }

/* -------------------------------------------------------------------------
   allocate all arrays
   ------------------------------------------------------------------------- */
void FixVREVC::allocate() {
  // memory
  int n = atom->ntypes;
  memory->create(r0, n + 1, "fix:r0");
  for (int i = 0; i < n + 1; i++)
    r0[i] = rbond;

  int nall = atom->nlocal + atom->nghost;
  maxnall = nall;

  // bonds
  maxbond = atom->nbonds;
  memory->create(bond_to_tags, maxbond, 2, "fix:bond_to_tags");
  memory->create(next_bond, nall, "fix:next_bond");
  memory->create(previous_bond, nall, "fix:previous_bond");
  memory->create(tag_to_bonds, nall, max_ntag_to_bonds, "fix:tag_to_bonds");
  memory->create(ntag_to_bonds, nall, "fix:ntag_to_bonds");

  // compute or not minimal distance
  memory->create(compute_ij, mcompute_ij, 7, "fix:compute_ij");
  memory->create(store_ij, mcompute_ij, 6, "fix:store_ij");

  // overlaps
  memory->create(igraph, size_igraph, "fix:igraph");
  std::fill_n(igraph, size_igraph, -1);
  memory->create(ligraph, MO + Mo, "fix:ligraph");
  std::fill_n(ligraph, MO + Mo, 0);
  memory->create(loverlaps, MO, "fix:loverlaps");
  memory->create(ioverlaps, MO, "fix:ioverlaps");
  memory->create(overlap01, MO, "fix:overlap01");
  memory->create(segment0, MO, "fix:segment0");
  memory->create(segment1, MO, "fix:segment1");
  memory->create(obeads0, MO, 2, "fix:obeads0");
  memory->create(obeads1, MO, 2, "fix:obeads1");
  memory->create(od1, MO, "fix:od1");
  memory->create(od2, MO, "fix:od2");
  memory->create(omd, MO, "fix:omd");
  memory->create(nx12, MO, "fix:nx12");
  memory->create(ny12, MO, "fix:ny12");
  memory->create(nz12, MO, "fix:nz12");
  memory->create(Dv, MO, "fix:Dv");
  memory->create(Dv0, MO, "fix:Dv0");
  memory->create(deja_vu, MO, "fix:deja_vu");
  memory->create(Jcoll, 12 * (MO + Mo) + 6 * C, "fix:Jcoll");
  std::fill_n(Jcoll, 12 * (MO + Mo) + 6 * C, 0.0);
  memory->create(Jcoll_nzero, 12 * (MO + Mo) + 6 * C, "fix:Jcoll_nzero");
  std::fill_n(Jcoll_nzero, 12 * (MO + Mo) + 6 * C, -1);
  memory->create(next_constraint, (C + MO + Mo) * nperc, "fix:next_constraint");
  memory->create(lnext, C + MO + Mo, "fix:lnext");
  std::fill_n(lnext, C + MO + Mo, 0);
  memory->create(Bcoll, C + MO + Mo, "fix:Bcoll");
  std::fill_n(Bcoll, C + MO + Mo, 0.0);
  memory->create(Lcoll, C + MO + Mo, "fix:Lcoll");
  std::fill_n(Lcoll, C + MO + Mo, .0);
  memory->create(sLcoll, C + MO + Mo, "fix:sLcoll");
  std::fill_n(sLcoll, C + MO + Mo, 0);
  memory->create(JJT, (C + MO + Mo) * nperc, "fix:JJT");
  std::fill_n(JJT, (C + MO + Mo) * nperc, 0.0);
  memory->create(JJTc_nzero, (C + MO + Mo) * nperc, "fix:JJTc_nzero");
  std::fill_n(JJTc_nzero, (C + MO + Mo) * nperc, -1);
  memory->create(lJJT_nzero, C + MO + Mo, "fix:lJJT_nzero");
  std::fill_n(lJJT_nzero, C + MO + Mo, 0);
  memory->create(JTl, 3 * nall, "fix:JTl");
  std::fill_n(JTl, 3 * nall, 0.0);
  memory->create(pgraph, nall, "fix:pgraph");
  std::fill_n(pgraph, nall, -1);
}

/* ----------------------------------------------------------------------
   allocate bonds
   ---------------------------------------------------------------------- */
void FixVREVC::allocate_bond() {
  if (idebug == 1)
    printf("resize ...\n");
  int nbondlist = neighbor->nbondlist;
  int nall = atom->nlocal + atom->nghost;

  if (maxbond < nbondlist) {
    memory->grow(bond_to_tags, nbondlist, 2, "fix:bond_to_tags");
    maxbond = nbondlist;
  }

  // from bond to tags
  for (int b = 0; b < maxbond; b++)
    bond_to_tags[b][0] = bond_to_tags[b][1] = -1;

  if (maxnall < nall) {
    memory->grow(tag_to_bonds, nall, max_ntag_to_bonds, "fix:tag_to_bonds");
    memory->grow(ntag_to_bonds, nall, "fix:ntag_to_bonds");
    memory->grow(next_bond, nall, "fix:next_bond");
    memory->grow(previous_bond, nall, "fix:previous_bond");
    memory->grow(JTl, 3 * nall, "fix_vrevc:JTl");
    memory->grow(pgraph, nall, "fix_vrevc:pgraph");
    for (int p = 0; p < (3 * nall); p++)
      JTl[p] = .0;
    for (int p = 0; p < nall; p++)
      pgraph[p] = -1;
    maxnall = nall;
  }

  for (int p = 0; p < nall; p++) {
    // from tag to bonds
    ntag_to_bonds[p] = 0;
    for (int q = 0; q < max_ntag_to_bonds; q++)
      tag_to_bonds[p][q] = -1;
    // no next/previous bonds for now
    next_bond[p] = previous_bond[p] = -1;
  }
  if (idebug == 1)
    printf("resize done\n");
}

/* ----------------------------------------------------------------------
   from tag to bonds
   ---------------------------------------------------------------------- */
void FixVREVC::from_tag_to_bonds() {
  if (idebug == 1)
    printf("from tag to bonds ...\n");
  double **x = atom->x;
  tagint *tag = atom->tag;
  int **bondlist = neighbor->bondlist;
  int nbondlist = neighbor->nbondlist;
  int i, j, tagi, tagj;
  double dx, dy, dz, new_maxl;

  // allocate memory for bonds
  allocate_bond();

  // tag i and tag j from bond
  new_maxl = 0.0;
  for (int n = 0; n < nbondlist; n++) {
    i = bondlist[n][0];
    j = bondlist[n][1];
    // bond length
    dx = x[i][0] - x[j][0];
    dy = x[i][1] - x[j][1];
    dz = x[i][2] - x[j][2];
    new_maxl = MAX(new_maxl, sqrt(dx * dx + dy * dy + dz * dz));
    // from bond to tags
    tagi = tag[i];
    tagj = tag[j];
    bond_to_tags[n][0] = MIN(tagi, tagj);
    bond_to_tags[n][1] = MAX(tagi, tagj);
    // from tag to bonds
    tag_to_bonds[tagi][ntag_to_bonds[tagi]] = n;
    ntag_to_bonds[tagi]++;
    if (ntag_to_bonds[tagi] > max_ntag_to_bonds)
      error->all(FLERR, "i > max_tag_to_bonds");
    tag_to_bonds[tagj][ntag_to_bonds[tagj]] = n;
    ntag_to_bonds[tagj]++;
    if (ntag_to_bonds[tagj] > max_ntag_to_bonds)
      error->all(FLERR, "j > max_tag_to_bonds");
  }
  // new cutoff if max bond length increased during last time-step
  // cut = bead radius at each bond extremity
  // l   = bond length
  // a   = distance between mid-bond and bead
  // a^2 + l^2 / 4 = (2 * cut)^2
  // a = sqrt(3) * l / 2
  if (new_maxl > maxl) {
    maxl = new_maxl;
    if ((2.0 * rbond) >= (0.5 * sqrt(3.0) * maxl)) {
      neighbor->cutneighmax = 0.5 * maxl + rbond;
      neighbor->cutneighmaxsq = 0.25 * (maxl + rbond) * (maxl + rbond);
    } else {
      neighbor->cutneighmax = 0.5 * maxl;
      neighbor->cutneighmaxsq = 0.25 * maxl * maxl;
    }
  }
  // connectivity
  int B;
  for (int n = 0; n < nbondlist; n++) {
    tagi = bond_to_tags[n][0];
    tagj = bond_to_tags[n][1];
    // first tag
    i = ntag_to_bonds[tagi];
    for (int k = 0; k < i; k++) {
      B = tag_to_bonds[tagi][k];
      if (bond_to_tags[B][1] == tagi) {
        next_bond[B] = n;
        previous_bond[n] = B;
      }
    }
    // second tag
    j = ntag_to_bonds[tagj];
    for (int k = 0; k < j; k++) {
      B = tag_to_bonds[tagj][k];
      if (bond_to_tags[B][0] == tagj) {
        next_bond[n] = B;
        previous_bond[B] = n;
      }
    }
  }
  if (idebug == 1)
    printf("from tag to bonds done\n");
}

/* ----------------------------------------------------------------------
   pre exchange
   ---------------------------------------------------------------------- */
void FixVREVC::pre_exchange() {
  if (idebug == 1)
    printf("start pre_exchange\n");
  // // update ghosts
  // comm->forward_comm();
  if (idebug == 1)
    printf("pre_exchange done\n");
}

/* ----------------------------------------------------------------------
   zero all forces
   ---------------------------------------------------------------------- */
void FixVREVC::setup_pre_force(int /*zz*/) {
  if (idebug == 1)
    printf("start setup_pre_force\n");
  // zero all forces
  bigint nall = atom->nlocal + atom->nghost;
  for (int i = 0; i < nall; i++)
    atom->f[i][0] = atom->f[i][1] = atom->f[i][2] = 0.0;
  if (idebug == 1)
    printf("setup_pre_force done\n");
}

/* -------------------------------------------------------------------------
   memory usage of local atom-based array
   ------------------------------------------------------------------------- */
double FixVREVC::memory_usage() {
  double bytes = atom->nmax * 2 * sizeof(double);
  return bytes;
}

/* -------------------------------------------------------------------------
   find min distance for bonds i0/j0 and i1/j1 (with KKT conditions)
   ------------------------------------------------------------------------- */
inline void FixVREVC::getMinDist_KKT(double **&x, double &dx, double &dy,
                                     double &dz, double &ti, double &tj,
                                     int &i0, int &j0, int &i1, int &j1) {
  double diffx0, diffy0, diffz0, diffx1, diffy1, diffz1;
  double T0x, T0y, T0z, T1x, T1y, T1z;
  double r_star[9], s_star[9];
  /*! Segment length of each of the two segments
   */
  diffx0 = x[j0][0] - x[i0][0];
  diffy0 = x[j0][1] - x[i0][1];
  diffz0 = x[j0][2] - x[i0][2];
  double l0 = sqrt(diffx0 * diffx0 + diffy0 * diffy0 + diffz0 * diffz0);
  diffx1 = x[j1][0] - x[i1][0];
  diffy1 = x[j1][1] - x[i1][1];
  diffz1 = x[j1][2] - x[i1][2];
  double l1 = sqrt(diffx1 * diffx1 + diffy1 * diffy1 + diffz1 * diffz1);
  /*! Tangent vectors of each of the two segments
   */
  T0x = diffx0 / l0;
  T0y = diffy0 / l0;
  T0z = diffz0 / l0;
  T1x = diffx1 / l1;
  T1y = diffy1 / l1;
  T1z = diffz1 / l1;
  /*! Dot product between tangent segments
   */
  double cos01 = T0x * T1x + T0y * T1y + T0z * T1z;
  /* Distance vector between origin point of the two segments.
   */
  double DELTAx = x[i0][0] - x[i1][0];
  double DELTAy = x[i0][1] - x[i1][1];
  double DELTAz = x[i0][2] - x[i1][2];
  /*! Solutions for r and s inside the feasible region.
   */
  /*! solution 1 is not valid if the two segments are parallel.
   */
  double T0_DELTA = T0x * DELTAx + T0y * DELTAy + T0z * DELTAz;
  double T1_DELTA = T1x * DELTAx + T1y * DELTAy + T1z * DELTAz;
  if (cos01 > (SMALL - 1.) && cos01 < (1. - SMALL)) {
    r_star[0] = (cos01 * T1_DELTA - T0_DELTA) / (1. - cos01 * cos01);
    s_star[0] = (T1_DELTA - cos01 * T0_DELTA) / (1. - cos01 * cos01);
  } else {
    r_star[0] = -1.;
    s_star[0] = -1.;
  }
  /*! Solutions for r and s outside the feasible region.
   */
  /*! solution 2
   */
  r_star[1] = 0.;
  s_star[1] = T1_DELTA;
  /*! solution 3
   */
  r_star[2] = -T0_DELTA;
  s_star[2] = 0.;
  /*! solution 4
   */
  r_star[3] = 0.;
  s_star[3] = 0.;
  /*! solution 5
   */
  r_star[4] = l0;
  s_star[4] = l0 * cos01 + T1_DELTA;
  /*! solution 6
   */
  r_star[5] = l0;
  s_star[5] = 0.;
  /*! solution 7
   */
  r_star[6] = l1 * cos01 - T0_DELTA;
  s_star[6] = l1;
  /*! solution 8
   */
  r_star[7] = 0.;
  s_star[7] = l1;
  /*! solution 9
   */
  r_star[8] = l0;
  s_star[8] = l1;
  /*! min of d^2, loop over the nine previous solutions
   */
  double d2, d2_min_KKT = DELTAx * DELTAx + DELTAy * DELTAy + DELTAz * DELTAz +
                          l0 * l0 + l1 * l1;
  for (int i = 0; i < 9; i++) {
    /*! The solution is valid if and only if the two points are lying to the
     * segments and the corresponding distance is minimal.
     */
    if (!(r_star[i] < 0. || r_star[i] > l0 || s_star[i] < 0. ||
          s_star[i] > l1)) {
      diffx0 = x[i0][0] + r_star[i] * T0x;
      diffy0 = x[i0][1] + r_star[i] * T0y;
      diffz0 = x[i0][2] + r_star[i] * T0z;
      diffx1 = x[i1][0] + s_star[i] * T1x;
      diffy1 = x[i1][1] + s_star[i] * T1y;
      diffz1 = x[i1][2] + s_star[i] * T1z;
      d2 = (diffx0 - diffx1) * (diffx0 - diffx1) +
           (diffy0 - diffy1) * (diffy0 - diffy1) +
           (diffz0 - diffz1) * (diffz0 - diffz1);
      /*! Keep the minimal distance amongst the nine solutions.
       */
      if (d2 < d2_min_KKT) {
        d2_min_KKT = d2;
        // ti and tj between 0 and 1
        ti = fmin(1., fmax(.0, r_star[i] / l0));
        tj = fmin(1., fmax(.0, s_star[i] / l1));
        // min dist
        dx = diffx0 - diffx1;
        dy = diffy0 - diffy1;
        dz = diffz0 - diffz1;
      }
    }
  }
}

/*! @brief sort overlap index (binary insertion sort)
  @param noverlaps number of overlaps
  @param P number of beads
  @param ioverlaps list of index for each overlap
  @param loverlaps list of mapping for each overlap
 */
void FixVREVC::sort_overlap_index(int noverlaps, int P,
                                  unsigned int *&ioverlaps, int *&loverlaps,
                                  bool bsort) {
  if (idebug == 1)
    printf("sort overlap index %i %i ...\n", noverlaps, P);
  unsigned int oi;
  int ol;
  bool binsertion;
  // binary insertion sort
  if (bsort) {
    ioverlaps[0] = obeads0[0][0] * P + obeads1[0][0];
    loverlaps[0] = 0;
    for (int o = 1; o < noverlaps; o++) {
      oi = obeads0[o][0] * P + obeads1[o][0];
      ol = o;
      binsertion = false;
      // insertion ?
      for (int p = 0; p < o; p++) {
        if (oi < ioverlaps[p]) {
          for (int q = (o - 1); q >= p; q--) {
            ioverlaps[q + 1] = ioverlaps[q];
            loverlaps[q + 1] = loverlaps[q];
          }
          ioverlaps[p] = oi;
          loverlaps[p] = ol;
          binsertion = true;
          break;
        }
      }
      if (!binsertion) {
        ioverlaps[o] = oi;
        loverlaps[o] = ol;
      }
    }
  } else {
    for (int o = 0; o < noverlaps; o++) {
      ioverlaps[o] = obeads0[o][0] * P + obeads1[o][0];
      loverlaps[o] = o;
    }
  }
  if (idebug == 1)
    printf("sort overlap index done\n");
}

/*! @brief return true if |a - b| < tol * |b|
  @param a
  @param b
  @param tol tolerance
*/
bool FixVREVC::err_a_b(double a, double b, double tol) {
  if (a == .0 || b == .0)
    return fabs(a - b) < tol;
  else
    return fabs(a - b) < (tol * fabs(b));
}

/*! @brief "deja_vu" collisions
  @param loverlaps list of mapping for each overlap
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param deja_vu
  @param noverlaps number of overlaps
 */
void FixVREVC::deja_vu_collisions(int *&loverlaps, double *&od1, double *&od2,
                                  bool *&deja_vu, int &noverlaps) {
  if (idebug == 1)
    printf("deja vu ... (%i)\n", noverlaps);
  double d1, d2, d3, d4;
  int i0, j0, i1, j1, oo, OO, b1, b2, b3, b4;
  bool b13, b24, b14, b23;
  bool d13, d24, d14, d23;
  // pre-build for the "deja vu"
  int loverlaps01 = 0;
  // build list of overlaps/extremities
  for (int o = 0; o < noverlaps; o++) {
    i0 = loverlaps[o];
    d1 = od1[i0];
    d2 = od2[i0];
    if ((1. - d1) < SMALL || d1 < SMALL || (1. - d2) < SMALL || d2 < SMALL) {
      overlap01[loverlaps01] = i0;
      loverlaps01++;
    }
  }
  // "deja vu"
  std::fill_n(deja_vu, noverlaps, false);
  for (int i = 0; i < (loverlaps01 - 1); i++) {
    oo = overlap01[i];
    b1 = segment0[oo];
    b2 = segment1[oo];
    d1 = od1[oo];
    d2 = od2[oo];
    for (int j = (i + 1); j < loverlaps01; j++) {
      OO = overlap01[j];
      if (!deja_vu[OO]) {
        b3 = segment0[OO];
        b4 = segment1[OO];
        d3 = od1[OO];
        d4 = od2[OO];
        // point and monomer ?
        b13 = ((b1 == b3 && err_a_b(d1, d3, 1e-4)) ||
               (next_bond[b1] == b3 && d1 == 1. && d3 == .0) ||
               (b1 == next_bond[b3] && d1 == .0 && d3 == 1.));
        b24 = ((b2 == b4 && err_a_b(d2, d4, 1e-4)) ||
               (next_bond[b2] == b4 && d2 == 1. && d4 == .0) ||
               (b2 == next_bond[b4] && d2 == .0 && d4 == 1.));
        if (b13 && b24)
          deja_vu[OO] = true;
        b14 = ((b1 == b4 && err_a_b(d1, d4, 1e-4)) ||
               (next_bond[b1] == b4 && d1 == 1. && d4 == .0) ||
               (b1 == next_bond[b4] && d1 == .0 && d4 == 1.));
        b23 = ((b2 == b3 && err_a_b(d2, d3, 1e-4)) ||
               (next_bond[b2] == b3 && d2 == 1. && d3 == .0) ||
               (b2 == next_bond[b3] && d2 == .0 && d3 == 1.));
        if (b14 && b23)
          deja_vu[OO] = true;
      }
    }
  }
  // delete the "deja vu"
  i0 = 0;
  for (int o = 0; o < noverlaps; o++) {
    oo = loverlaps[o];
    if (!deja_vu[oo]) {
      overlap01[i0] = oo;
      i0++;
    }
  }
  for (int o = 0; o < i0; o++)
    loverlaps[o] = overlap01[o];
  for (int i = i0; i < noverlaps; i++)
    loverlaps[i] = -1;
  std::fill_n(deja_vu, noverlaps, false);
  noverlaps = i0;
  // again: remove duplicates
  for (int i = 0; i < (noverlaps - 1); i++) {
    oo = loverlaps[i];
    b1 = segment0[oo];
    b2 = segment1[oo];
    d1 = od1[oo];
    d2 = od2[oo];
    for (int j = (i + 1); j < noverlaps; j++) {
      OO = loverlaps[j];
      if (!deja_vu[OO]) {
        b3 = segment0[OO];
        b4 = segment1[OO];
        d3 = od1[OO];
        d4 = od2[OO];
        // same pair of segments ?
        if (b1 == b3 && err_a_b(d1, d3, 1e-5) && b2 == b4 &&
            err_a_b(d2, d4, 1e-5))
          deja_vu[OO] = true;
      }
    }
  }
  // again: delete the "deja vu"
  i0 = 0;
  for (int o = 0; o < noverlaps; o++) {
    oo = loverlaps[o];
    if (!deja_vu[oo]) {
      overlap01[i0] = oo;
      i0++;
    }
  }
  for (int o = 0; o < i0; o++)
    loverlaps[o] = overlap01[o];
  for (int i = i0; i < noverlaps; i++)
    loverlaps[i] = -1;
  noverlaps = i0;
  if (idebug == 1) {
    printf("deja vu done (%i)\n", noverlaps);
    for (int o = 0; o < noverlaps; o++) {
      oo = loverlaps[o];
      if ((segment0[oo] == s0 && segment1[oo] == s1) ||
          (segment0[oo] == s1 && segment1[oo] == s0)) {
        printf("ok(%i,%i/%i) s0 and s1 (after)\n", o, oo, noverlaps);
        printf("   %i %i %i %i\n", obeads0[oo][0], obeads0[oo][1],
               obeads1[oo][0], obeads1[oo][1]);
      }
    }
  }
}

/*! @brief build the relative velocities from collisions
  We know that v+ = v + l * w x t /2 and v- = v - l * w x t / 2 such that v =
  (v- + v+) / 2 and w x t = (v+ - v-) / l hold. Therefore V = v- + d * l * w x t
  with v+ and v- with d between 0 and 1: V = v- + d * (v+ - v-) = (1 - d) * v- +
  d * v+.
  @param noverlaps number of overlaps
  @param loverlaps mapping (in case of deja_vu)
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param nx12 normal (x) to the contact point
  @param ny12 normal (y) to the contact point
  @param nz12 normal (z) to the contact point
  @param Dv relative velocities
  @param Dv0 previous relative velocities
 */
void FixVREVC::build_U_collisions(int noverlaps, int *&loverlaps, double *&od1,
                                  double *&od2, double **&v, double *&nx12,
                                  double *&ny12, double *&nz12, double *&Dv,
                                  double *&Dv0) {
  if (idebug == 1)
    printf("build relative velocity to the contact's normal ...\n");
  int oo, i0, i1, j0, j1;
  double d1, d2, U;
#pragma omp parallel for private(oo, i0, j0, i1, j1, d1, d2, U)
  for (int o = 0; o < noverlaps; o++) {
    // overlap information
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d1 = od1[oo];
    d2 = od2[oo];
    // dot(Dv, n)
    U = ((1. - d1) * v[i0][0] + d1 * v[j0][0] -
         ((1. - d2) * v[i1][0] + d2 * v[j1][0])) *
        nx12[oo];
    U += ((1. - d1) * v[i0][1] + d1 * v[j0][1] -
          ((1. - d2) * v[i1][1] + d2 * v[j1][1])) *
         ny12[oo];
    U += ((1. - d1) * v[i0][2] + d1 * v[j0][2] -
          ((1. - d2) * v[i1][2] + d2 * v[j1][2])) *
         nz12[oo];
    Dv[oo] = U * (1.0 - 2.0 * (U < 0.0));
    Dv0[o] = U;
  }
  if (idebug == 1)
    printf("build relative velocity to the contact's normal done\n");
}

/*! @brief build collision(s) graph(s)
  @param noverlaps number of overlaps
  @param loverlaps list of mapping for each overlap
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param pgraph from bead to graph
  @param ligraph number of overlaps per graph
 */
int FixVREVC::build_collisions_graphs(int noverlaps, int *&loverlaps,
                                      double *&od1, double *&od2, int *&pgraph,
                                      int *&ligraph) {
  if (idebug == 1)
    printf("build collisions graphs ...\n");
  int lg = 0, oo, OO, i0, j0, i1, j1, I0, J0, I1, J1;
  double d0, d1, D0, D1;
  bool refus;
  // loop over overlaps
  for (int i = 0; i < noverlaps; i++) {
    oo = loverlaps[i];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d0 = od1[oo];
    d1 = od2[oo];
    // does the current overlap belong to already existing collision graph ?
    if (pgraph[i0 * (d0 < 1.) + j0 * (d0 == 1.)] == -1 &&
        pgraph[i0 * (d0 == .0) + j0 * (d0 > .0)] == -1 &&
        pgraph[i1 * (d1 < 1.) + j1 * (d1 == 1.)] == -1 &&
        pgraph[i1 * (d1 == .0) + j1 * (d1 > .0)] == -1) {
      // check extremities
      if (d0 < 1.)
        pgraph[i0] = lg;
      if (d0 > .0)
        pgraph[j0] = lg;
      if (d1 < 1.)
        pgraph[i1] = lg;
      if (d1 > .0)
        pgraph[j1] = lg;
      ligraph[lg] = 0;
      // find all the children of the current overlap: build the new collision
      // graph.
      while (1) {
        refus = true;
        for (int o = (i + 1); o < noverlaps; o++) {
          OO = loverlaps[o];
          I0 = obeads0[OO][0];
          J0 = obeads0[OO][1];
          I1 = obeads1[OO][0];
          J1 = obeads1[OO][1];
          D0 = od1[OO];
          D1 = od2[OO];
          if (pgraph[I0 * (D0 < 1.) + J0 * (D0 == 1.)] == lg ||
              pgraph[I0 * (D0 == .0) + J0 * (D0 > .0)] == lg ||
              pgraph[I1 * (D1 < 1.) + J1 * (D1 == 1.)] == lg ||
              pgraph[I1 * (D1 == .0) + J1 * (D1 > .0)] == lg) {
            // four beads to check
            if (pgraph[I0 * (D0 < 1.) + J0 * (D0 == 1.)] == -1) {
              refus = false;
              pgraph[I0 * (D0 < 1.) + J0 * (D0 == 1.)] = lg;
            }
            if (pgraph[I0 * (D0 == .0) + J0 * (D0 > .0)] == -1) {
              refus = false;
              pgraph[I0 * (D0 == .0) + J0 * (D0 > .0)] = lg;
            }
            if (pgraph[I1 * (D1 < 1.) + J1 * (D1 == 1.)] == -1) {
              refus = false;
              pgraph[I1 * (D1 < 1.) + J1 * (D1 == 1.)] = lg;
            }
            if (pgraph[I1 * (D1 == .0) + J1 * (D1 > .0)] == -1) {
              refus = false;
              pgraph[I1 * (D1 == .0) + J1 * (D1 > .0)] = lg;
            }
          }
        }
        if (refus)
          break;
      } // elihw
      // count the number of overlaps for the current graph.
      for (int o = 0; o < noverlaps; o++) {
        OO = loverlaps[o];
        I0 = obeads0[OO][0];
        J0 = obeads0[OO][1];
        I1 = obeads1[OO][0];
        J1 = obeads1[OO][1];
        D0 = od1[OO];
        D1 = od2[OO];
        ligraph[lg] += (pgraph[I0 * (D0 < 1.) + J0 * (D0 == 1.)] == lg ||
                        pgraph[I0 * (D0 == .0) + J0 * (D0 > .0)] == lg ||
                        pgraph[I1 * (D1 < 1.) + J1 * (D1 == 1.)] == lg ||
                        pgraph[I1 * (D1 == .0) + J1 * (D1 > .0)] == lg);
      }
      lg++;
    }
  }
  if (idebug == 1)
    printf("build collisions graphs (%i->%i) done\n", noverlaps, lg);
  return lg;
}

/*! @brief resize igraph ?
  change the number of overlaps per graph
  change the graph maximum number of overlaps
  @param lgraph number of overlap graphs
  @param ligraph number of overlaps per graph
  @param size_igraph size of igraph
  @param igraph list of overlaps for each graph
  @param operg number of overlap constraints per graph
 */
void FixVREVC::resize_igraph(int lgraph, int *&ligraph, int &size_igraph,
                             int *&igraph, int &operg) {
  if (idebug == 1)
    printf("resize_igraphs ...\n");
  int new_operg = 0;
  for (int i = 0; i < lgraph; i++)
    new_operg = MAX(new_operg, ligraph[i]);
  // resize igraph ?
  if ((lgraph * new_operg) > size_igraph) {
    size_igraph = lgraph * new_operg;
    memory->grow(igraph, size_igraph, "fix:igraph");
    std::fill_n(igraph, size_igraph, -1);
  }
  // new number of overlap constraints per graph
  operg = new_operg;
  if (idebug == 1)
    printf("resize_igraphs done\n");
}

/*! @brief index of the overlap(s) for each collision graph
  @param lgraph number of overlap graphs
  @param noverlaps number of overlaps
  @param operg number of overlap constraints per graph
  @param loverlaps
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param pgraph from bead to graph
  @param igraph list of overlap constraint indices per graph
  @param ligraph list of the number of overlap constraints per graph
 */
void FixVREVC::graph_to_overlap(int lgraph, int noverlaps, int operg,
                                int *&loverlaps, double *&od1, double *&od2,
                                int *&pgraph, int *&igraph, int *&ligraph) {
  if (idebug == 1)
    printf("index of the overlap(s) for each collision graph ...\n");
  int oo, i0, j0, i1, j1, p1, p2, p3, p4, lg;
  double d0, d1, d2;
  std::fill(ligraph, ligraph + lgraph, 0);
  for (int o = 0; o < noverlaps; o++) {
    oo = loverlaps[o];
    if (oo < 0)
      error->all(FLERR, "negative overlap mapping");
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d0 = od1[oo];
    d1 = od2[oo];
    // mapping between bead and graph
    p1 = pgraph[i0 * (d0 < 1.0) + j0 * (d0 == 1.0)];
    p2 = pgraph[i0 * (d0 == 0.0) + j0 * (d0 > 0.0)];
    p3 = pgraph[i1 * (d1 < 1.0) + j1 * (d1 == 1.0)];
    p4 = pgraph[i1 * (d1 == 0.0) + j1 * (d1 > 0.0)];
    // check if mapping index is negative
    if (p1 == -1 || p2 == -1 || p3 == -1 || p4 == -1) {
      printf("warning graph_to_overlap(-1) o=%i %i %i(%f) %i %i(%f)\n", o, p1,
             p2, d0, p3, p4, d1);
      printf("%i %i %i %i\n", i0, j0, i1, j1);
      printf("%i %i %i %i\n", i0 * (d0 < 1.) + j0 * (d0 == 1.),
             i0 * (d0 == .0) + j0 * (d0 > .0), i1 * (d1 < 1.) + j1 * (d1 == 1.),
             i1 * (d1 == .0) + j1 * (d1 > .0));
      exit(EXIT_FAILURE);
    }
    // check if mapping is greater than the number of graphs
    if (p1 >= lgraph || p2 >= lgraph || p3 >= lgraph || p4 >= lgraph) {
      printf("warning graph_to_overlap(%i) o=%i %i %i(%f) %i %i(%f)\n", lgraph,
             o, p1, p2, d0, p3, p4, d1);
      printf("%i %i %i %i\n", i0, j0, i1, j1);
      printf("%i %i %i %i\n", i0 * (d0 < 1.) + j0 * (d0 == 1.),
             i0 * (d0 == .0) + j0 * (d0 > .0), i1 * (d1 < 1.) + j1 * (d1 == 1.),
             i1 * (d1 == .0) + j1 * (d1 > .0));
      exit(EXIT_FAILURE);
    }
    // check if the number of overlaps per graph exceed the limit value
    if (ligraph[p1] >= operg || ligraph[p2] >= operg || ligraph[p3] >= operg ||
        ligraph[p4] >= operg) {
      printf("warning graph_to_overlap ligraph>=operg:");
      printf("%i/%i: %i-%i(%f) with %i-%i(%f)\n", o, noverlaps, i0, j0, d0, i1,
             j1, d1);
      printf("g=%i %i %i %i\n", p1, p2, p3, p4);
      printf("lg=%i %i %i %i/%i\n", ligraph[p1], ligraph[p2], ligraph[p3],
             ligraph[p4], operg);
      exit(EXIT_FAILURE);
    }
    // assign overlap to graph
    igraph[p1 * operg + ligraph[p1]] = o;
    ligraph[p1]++;
    if (p1 != p2) {
      igraph[p2 * operg + ligraph[p2]] = o;
      ligraph[p2]++;
    }
    if (p1 != p3 && p2 != p3) {
      igraph[p3 * operg + ligraph[p3]] = o;
      ligraph[p3]++;
    }
    if (p1 != p4 && p2 != p4 && p3 != p4) {
      igraph[p4 * operg + ligraph[p4]] = o;
      ligraph[p4]++;
    }
  }
  // sort the overlap index for each graph
  bool binsertion;
  int *gbuffer = new int[operg]();
  for (int g = 0; g < lgraph; g++) {
    gbuffer[0] = igraph[g * operg + 0];
    lg = ligraph[g];
    for (int i = 1; i < lg; i++) {
      i0 = igraph[g * operg + i];
      binsertion = false;
      for (int j = 0; j < i; j++) {
        // binary insertion sort
        if (i0 < gbuffer[j]) {
          for (int k = (i - 1); k >= j; k--)
            gbuffer[k + 1] = gbuffer[k];
          gbuffer[j] = i0;
          binsertion = true;
          break;
        }
      }
      if (!binsertion)
        gbuffer[i] = i0;
    }
    for (int i = 0; i < lg; i++)
      igraph[g * operg + i] = gbuffer[i];
  }
  delete[] gbuffer;
  gbuffer = NULL;
  if (idebug == 1) {
    // check sort
    for (int g = 0; g < lgraph; g++) {
      lg = ligraph[g];
      for (int i = 0; i < (lg - 1); i++) {
        for (int j = (i + 1); j < lg; j++) {
          if (igraph[g * operg + i] > igraph[g * operg + j])
            error->all(FLERR, "warning sort\n");
        }
      }
    }
    printf("index of the overlap(s) for each collision graph done\n");
  }
}

/*! @brief add overlaps to the constraint matrix J
  @param noverlaps number of overlaps
  @param loverlaps map the overlap index
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param nx12 normal (x) to the contact point
  @param ny12 normal (y) to the contact point
  @param nz12 normal (z) to the contact point
  @param Jcoll constraint matrix
  @param Jcoll_nzero list of non-zero entries for Jcoll
  @param C not needed for now
 */
void FixVREVC::Jo_linear_chain_velocity_based(int noverlaps, int *&loverlaps,
                                              double *&od1, double *&od2,
                                              double *&nx12, double *&ny12,
                                              double *&nz12, double *&Jcoll,
                                              int *&Jcoll_nzero, int C) {
  if (idebug == 1)
    printf("build constraint matrix J ...\n");
  int oo, i0, j0, i1, j1, S;
  double d0, d1, nx, ny, nz;
#pragma omp parallel for private(oo, i0, j0, i1, j1, d0, d1, nx, ny, nz, S)
  for (int o = 0; o < noverlaps; o++) {
    // overlap informations
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d0 = od1[oo];
    d1 = od2[oo];
    nx = nx12[oo];
    ny = ny12[oo];
    nz = nz12[oo];
    S = C * 6 + o * 12;
    // col != 0
    for (int j = 0; j < 3; j++) {
      Jcoll_nzero[S + j] = 3 * i0 + j;
      Jcoll_nzero[S + 3 + j] = 3 * j0 + j;
      Jcoll_nzero[S + 6 + j] = 3 * i1 + j;
      Jcoll_nzero[S + 9 + j] = 3 * j1 + j;
    }
    // J
    Jcoll[S + 0] = (1.0 - d0) * nx;
    Jcoll[S + 1] = (1.0 - d0) * ny;
    Jcoll[S + 2] = (1.0 - d0) * nz;
    Jcoll[S + 3] = d0 * nx;
    Jcoll[S + 4] = d0 * ny;
    Jcoll[S + 5] = d0 * nz;
    Jcoll[S + 6] = (d1 - 1.0) * nx;
    Jcoll[S + 7] = (d1 - 1.0) * ny;
    Jcoll[S + 8] = (d1 - 1.0) * nz;
    Jcoll[S + 9] = -d1 * nx;
    Jcoll[S + 10] = -d1 * ny;
    Jcoll[S + 11] = -d1 * nz;
  }
  if (idebug == 1)
    printf("build constraint matrix J done\n");
}

/*! @brief for each overlap get the index of each overlap belonging to the same
  graph
  @param lgraph number of graphs
  @param operg number of overlaps per graph
  @param igraph list of overlaps per graph
  @param ligraph list of number of overlaps per graph
  @param next_constraint next constraint of graph 'g'
  @param lnext number of next constraints
  @param nperc number of next constraints per constraint
  @param C not needed for now
 */
void FixVREVC::next_overlaps(int lgraph, int operg, int *&igraph, int *&ligraph,
                             int *&next_constraint, int *&lnext, int nperc,
                             int C) {
  if (idebug == 1)
    printf("next_overlaps ...\n");
  int i0, i1, lg;
  // loop over the graphs
#pragma omp parallel for private(i0, i1, lg)
  for (int g = 0; g < lgraph; g++) {
    lg = ligraph[g];
    // loop over the overlaps of graph 'g'
    for (int i = 0; i < lg; i++) {
      i0 = C + igraph[g * operg + i];
      for (int j = i; j < lg; j++) {
        i1 = C + igraph[g * operg + j];
        if (lnext[i0] >= nperc) {
          printf("!!! warning next_overlaps %i/%i %i/%i %i/%i: %i/%i\n", g,
                 lgraph, i, lg, j, lg, lnext[i0], nperc);
          exit(EXIT_FAILURE);
        }
        next_constraint[i0 * nperc + lnext[i0]] = i1;
        lnext[i0]++;
      }
    }
  }
  if (idebug == 1)
    printf("next_overlaps done\n");
}

/*! @brief build J*M^-1*J^T for articulated system + collisions and guess
  solutions for J*M^-1*J^T*L=B
  @param next_constraint next constraint of graph 'g'
  @param lnext number of next constraints
  @param nperc number of next constraints per constraint
  @param Jcoll constraint matrix
  @param Jcoll_nzero list of non-zero entries for Jcoll
  @param JJT matrix J*M^-1*J^T
  @param JJTc_nzero for a given row constraint: list of non-zero entries
  @param lJJT_nzero for a given row constraint: number of non-zero entries
  @param Lcoll L from J*M^-1*J^T*L=B
  @param Bcoll B from J*M^-1*J^T*L=B
  @param C not needed for now
  @param noverlaps number of overlaps
  @param ms mass of the particles
 */
void FixVREVC::build_JinvMJT(int *&next_constraint, int *&lnext, int nperc,
                             double *&Jcoll, int *&Jcoll_nzero, double *&JJT,
                             int *&JJTc_nzero, int *&lJJT_nzero, double *&Lcoll,
                             double *&Bcoll, int C, int noverlaps, double ms) {
  if (idebug == 1)
    printf("build J*inv(M)*J^T ...\n");
  // double *mass = atom->mass;
  // int *type = atom->type;
  double invm = 1.0 / ms, d0, d1, d2, cfm = 0.0;
  int D = C + noverlaps;
  int i0, i1, j1, c0, c1, I, J, L, index;
  std::fill_n(lJJT_nzero, D, 0);
  // loop over the constraints
  // #pragma omp parallel for private(I,i0,c0,L,J,i1,j1,c1,d0,d1,d2,index)
  for (int i = 0; i < D; i++) {
    I = i;
    i0 = (I < C) ? 6 : 12;
    c0 = (I < C) ? I * 6 : C * 6 + (I - C) * 12;
    // loop over the constraints that belong to the same graph
    L = lnext[i];
    for (int j = 0; j < L; j++) {
      J = next_constraint[I * nperc + j];
      i1 = (J < C) ? 6 : 12;
      c1 = (J < C) ? J * 6 : C * 6 + (J - C) * 12;
      d0 = 0.0;
      if (I == J)
        for (int k = 0; k < i0; k++)
          d0 += Jcoll[c0 + k] * invm * Jcoll[c1 + k];
      else {
        for (int k = 0; k < i0; k++) {
          j1 = Jcoll_nzero[c0 + k];
          d1 = Jcoll[c0 + k];
          // case: both extremity are involved in the overlap
          if (d1 != 0.0) {
            for (int l = 0; l < i1; l++) {
              d2 = Jcoll[c1 + l];
              d0 += (j1 == Jcoll_nzero[c1 + l] && d2 != 0.0) ? d1 * invm * d2
                                                             : 0.0;
            }
          }
        }
      }
      // JJT[I,J]
      if (d0 != 0.0) { // fabs(d0) > 1e-8) {
        index = I * nperc + lJJT_nzero[I];
        JJT[index] = d0 + (I == J) * cfm;
        JJTc_nzero[index] = J;
        lJJT_nzero[I]++;
        if (I != J) {
          // JJT[J,I]
          index = J * nperc + lJJT_nzero[J];
          JJT[index] = d0;
          JJTc_nzero[index] = I;
          lJJT_nzero[J]++;
        } else
          Lcoll[I] = Bcoll[I] / (d0 + cfm);
      }
    }
  }
  if (idebug == 1)
    printf("build J*inv(M)*J^T done\n");
}

/*! @brief guess solution of J*M^-1*J^T*L=B
  @param next_constraint next constraint of graph 'g'
  @param lnext number of next constraints
  @param nperc number of next constraints per constraint
  @param JJT matrix J*M^-1*J^T
  @param lJJT_nzero for a given row constraint: number of non-zero entries
  @param Lcoll L from J*M^-1*J^T*L=B
  @param bs B from J*M^-1*J^T*L=B
  @param C not needed for now
  @param noverlaps number of overlaps
 */
void FixVREVC::guess_solution(const int *next_constraint, const int *lnext,
                              int nperc, const double *JJT,
                              const int *lJJT_nzero, double *&Lcoll,
                              const double *bs, int C, int noverlaps) {
  double cfm = 0.0;
  int J, L, D = C + noverlaps;
  // loop over the constraints
  for (int i = 0; i < D; i++) {
    // loop over the constraints that belong to the same graph
    L = lnext[i];
    for (int j = 0; j < L; j++) {
      J = next_constraint[i * nperc + j];
      if (i == J) {
        Lcoll[i] = bs[i] / (JJT[i * nperc + lJJT_nzero[i]] + cfm);
        break;
      }
    }
  }
}

/*! @brief add collisions to the rhs of J*M^-1*J^T*L=B
  J(t)*V(t+h)=rhs
  J(t)*(V(t)+h*M^-1*(F(t)+J(t)^T*L(t)))=rhs
  J(t)*M^-1*J(t)^T*L(t)=(rhs-J(t)*V(t))/h-J(t)*M^-1*F(t)
  @param noverlaps number of overlaps
  @param loverlaps map the overlap index
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param v velocities
  @param Dv relative velocities
  @param nx12 normal (x) to the contact point
  @param ny12 normal (y) to the contact point
  @param nz12 normal (z) to the contact point
  @param inv_dt inverse of time-step
  @param Bcoll B from J*M^-1*J^T*L=B
  @param C not needed for now
 */
void FixVREVC::add_collisions_to_bs(int noverlaps, int *&loverlaps,
                                    double *&od1, double *&od2, double **&v,
                                    double *&Dv, double *&nx12, double *&ny12,
                                    double *&nz12, double inv_dt,
                                    double *&Bcoll, int *&sLcoll, int C) {
  if (idebug == 1)
    printf("add_collisions_to_bs ...\n");
  int oo, i0, j0, i1, j1;
  double d0, d1, jv;
#pragma omp parallel for private(oo, i0, j0, i1, j1, d0, d1, jv)
  for (int o = 0; o < noverlaps; o++) {
    // overlap informations
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d0 = od1[oo];
    d1 = od2[oo];
    // dot(Dv, n)
    jv = ((1.0 - d0) * v[i0][0] + d0 * v[j0][0] -
          ((1.0 - d1) * v[i1][0] + d1 * v[j1][0])) *
         nx12[oo];
    jv += ((1.0 - d0) * v[i0][1] + d0 * v[j0][1] -
           ((1.0 - d1) * v[i1][1] + d1 * v[j1][1])) *
          ny12[oo];
    jv += ((1.0 - d0) * v[i0][2] + d0 * v[j0][2] -
           ((1.0 - d1) * v[i1][2] + d1 * v[j1][2])) *
          nz12[oo];
    Bcoll[C + o] = (Dv[oo] - jv) * inv_dt;
    // too much correction during previous step ?
    sLcoll[C + o] = (jv > 0.0) ? 0 : (((Dv[oo] - jv) < 0.0) ? -1 : 1);
  }
  if (idebug == 1)
    printf("add_collisions_to_bs done\n");
}

/*! @brief Successive-Over-Relaxation for J*M^-1*J^T*L=B (such that L>=0 for
  collisions) returns the cumulative number of SORs
  @param JJT matrix J*M^-1*J^T
  @param JJTc_nzero for a given row constraint: list of non-zero entries
  @param lJJT_nzero for a given row constraint: number of non-zero entries
  @param Lcoll L from J*M^-1*J^T*L=B
  @param Bcoll B from J*M^-1*J^T*L=B
  @param nperc number of constraints per constraint
  @param noverlaps number of overlaps
  @param C not needed for now
  @param wsor SOR parameter
  @param nsor number of SORs
 */
int FixVREVC::SOR_JinvMJT(double *&JJT, int *&JJTc_nzero, int *&lJJT_nzero,
                          double *&Lcoll, int *&sLcoll, double *&Bcoll,
                          int nperc, int noverlaps, int C, double wsor,
                          int nsor) {
  if (idebug == 1)
    printf("SOR_JinvMJT ...\n");
  bool bsolution, bchange = false;
  int J, L, D = C + noverlaps, isor = 0;
  double z1, z2, d0, d1;
  double rtol0 = psor, rtol = psor, rtol_change = psor;
  // sor
  while (isor < nsor) {
    bchange = false;
    for (int i = 0; i < D; i++) {
      L = lJJT_nzero[i];
      d0 = 0.0;
      d1 = 1.0;
      for (int j = 0; j < L; j++) {
        J = JJTc_nzero[i * nperc + j];
        if (J < 0)
          error->all(FLERR, "non-zero column of JJT is negative");
        d0 += (i != J) ? JJT[i * nperc + j] * Lcoll[J] : 0.0;
        d1 = (i == J) ? JJT[i * nperc + j] : d1;
      }
      d1 = (Bcoll[i] - d0) / d1;
      // previous solution
      z1 = Lcoll[i];
      // new solution
      switch (sLcoll[i]) {
      case 1:
        z2 = (i < C) ? z1 + wsor * (d1 - z1) : fmax(0.0, z1 + wsor * (d1 - z1));
        break;
      case 0:
        z2 = 0.0;
        break;
      case -1:
        z2 = (i < C) ? z1 + wsor * (d1 - z1) : fmin(0.0, z1 + wsor * (d1 - z1));
        break;
      default:
        z2 = z1 + wsor * (d1 - z1);
        break;
      }
      // does the solution change anymore ?
      bchange = (bchange || ((z1 == 0.0 || z2 == 0.0)
                                 ? (fabs(z1 - z2) > rtol0)
                                 : (fabs(z1 - z2) > (rtol_change * fabs(z1)))));
      // new solution
      Lcoll[i] = z2;
    }
    // check the solution
    bsolution = true;
#pragma omp parallel for private(d0, L, z1) reduction(&& : bsolution)
    for (int i = 0; i < ((bsolution)*D); i++) {
      d0 = 0.0;
      L = lJJT_nzero[i];
      for (int j = 0; j < L; j++)
        d0 += JJT[i * nperc + j] * Lcoll[JJTc_nzero[i * nperc + j]];
      z1 = Bcoll[i];
      bsolution =
          (bsolution && ((z1 == 0.0) ? (fabs(z1 - d0) < rtol0)
                                     : (fabs(z1 - d0) < (rtol * fabs(z1)))));
    }
    isor++;
    if (bsolution || !bchange)
      break;
  }
  if (idebug == 1)
    printf("SOR_JinvMJT done, %i/%i sor(s)\n", isor, nsor);
  return isor;
}

/*! @brief Successive-Over-Relaxation for J*M^-1*J^T*L=B (such that L>=0 for
  collisions) compute optimum wsor and overwrite 'wsor' argument returns the
  cumulative number of SORs
  @param JJT matrix J*M^-1*J^T
  @param JJTc_nzero for a given row constraint: list of non-zero entries
  @param lJJT_nzero for a given row constraint: number of non-zero entries
  @param Lcoll L from J*M^-1*J^T*L=B
  @param Bcoll B from J*M^-1*J^T*L=B
  @param nperc number of constraints per constraint
  @param noverlaps number of overlaps
  @param wsor SOR parameter
  @param nsor number of SORs
  @param lgraph number of graphs
  @param igraph list of overlaps per graph
  @param ligraph list of number of overlaps per graph
  @param operg number of overlaps per graph
 */
int FixVREVC::gSOR_JinvMJT(double *&JJT, int *&JJTc_nzero, int *&lJJT_nzero,
                           double *&Lcoll, int *&sLcoll, double *&Bcoll,
                           int nperc, int noverlaps, double wsor, int nsor,
                           int lgraph, const int *igraph, const int *ligraph,
                           int operg) {
  if (idebug == 1)
    printf("SOR_JinvMJT ...\n");
  bool bsolution, bchange;
  double z1, z2, d0, d1;
  double rtol0 = psor, rtol = psor, rtol_change = psor;
  int J, L, lg, og, cum_isor = 0, isor = 0, new_wsor = wsor, M = 0;
  for (int g = 0; g < lgraph; g++)
    M = MAX(M, ligraph[g]); // ???
  // variables: SOR wopt and Power-Method ???
  double *A = new double[M * M]();    // ???
  double *C = new double[M * M]();    // ???
  double *eigV = new double[M]();     // ???
  double *new_eigV = new double[M](); // ???
  // sor
#pragma omp parallel for firstprivate(lg,bchange,bsolution,isor,new_wsor,og,L,d0,d1,J,z1,z2,A,C,eigV,new_eigV) reduction(+:cum_isor)
  // #pragma omp simd
  // firstprivate(lg,bchange,bsolution,isor,new_wsor,og,L,d0,d1,J,z1,z2,A,C,eigV,new_eigV)
  // reduction(+:cum_isor)
  for (int g = 0; g < lgraph; g += 8) {
    for (int h = 0; h < 8; h++) {
      if ((g + h) < lgraph) {
        lg = ligraph[g + h];
        isor = 0;
        new_wsor = wsor;
        // compute wsor optimum
        if (lg > 1 && lg < 10) {
          // J*M^-1*J^T matrix for the current graph
          std::fill_n(A, M * M, 0.0);
          for (int i = 0; i < lg; i++) {
            og = igraph[(g + h) * operg + i];
            L = lJJT_nzero[og];
            for (int j = i; j < lg; j++) {
              J = igraph[(g + h) * operg + j];
              for (int k = 0; k < L; k++) {
                if (J == JJTc_nzero[og * nperc + k]) {
                  A[i * M + j] = JJT[og * nperc + k];
                  A[j * M + i] = A[i * M + j];
                  break;
                }
              }
            }
          }
          // Jacobi iteration matrix
          d1 = 0.0;
          std::fill_n(C, M * M, 0.0);
          for (int i = 0; i < lg; i++) {
            d0 = 1.0 / A[i * M + i];
            for (int j = 0; j < lg; j++)
              C[i * M + j] = -A[i * M + j] * d0;
            C[i * M + i] += 1.0;
            // eigenvector guess
            d0 = 1.0 - 2.0 * random->uniform();
            eigV[i] = d0;
            d1 += d0 * d0;
          }
          d1 = 1.0 / sqrt(d1);
          for (int i = 0; i < lg; i++)
            eigV[i] *= d1;
          // power iterations
          for (int p = 0; p < 10000; p++) {
            // new vector
            d0 = 0.0;
            for (int i = 0; i < lg; i++) {
              d1 = 0.0;
              for (int j = 0; j < lg; j++)
                d1 += C[i * M + j] * eigV[j];
              new_eigV[i] = d1;
              // norm
              d0 += d1 * d1;
            }
            d0 = 1.0 / sqrt(d0);
            // normalize
            bchange = false;
            for (int i = 0; i < lg; i++) {
              d1 = new_eigV[i] * d0;
              bchange =
                  (bchange || fabs(eigV[i] - d1) > (1e-6 * fabs(eigV[i])));
              eigV[i] = d1;
            }
            if (!bchange)
              break;
          }
          // spectral radius ||C*x||/||x||
          // C*x
          for (int i = 0; i < lg; i++) {
            new_eigV[i] = 0.0;
            for (int j = 0; j < lg; j++)
              new_eigV[i] += C[i * M + j] * eigV[j];
          }
          // compute ||C*x|| and ||x||
          // ||x|| should be 1
          d0 = d1 = 0.0;
          for (int i = 0; i < lg; i++) {
            d0 += new_eigV[i] * new_eigV[i];
            d1 += eigV[i] * eigV[i];
          }
          new_wsor = fmax(0.0, fmin(2.0, ((d0 / d1) < 1.0)
                                             ? 2.0 / (1.0 + sqrt(1.0 - d0 / d1))
                                             : wsor));
        }
        // while not precision or did not reach the number of iterations
        while (isor < nsor) {
          bchange = false;
          for (int i = 0; i < lg; i++) {
            og = igraph[(g + h) * operg + i];
            L = lJJT_nzero[og];
            d0 = 0.0;
            d1 = 1.0;
            for (int j = 0; j < L; j++) {
              J = JJTc_nzero[og * nperc + j];
              if (J < 0)
                error->all(FLERR, "non-zero column of JJT is negative");
              d0 += (og != J) ? JJT[og * nperc + j] * Lcoll[J] : 0.0;
              d1 = (og == J) ? JJT[og * nperc + j] : d1;
            }
            d1 = (Bcoll[og] - d0) / d1;
            // previous solution
            z1 = Lcoll[og];
            // new solution
            switch (sLcoll[og]) {
            case -1:
              z2 = fmin(0.0, z1 + new_wsor * (d1 - z1));
              break;
            case 0:
              z2 = 0.0;
              break;
            case 1:
              z2 = fmax(0.0, z1 + new_wsor * (d1 - z1));
              break;
            default:
              z2 = z1 + new_wsor * (d1 - z1);
              break;
            }
            // does the solution change anymore ?
            bchange =
                (z1 == 0.0 || z2 == 0.0)
                    ? (bchange || fabs(z1 - z2) > rtol0)
                    : (bchange || fabs(z1 - z2) > (rtol_change * fabs(z1)));
            // new solution
            Lcoll[og] = z2;
          }
          // check the solution
          bsolution = true;
          for (int i = 0; i < ((bsolution)*lg); i++) {
            og = igraph[(g + h) * operg + i];
            d0 = 0.0;
            L = lJJT_nzero[og];
            for (int j = 0; j < L; j++)
              d0 += JJT[og * nperc + j] * Lcoll[JJTc_nzero[og * nperc + j]];
            z1 = Bcoll[og];
            bsolution = (z1 == 0.0)
                            ? (bsolution && fabs(z1 - d0) < rtol0)
                            : (bsolution && fabs(z1 - d0) < (rtol * fabs(z1)));
          }
          isor++;
          if (bsolution || !bchange)
            break;
        }
        cum_isor += isor;
      }       // fi
    }         // rof
  }           // rof
  delete[] A; // ???
  A = NULL;
  delete[] C; // ???
  C = NULL;
  delete[] eigV; // ???
  eigV = NULL;
  delete[] new_eigV; // ???
  new_eigV = NULL;
  if (idebug == 1)
    printf("SOR_JinvMJT done, %i/%i sor(s)\n", isor, nsor);
  return cum_isor;
}

/*! @brief get the articulated system + collisions feedback forces J^T*lambda
  @param Lcoll Lagrange multipliers from constraints
  @param Jcoll constraint matrix
  @param Jcoll_nzero list of non-zero entries for Jcoll
  @param JTl where to store the feedback forces J^T*lambda
  @param noverlaps number of overlaps
  @param C not needed for now
 */
void FixVREVC::JTlambda(double *&Lcoll, double *&Jcoll, int *&Jcoll_nzero,
                        double *&JTl, int noverlaps, int C) {
  if (idebug == 1)
    printf("J^T*lambda ...\n");
  int D = C + noverlaps;
  int I, nc;
  for (int i = 0; i < D; i++) {
    I = (i < C) ? i * 6 : C * 6 + (i - C) * 12;
    nc = (i < C) ? 6 : 12;
    for (int j = 0; j < nc; j++)
      JTl[Jcoll_nzero[I + j]] += Jcoll[I + j] * Lcoll[i];
  }
  if (idebug == 1)
    printf("J^T*lambda done\n");
}

/*! @brief add J^T*lambda to velocities
  @param dt time-step
  @param mass mass
  @param v velocities
  @param JTl feedback forces J^T*lambda
 */
void FixVREVC::add_JTl_to_velocities(double dt, double mass, double **&v,
                                     double *&JTl) {
  if (idebug == 1)
    printf("add J^T*lambda to velocities ...\n");
  double d0 = dt / mass;
  int oo, i0, j0, i1, j1;
  for (int o = 0; o < noverlaps; o++) {
    // overlap information
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    // add dt * J^T * lambda / mass to v and then zero J^T * lambda
    for (int i = 0; i < 3; i++) {
      v[i0][i] += d0 * JTl[3 * i0 + i];
      v[j0][i] += d0 * JTl[3 * j0 + i];
      v[i1][i] += d0 * JTl[3 * i1 + i];
      v[j1][i] += d0 * JTl[3 * j1 + i];
      JTl[3 * i0 + i] = 0.0;
      JTl[3 * j0 + i] = 0.0;
      JTl[3 * i1 + i] = 0.0;
      JTl[3 * j1 + i] = 0.0;
    }
  }
  if (idebug == 1)
    printf("add J^T*lambda to velocities done\n");
}

/*! @brief add J^T*lambda to forces
  @param P number of beads
  @param f forces
  @param JTl feedback forces J^T*lambda
 */
void FixVREVC::add_JTl_to_forces(int P, double **&f, double *&JTl) {
  int oo, i0, j0, i1, j1;
  for (int o = 0; o < noverlaps; o++) {
    // overlap
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    // add J^T*lambda to f and then zero J^T*lambda
    // #pragma omp parallel for
    for (int i = 0; i < 3; i++) {
      f[i0][i] += JTl[3 * i0 + i];
      f[j0][i] += JTl[3 * j0 + i];
      f[i1][i] += JTl[3 * i1 + i];
      f[j1][i] += JTl[3 * j1 + i];
      JTl[3 * i0 + i] = 0.0;
      JTl[3 * j0 + i] = 0.0;
      JTl[3 * i1 + i] = 0.0;
      JTl[3 * j1 + i] = 0.0;
    }
  }
}

/*! @brief check relative velocity from overlaps
  @param noverlaps number of overlaps
  @param loverlaps mapping in case of deja vu
  @param od1 distance from the left bead of the first bond
  @param od2 distance from the left bead of the second bond
  @param nx12 normal (x) to the contact point
  @param ny12 normal (y) to the contact point
  @param nz12 normal (z) to the contact point
  @param v velocities from atom->v
  @param rtol tolerance if the value is not 0
  @param rtol_change change in solution tolerance
  @param rtol0 tolerance if the value is 0
  @param b0 precision ?
  @param b1 does the solution change anymore ?
  @param b2 is relative velocity positive along the normal to the contact ?
  @param b3 relative velocity change too big ?
 */
void FixVREVC::check_relative_velocity_collisions(
    int noverlaps, int *&loverlaps, double *&od1, double *&od2, double *&nx12,
    double *&ny12, double *&nz12, double **&v, double *&Dv, double *&Dv0,
    double rtol, double rtol_change, double rtol0, bool &b0, bool &b1, bool &b2,
    bool &b3) {
  int oo, og, i0, j0, i1, j1, lg;
  double d0, d1, d2;
  b0 = b1 = b2 = b3 = true;
  // ???
  bool p0, c0, n0, r0;
  if (idebug == 1) {
    for (int g = 0; g < lgraph; g++) {
      lg = ligraph[g];
      for (int l = 0; l < lg; l++) {
        og = igraph[g * operg + l];
        oo = loverlaps[og];
        // overlap
        i0 = obeads0[oo][0];
        j0 = obeads0[oo][1];
        i1 = obeads1[oo][0];
        j1 = obeads1[oo][1];
        d1 = od1[oo];
        d2 = od2[oo];
        // dot(Dv, n)
        d0 = ((1. - d1) * v[i0][0] + d1 * v[j0][0] -
              ((1. - d2) * v[i1][0] + d2 * v[j1][0])) *
             nx12[oo];
        d0 += ((1. - d1) * v[i0][1] + d1 * v[j0][1] -
               ((1. - d2) * v[i1][1] + d2 * v[j1][1])) *
              ny12[oo];
        d0 += ((1. - d1) * v[i0][2] + d1 * v[j0][2] -
               ((1. - d2) * v[i1][2] + d2 * v[j1][2])) *
              nz12[oo];
        p0 = (Dv[oo] == .0) ? (fabs(d0 - Dv[oo]) < rtol0)
                            : (fabs(d0 - Dv[oo]) < (rtol * fabs(Dv[oo])));
        c0 = (Dv0[og] == .0)
                 ? (fabs(d0 - Dv0[og]) < rtol0)
                 : (fabs(d0 - Dv0[og]) < (rtol_change * fabs(Dv0[og])));
        n0 = (d0 > .0);
        r0 = (d0 < (1.01 * Dv[oo]));
        if (((i0 == s00 && j0 == s01) && (i1 == s10 && j1 == s11) ||
             (i0 == s10 && j0 == s11) && (i1 == s00 && j1 == s01)) &&
            !((p0 || c0) && n0 && r0)) {
          printf("--- s %i graph %i/%i size %i\n", update->ntimestep, g, lgraph,
                 lg);
          printf("overlap: %i and %i\n", og, oo);
          printf("          %i %i - %i %i\n", i0, j0, i1, j1);
          printf("rel vel(%i)=%e -> %e/%e(%i: Lcoll=%e Bcoll=%e)\n", oo,
                 Dv0[og], d0, Dv[oo], sLcoll[og], Lcoll[og], Bcoll[og]);
          printf("       (%i %i %i)(%f) and (%i %i %i)(%f)\n",
                 previous_bond[segment0[oo]], segment0[oo],
                 next_bond[segment0[oo]], d1, previous_bond[segment1[oo]],
                 segment1[oo], next_bond[segment1[oo]], d2);
          printf("       %e %e %e/%i%i%i%i\n", nx12[oo], ny12[oo], nz12[oo], p0,
                 c0, n0, r0);
        }
      }
    }
  }
  // check
  for (int o = 0; o < noverlaps; o++) {
    oo = loverlaps[o];
    i0 = obeads0[oo][0];
    j0 = obeads0[oo][1];
    i1 = obeads1[oo][0];
    j1 = obeads1[oo][1];
    d1 = od1[oo];
    d2 = od2[oo];
    d0 = ((1. - d1) * v[i0][0] + d1 * v[j0][0] -
          ((1. - d2) * v[i1][0] + d2 * v[j1][0])) *
         nx12[oo];
    d0 += ((1. - d1) * v[i0][1] + d1 * v[j0][1] -
           ((1. - d2) * v[i1][1] + d2 * v[j1][1])) *
          ny12[oo];
    d0 += ((1. - d1) * v[i0][2] + d1 * v[j0][2] -
           ((1. - d2) * v[i1][2] + d2 * v[j1][2])) *
          nz12[oo];
    // precision?
    b0 = (Dv[oo] == 0.0) ? (b0 && fabs(d0 - Dv[oo]) < rtol0)
                         : (b0 && fabs(d0 - Dv[oo]) < (rtol * fabs(Dv[oo])));
    // does it change from previous iteration?
    b1 = (Dv0[o] == 0.0)
             ? (b1 && fabs(d0 - Dv0[o]) < rtol0)
             : (b1 && fabs(d0 - Dv0[o]) < (rtol_change * fabs(Dv0[o])));
    // does relative velocity is positive?
    b2 = (b2 && d0 > 0.0);
    b3 = (b3 && d0 < (1.01 * Dv[oo]));
    Dv0[o] = d0;
  }
}

/*! @brief clear graphs
  @param lgraph number of graphs
  @param ligraph number of overlaps per graph
  @param igraph list of overlaps per graph
  @param operg max number of overlaps per graph
 */
void FixVREVC::clear_igraph(int lgraph, int *&ligraph, int *&igraph,
                            int operg) {
  int i0;
#pragma omp parallel for private(i0)
  for (int g = 0; g < lgraph; g++) {
    i0 = ligraph[g];
    for (int i = 0; i < i0; i++)
      igraph[g * operg + i] = -1;
    ligraph[g] = 0;
  }
}

/*! @brief clear J*invM*J^T
  @param JJT J*M^-1*J^T
  @param list of constraints for each constraint
  @param number of constraints per constraint
  @param nperc max number of constraints per constraint
  @param noverlaps number of overlaps
  @param C number of constraints (from bonds)
 */
void FixVREVC::clear_JinvMJT(double *&JJT, int *&JJTc_nzero, int *&lJJT_nzero,
                             int *&lnext, int nperc, int noverlaps, int C) {
  int D = C + noverlaps, L;
#pragma omp parallel for private(L)
  for (int i = 0; i < D; i++) {
    L = lJJT_nzero[i];
    for (int j = 0; j < L; j++) {
      JJT[i * nperc + j] = 0.0;
      JJTc_nzero[i * nperc + j] = -1;
    }
    lJJT_nzero[i] = 0;
    lnext[i] = 0;
  }
}

/*! @brief clear arrays used by the overlaps
 */
void FixVREVC::clear_overlaps() {
  int oo, nall;
  for (int o = 0; o < noverlaps; o++) {
    oo = loverlaps[o];
    pgraph[obeads0[oo][0]] = -1;
    pgraph[obeads0[oo][1]] = -1;
    pgraph[obeads1[oo][0]] = -1;
    pgraph[obeads1[oo][1]] = -1;
  }
  if (idebug == 1) {
    nall = atom->nlocal + atom->nghost;
    for (int p = 0; p < nall; p++) {
      if (pgraph[p] != -1)
        error->all(FLERR, "pgraph is not -1");
    }
  }
}

/*! @brief compute velocities to resolve excluded volume constraints
 */
void FixVREVC::end_of_step() {
#ifdef _OPENMP
  resolve_excluded_volume_constraints_omp(0);
#endif
#ifndef _OPENMP
  resolve_excluded_volume_constraints(0);
#endif
}

/*! @brief compute velocities to resolve excluded volume constraints
 */
void FixVREVC::pre_force(int /*vflag*/) {
#ifdef _OPENMP
  resolve_excluded_volume_constraints_omp(0);
#endif
#ifndef _OPENMP
  resolve_excluded_volume_constraints(0);
#endif
}

/*! @brief compute velocities to resolve excluded volume constraints
 */
void FixVREVC::resolve_excluded_volume_constraints(int /*vflag*/) {
  double **x = atom->x;
  double **v = atom->v;
  double dt = update->dt;
  double ms = 1.;
  int nlocal = atom->nlocal;
  int *type = atom->type;
  tagint *tag = atom->tag;
  int nall = nlocal + atom->nghost;
  int I, J, I0, I1, J0, J1;
  int i, j, si, sj, ii, jj, oo, mi, mj, m0, M0, m1, M1, inum, jnum;
  int iwhile, tmp0, tmp1, tmpi, tmpj, tagi, tagj;
  double dijsq, inv_dij, dij, li, lj;
  bool boverlaps = false, b0, b1, b2, b3;
  double dx, dy, dz, dv, ti, tj;
  double max_overlap = .0, din, oin, maxv = .0;
  if (1 || idebug == 1)
    for (i = 0; i < nall; i++)
      maxv = MAX(maxv, sqrt(v[i][0] * v[i][0] + v[i][1] * v[i][1] +
                            v[i][2] * v[i][2]));

  // neighbor list
  if (idebug == 1)
    printf("\ns=%i neighbor(%i) build ago=%i ncalls=%i\n", update->ntimestep,
           vrevc_irequest, neighbor->ago, neighbor->ncalls);
  neighbor->build_one(list);
  if (idebug == 1)
    printf("neighbor build done\n");
  int *ilist, *jlist, *numneigh, **firstneigh;
  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;
  from_tag_to_bonds();
  if (idebug == 1)
    printf("while ...\n");
  iwhile = 0;
  while (1) {
    // get pairwize of bonds
    noverlaps = 0;
    max_overlap = .0;
    boverlaps = false;
    for (ii = 0; ii < inum; ii++) {
      i = ilist[ii];
      tagi = tag[i];
      jnum = numneigh[i];
      jlist = firstneigh[i];
      for (jj = 0; jj < jnum; jj++) {
        j = jlist[jj];
        j &= NEIGHMASK;
        tagj = tag[j];
        // loop over the bonds for 'tagi'
        for (int a = 0; a < ntag_to_bonds[tagi]; a++) {
          tmpi = tag_to_bonds[tagi][a];
          // loop over the bonds for 'tagj'
          for (int b = 0; b < ntag_to_bonds[tagj]; b++) {
            tmpj = tag_to_bonds[tagj][b];
            if (tmpi == tmpj)
              continue;
            // sort bond index
            if (tmpi > tmpj) {
              I = tmpj;
              J = tmpi;
              si = j;
              sj = i;
            } else {
              I = tmpi;
              J = tmpj;
              si = i;
              sj = j;
            }
            // connectivity ?
            if (next_bond[I] == -1 && previous_bond[I] == -1)
              error->all(FLERR, "error bond/previous are -1");
            if (next_bond[J] == -1 && previous_bond[J] == -1)
              error->all(FLERR, "error bond/previous are -1");
            // exclude two consecutive bonds
            if (next_bond[I] == J || previous_bond[I] == J)
              continue;
            I0 = domain->closest_image(si, atom->map(bond_to_tags[I][0]));
            J0 = domain->closest_image(si, atom->map(bond_to_tags[I][1]));
            I1 = domain->closest_image(sj, atom->map(bond_to_tags[J][0]));
            J1 = domain->closest_image(sj, atom->map(bond_to_tags[J][1]));
            if (I0 == -1 || J0 == -1 || I1 == -1 || J1 == -1)
              error->all(FLERR, "bead from bond is -1");
            // bond length
            dx = x[I0][0] - x[J0][0];
            dy = x[I0][1] - x[J0][1];
            dz = x[I0][2] - x[J0][2];
            li = sqrt(dx * dx + dy * dy + dz * dz);
            dx = x[I1][0] - x[J1][0];
            dy = x[I1][1] - x[J1][1];
            dz = x[I1][2] - x[J1][2];
            lj = sqrt(dx * dx + dy * dy + dz * dz);
            if (idebug == 0 && ((I == s0 && J == s1) || (J == s0 && I == s1))) {
              printf("---(%i,w=%i,i=%i,j=%i,a=%i,b=%i) %i: %i %i, %i: %i %i\n",
                     update->ntimestep, iwhile, si, sj, a, b, I, I0, J0, J, I1,
                     J1);
              printf("   l(%i)=%f l(%i)=%f\n", s0, li, s1, lj);
              printf("   si=%i(%i %i) sj=%i(%i %i)\n", si, I0, J0, sj, I1, J1);
            }
            // mid-to-mid distance
            dx = 0.5 * (x[I0][0] + x[J0][0]) - 0.5 * (x[I1][0] + x[J1][0]);
            dy = 0.5 * (x[I0][1] + x[J0][1]) - 0.5 * (x[I1][1] + x[J1][1]);
            dz = 0.5 * (x[I0][2] + x[J0][2]) - 0.5 * (x[I1][2] + x[J1][2]);
            dijsq = dx * dx + dy * dy + dz * dz;
            dij = 0.5 * li + r0[type[I0]] + 0.5 * lj + r0[type[I1]];
            // no need to check minimal distance against bond diameter
            if (dijsq > (dij * dij))
              continue;
            // using minimal distance with KKT conditions
            getMinDist_KKT(x, dx, dy, dz, ti, tj, I0, J0, I1, J1);
            dijsq = dx * dx + dy * dy + dz * dz;
            if (sqrt(dijsq) < (r0[type[I0]] + r0[type[I1]])) {
              if ((I == s0 && J == s1) || (J == s0 && I == s1))
                printf("   in-min=%f(%f)\n", sqrt(dijsq),
                       1. - sqrt(dijsq) / (r0[type[I0]] + r0[type[I1]]));
              inv_dij = 1. / sqrt(dijsq);
              if (noverlaps < MO) {
                segment0[noverlaps] = I;
                segment1[noverlaps] = J;
                I0 = atom->map(bond_to_tags[I][0]);
                J0 = atom->map(bond_to_tags[I][1]);
                I1 = atom->map(bond_to_tags[J][0]);
                J1 = atom->map(bond_to_tags[J][1]);
                if (I0 == -1 || J0 == -1 || I1 == -1 || J1 == -1)
                  error->all(FLERR, "atom->map gives -1");
                else {
                  obeads0[noverlaps][0] = I0;
                  obeads0[noverlaps][1] = J0;
                  obeads1[noverlaps][0] = I1;
                  obeads1[noverlaps][1] = J1;
                }
                omd[noverlaps] = sqrt(dijsq);
                // position on bond 0
                od1[noverlaps] = ti;
                // position on bond 1
                od2[noverlaps] = tj;
                // normalized minimal distance vector
                nx12[noverlaps] = dx * inv_dij;
                ny12[noverlaps] = dy * inv_dij;
                nz12[noverlaps] = dz * inv_dij;
                oin = 1. - sqrt(dijsq) / (r0[type[I0]] + r0[type[I1]]);
                max_overlap = MAX(max_overlap, oin);
                // store max overlap
                if (max_overlap == oin) {
                  din = sqrt(dijsq);
                  mi = I;
                  mj = J;
                  m0 = I0;
                  M0 = J0;
                  m1 = I1;
                  M1 = J1;
                }
              } else
                boverlaps = true;
              noverlaps++;
            }
            // resize ?
            if (boverlaps) {
              MO = noverlaps + 1;
              memory->grow(ligraph, MO + Mo, "fix:ligraph");
              std::fill_n(ligraph, MO + Mo, 0);
              memory->grow(loverlaps, MO, "fix:loverlaps");
              memory->grow(ioverlaps, MO, "fix:ioverlaps");
              memory->grow(overlap01, MO, "fix:overlap01");
              memory->grow(segment0, MO, "fix:segment0");
              memory->grow(segment1, MO, "fix:segment1");
              memory->grow(obeads0, MO, 2, "fix:obeads0");
              memory->grow(obeads1, MO, 2, "fix:obeads1");
              memory->grow(od1, MO, "fix:od1");
              memory->grow(od2, MO, "fix:od2");
              memory->grow(omd, MO, "fix:omd");
              memory->grow(nx12, MO, "fix:nx12");
              memory->grow(ny12, MO, "fix:ny12");
              memory->grow(nz12, MO, "fix:nz12");
              memory->grow(Dv, MO, "fix:Dv");
              memory->grow(Dv0, MO, "fix:Dv0");
              memory->grow(deja_vu, MO, "fix:deja_vu");
              memory->grow(Jcoll, 12 * (MO + Mo) + 6 * C, "fix:Jcoll");
              std::fill_n(Jcoll, 12 * (MO + Mo) + 6 * C, .0);
              memory->grow(Jcoll_nzero, 12 * (MO + Mo) + 6 * C,
                           "fix:Jcoll_nzero");
              std::fill_n(Jcoll_nzero, 12 * (MO + Mo) + 6 * C, -1);
              memory->grow(next_constraint, (C + MO + Mo) * nperc,
                           "fix:next_constraint");
              memory->grow(lnext, C + MO + Mo, "fix:lnext");
              std::fill_n(lnext, C + MO + Mo, 0);
              memory->grow(Bcoll, C + MO + Mo, "fix:Bcoll");
              std::fill_n(Bcoll, C + MO + Mo, .0);
              memory->grow(Lcoll, C + MO + Mo, "fix:Lcoll");
              std::fill_n(Lcoll, C + MO + Mo, .0);
              memory->grow(sLcoll, C + MO + Mo, "fix:sLcoll");
              std::fill_n(sLcoll, C + MO + Mo, 0);
              memory->grow(JJT, (C + MO + Mo) * nperc, "fix:JJT");
              std::fill_n(JJT, (C + MO + Mo) * nperc, .0);
              memory->grow(JJTc_nzero, (C + MO + Mo) * nperc, "fix:JJTc_nzero");
              std::fill_n(JJTc_nzero, (C + MO + Mo) * nperc, -1);
              memory->grow(lJJT_nzero, C + MO + Mo, "fix:lJJT_nzero");
              std::fill_n(lJJT_nzero, C + MO + Mo, 0);
            }
          }
        }
      }
    }
    iwhile++;
    if (!boverlaps)
      break;
  } // elihw
  if (idebug == 1)
    printf("while done\n");

  if (noverlaps > 0) {
    sort_overlap_index(noverlaps, nlocal, ioverlaps, loverlaps, false);
    // "deja vu"
    deja_vu_collisions(loverlaps, od1, od2, deja_vu, noverlaps);
    build_U_collisions(noverlaps, loverlaps, od1, od2, v, nx12, ny12, nz12, Dv,
                       Dv0);
    lgraph = build_collisions_graphs(noverlaps, loverlaps, od1, od2, pgraph,
                                     ligraph);
    // resize igraph ?
    resize_igraph(lgraph, ligraph, size_igraph, igraph, operg);
    graph_to_overlap(lgraph, noverlaps, operg, loverlaps, od1, od2, pgraph,
                     igraph, ligraph);
    if ((2 * operg + 2) > nperc) {
      nperc = 2 * operg + 2;
      memory->grow(next_constraint, (C + MO + Mo) * nperc,
                   "fix:next_constraint");
      memory->grow(JJT, (C + MO + Mo) * nperc, "fix:JJT");
      memory->grow(JJTc_nzero, (C + MO + Mo) * nperc, "fix:JJTc_nzero");
    }
    // build and resolve
    Jo_linear_chain_velocity_based(noverlaps, loverlaps, od1, od2, nx12, ny12,
                                   nz12, Jcoll, Jcoll_nzero, 0);
    next_overlaps(lgraph, operg, igraph, ligraph, next_constraint, lnext, nperc,
                  C);
    build_JinvMJT(next_constraint, lnext, nperc, Jcoll, Jcoll_nzero, JJT,
                  JJTc_nzero, lJJT_nzero, Lcoll, Bcoll, 0, noverlaps, ms);
    iwhile = 0;
    while (1) {
      add_collisions_to_bs(noverlaps, loverlaps, od1, od2, v, Dv, nx12, ny12,
                           nz12, 1. / dt, Bcoll, sLcoll, 0);
      if (iwhile > 0)
        guess_solution(next_constraint, lnext, nperc, JJT, lJJT_nzero, Lcoll,
                       Bcoll, C, noverlaps);
      // isor =
      // SOR_JinvMJT(JJT,JJTc_nzero,lJJT_nzero,Lcoll,sLcoll,Bcoll,nperc,noverlaps,0,wsor,nsor);
      isor =
          gSOR_JinvMJT(JJT, JJTc_nzero, lJJT_nzero, Lcoll, sLcoll, Bcoll, nperc,
                       noverlaps, wsor, nsor, lgraph, igraph, ligraph, operg);
      if ((update->ntimestep % 1000) == 0 || idebug == 1) {
        printf("%i overlap(s) max(overlap)=%f max(v)*dt=%f isor=%i\n",
               noverlaps, max_overlap, maxv * update->dt, isor);
        printf("%i graph(s) %i overlap(s) per graph\n", lgraph, operg);
        printf("while=%i\n", iwhile);
      }
      // get the forces and add it to the velocities
      JTlambda(Lcoll, Jcoll, Jcoll_nzero, JTl, noverlaps, C);
      add_JTl_to_velocities(dt, ms, v, JTl);
      check_relative_velocity_collisions(noverlaps, loverlaps, od1, od2, nx12,
                                         ny12, nz12, v, Dv, Dv0, vtol, vtol,
                                         vtol, b0, b1, b2, b3);
      if ((b0 || (b1 && iwhile > 0)) && b2 && (b3 || iwhile > 0))
        break;
      iwhile++;
      if (iwhile >= 0)
        break;
    }
    // clean
    if (idebug == 1)
      printf("clean ...\n");
    std::fill_n(Dv, MO, 0.0);
    std::fill_n(Dv0, MO, 0.0);
    std::fill(Lcoll + C, Lcoll + C + MO + Mo, 0.0);
    std::fill(sLcoll + C, sLcoll + C + MO + Mo, 0);
    std::fill(Bcoll + C, Bcoll + C + MO + Mo, 0.0);
    std::fill(Jcoll + 6 * C, Jcoll + 6 * C + 12 * (MO + Mo), 0.0);
    std::fill(Jcoll_nzero + 6 * C, Jcoll_nzero + 6 * C + 12 * (MO + Mo), -1);
    clear_igraph(lgraph, ligraph, igraph, operg);
    clear_JinvMJT(JJT, JJTc_nzero, lJJT_nzero, lnext, nperc, noverlaps, C);
    clear_overlaps();
    if (idebug == 1)
      printf("clean done\n");
    // does the max overlap exceed threshold ?
    if (max_overlap > oexit) {
      printf("\n%i: exit\n", update->ntimestep);
      printf("(b) %i %i %i %i\n", m0, M0, m1, M1);
      printf("(s) %i %i -> %f(%f), dt=%e\n", mi, mj, din, max_overlap,
             update->dt);
      printf("    from %i to next     %i\n", mi, next_bond[mi]);
      printf("    from %i to previous %i\n", mi, previous_bond[mi]);
      printf("    from %i to next     %i\n", mj, next_bond[mj]);
      printf("    from %i to previous %i\n", mj, previous_bond[mj]);
      printf("max(l)=%f, dt*max(v)=%f\n", maxl, (update->dt) * maxv);
      for (int t = 0; t < atom->ntypes; t++)
        printf("r[%i]=%f\n", type[t], r0[type[t]]);
      error->all(FLERR, "overlap too big, exit");
    }
  }
}

/*! @brief compute velocities to resolve excluded volume constraints (OpenMP)
 */
void FixVREVC::resolve_excluded_volume_constraints_omp(int /*vflag*/) {
  double **x = atom->x;
  double **v = atom->v;
  double dt = update->dt;
  double ms = 1.;
  int nlocal = atom->nlocal;
  int *type = atom->type;
  tagint *tag = atom->tag;
  int nall = nlocal + atom->nghost;
  int I, J, I0, I1, J0, J1;
  int i, j, si, sj, ii, jj, oo, mi, mj, m0, M0, m1, M1, inum, jnum;
  int iwhile, tmp0, tmp1, tmpi, tmpj, tagi, tagj;
  double sqrtdij, dijsq, inv_dij, dij, li, lj;
  bool b0, b1, b2, b3;
  double dx, dy, dz, dv, ti, tj;
  double max_overlap = .0, din, oin, maxv = .0;
  if (1 || idebug == 1)
    for (i = 0; i < nall; i++)
      maxv = MAX(maxv, sqrt(v[i][0] * v[i][0] + v[i][1] * v[i][1] +
                            v[i][2] * v[i][2]));

  // neighbor list
  if (idebug == 1)
    printf("\ns=%i neighbor(%i) build ago=%i ncalls=%i\n", update->ntimestep,
           vrevc_irequest, neighbor->ago, neighbor->ncalls);
  neighbor->build_one(list);
  if (idebug == 1)
    printf("neighbor build done\n");
  int *ilist, *jlist, *numneigh, **firstneigh;
  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;
  from_tag_to_bonds();
  if (idebug == 1)
    printf("while ...\n");
  iwhile = 0;
  while (1) {
    // get pairwize of bonds
    noverlaps = ncompute_ij = 0;
    max_overlap = .0;
    for (ii = 0; ii < inum; ii++) {
      i = ilist[ii];
      tagi = tag[i];
      jnum = numneigh[i];
      jlist = firstneigh[i];
      for (jj = 0; jj < jnum; jj++) {
        j = jlist[jj];
        j &= NEIGHMASK;
        tagj = tag[j];
        // loop over the bonds for 'tagi'
        for (int a = 0; a < ntag_to_bonds[tagi]; a++) {
          tmpi = tag_to_bonds[tagi][a];
          // loop over the bonds for 'tagj'
          for (int b = 0; b < ntag_to_bonds[tagj]; b++) {
            tmpj = tag_to_bonds[tagj][b];
            if (tmpi == tmpj)
              continue;
            // sort bond index
            if (tmpi > tmpj) {
              I = tmpj;
              J = tmpi;
              si = j;
              sj = i;
            } else {
              I = tmpi;
              J = tmpj;
              si = i;
              sj = j;
            }
            // connectivity ?
            if (next_bond[I] == -1 && previous_bond[I] == -1)
              error->all(FLERR, "error bond/previous are -1");
            if (next_bond[J] == -1 && previous_bond[J] == -1)
              error->all(FLERR, "error bond/previous are -1");
            // exclude two consecutive bonds
            if (next_bond[I] == J || previous_bond[I] == J)
              continue;
            I0 = domain->closest_image(si, atom->map(bond_to_tags[I][0]));
            J0 = domain->closest_image(si, atom->map(bond_to_tags[I][1]));
            I1 = domain->closest_image(sj, atom->map(bond_to_tags[J][0]));
            J1 = domain->closest_image(sj, atom->map(bond_to_tags[J][1]));
            if (I0 == -1 || J0 == -1 || I1 == -1 || J1 == -1)
              error->all(FLERR, "bead from bond is -1");
            // bond length
            dx = x[I0][0] - x[J0][0];
            dy = x[I0][1] - x[J0][1];
            dz = x[I0][2] - x[J0][2];
            li = sqrt(dx * dx + dy * dy + dz * dz);
            dx = x[I1][0] - x[J1][0];
            dy = x[I1][1] - x[J1][1];
            dz = x[I1][2] - x[J1][2];
            lj = sqrt(dx * dx + dy * dy + dz * dz);
            if (idebug == 0 && ((I == s0 && J == s1) || (J == s0 && I == s1))) {
              printf("---(%i,w=%i,i=%i,j=%i,a=%i,b=%i) %i: %i %i, %i: %i %i\n",
                     update->ntimestep, iwhile, si, sj, a, b, I, I0, J0, J, I1,
                     J1);
              printf("   l(%i)=%f l(%i)=%f\n", s0, li, s1, lj);
              printf("   si=%i(%i %i) sj=%i(%i %i)\n", si, I0, J0, sj, I1, J1);
            }
            // mid-to-mid distance
            dx = 0.5 * (x[I0][0] + x[J0][0]) - 0.5 * (x[I1][0] + x[J1][0]);
            dy = 0.5 * (x[I0][1] + x[J0][1]) - 0.5 * (x[I1][1] + x[J1][1]);
            dz = 0.5 * (x[I0][2] + x[J0][2]) - 0.5 * (x[I1][2] + x[J1][2]);
            dijsq = dx * dx + dy * dy + dz * dz;
            dij = 0.5 * li + r0[type[I0]] + 0.5 * lj + r0[type[I1]];
            // no need to check minimal distance against bond diameter
            if (dijsq > (dij * dij))
              continue;
            else {
              if (ncompute_ij < mcompute_ij) {
                compute_ij[ncompute_ij][0] = I;
                compute_ij[ncompute_ij][1] = I0;
                compute_ij[ncompute_ij][2] = J0;
                compute_ij[ncompute_ij][3] = J;
                compute_ij[ncompute_ij][4] = I1;
                compute_ij[ncompute_ij][5] = J1;
              }
              ncompute_ij++;
            }
          }
        }
      }
    } // rof
    if (ncompute_ij < mcompute_ij)
      break;
    else {
      mcompute_ij = ncompute_ij + 100;
      memory->grow(compute_ij, mcompute_ij, 7, "fix:compute_ij");
      memory->grow(store_ij, mcompute_ij, 6, "fix:store_ij");
    }
    iwhile++;
  } // elihw
  if (idebug == 1)
    printf("while done\n");
    // printf("c=%i/%i\n",ncompute_ij,mcompute_ij);
    // #pragma omp parallel for private(I0, J0, I1, J1, sqrtdij, dx, dy, dz, ti,
    // tj)
#pragma omp parallel for private(I0, I1, sqrtdij, dx, dy, dz, ti, tj)
  // #pragma omp simd private(I0, I1, sqrtdij, dx, dy, dz, ti, tj)
  for (int c = 0; c < ncompute_ij; c += 32) {
    for (int d = 0; d < 32; d++) {
      if ((c + d) < ncompute_ij) {
        I0 = compute_ij[c + d][1];
        // J0 = compute_ij[c + d][2];
        I1 = compute_ij[c + d][4];
        // J1 = compute_ij[c + d][5];
        // compute minimal distance with KKT conditions
        getMinDist_KKT(x, dx, dy, dz, ti, tj, I0, compute_ij[c + d][2], I1,
                       compute_ij[c + d][5]);
        sqrtdij = sqrt(dx * dx + dy * dy + dz * dz);
        if (sqrtdij < (r0[type[I0]] + r0[type[I1]])) {
          compute_ij[c + d][6] = 1;
          store_ij[c + d][0] = sqrtdij;
          store_ij[c + d][1] = ti;
          store_ij[c + d][2] = tj;
          sqrtdij = 1.0 / sqrtdij;
          store_ij[c + d][3] = dx * sqrtdij;
          store_ij[c + d][4] = dy * sqrtdij;
          store_ij[c + d][5] = dz * sqrtdij;
        } else
          compute_ij[c + d][6] = 0;
      }
    }
  }
  // number of overlaps
  noverlaps = 0;
  for (int c = 0; c < ncompute_ij; c++)
    noverlaps += compute_ij[c][6];
  // printf("o=%i\n",noverlaps);
  // resize ?
  if (noverlaps >= MO) {
    MO = noverlaps + 1;
    memory->grow(ligraph, MO + Mo, "fix:ligraph");
    std::fill_n(ligraph, MO + Mo, 0);
    memory->grow(loverlaps, MO, "fix:loverlaps");
    memory->grow(ioverlaps, MO, "fix:ioverlaps");
    memory->grow(overlap01, MO, "fix:overlap01");
    memory->grow(segment0, MO, "fix:segment0");
    memory->grow(segment1, MO, "fix:segment1");
    memory->grow(obeads0, MO, 2, "fix:obeads0");
    memory->grow(obeads1, MO, 2, "fix:obeads1");
    memory->grow(od1, MO, "fix:od1");
    memory->grow(od2, MO, "fix:od2");
    memory->grow(omd, MO, "fix:omd");
    memory->grow(nx12, MO, "fix:nx12");
    memory->grow(ny12, MO, "fix:ny12");
    memory->grow(nz12, MO, "fix:nz12");
    memory->grow(Dv, MO, "fix:Dv");
    memory->grow(Dv0, MO, "fix:Dv0");
    memory->grow(deja_vu, MO, "fix:deja_vu");
    memory->grow(Jcoll, 12 * (MO + Mo) + 6 * C, "fix:Jcoll");
    std::fill_n(Jcoll, 12 * (MO + Mo) + 6 * C, 0.0);
    memory->grow(Jcoll_nzero, 12 * (MO + Mo) + 6 * C, "fix:Jcoll_nzero");
    std::fill_n(Jcoll_nzero, 12 * (MO + Mo) + 6 * C, -1);
    memory->grow(next_constraint, (C + MO + Mo) * nperc, "fix:next_constraint");
    memory->grow(lnext, C + MO + Mo, "fix:lnext");
    std::fill_n(lnext, C + MO + Mo, 0);
    memory->grow(Bcoll, C + MO + Mo, "fix:Bcoll");
    std::fill_n(Bcoll, C + MO + Mo, 0.0);
    memory->grow(Lcoll, C + MO + Mo, "fix:Lcoll");
    std::fill_n(Lcoll, C + MO + Mo, 0.0);
    memory->grow(sLcoll, C + MO + Mo, "fix:sLcoll");
    std::fill_n(sLcoll, C + MO + Mo, 0);
    memory->grow(JJT, (C + MO + Mo) * nperc, "fix:JJT");
    std::fill_n(JJT, (C + MO + Mo) * nperc, 0.0);
    memory->grow(JJTc_nzero, (C + MO + Mo) * nperc, "fix:JJTc_nzero");
    std::fill_n(JJTc_nzero, (C + MO + Mo) * nperc, -1);
    memory->grow(lJJT_nzero, C + MO + Mo, "fix:lJJT_nzero");
    std::fill_n(lJJT_nzero, C + MO + Mo, 0);
  }
  // keep only minimal distance < 2 * bond radius
  noverlaps = 0;
  for (int c = 0; c < ncompute_ij; c++) {
    if (compute_ij[c][6] == 1) {
      I = compute_ij[c][0];
      I0 = compute_ij[c][1];
      J0 = compute_ij[c][2];
      J = compute_ij[c][3];
      I1 = compute_ij[c][4];
      J1 = compute_ij[c][5];
      //
      segment0[noverlaps] = I;
      segment1[noverlaps] = J;
      I0 = atom->map(bond_to_tags[I][0]);
      J0 = atom->map(bond_to_tags[I][1]);
      I1 = atom->map(bond_to_tags[J][0]);
      J1 = atom->map(bond_to_tags[J][1]);
      if (I0 == -1 || J0 == -1 || I1 == -1 || J1 == -1)
        error->all(FLERR, "atom->map gives -1");
      else {
        obeads0[noverlaps][0] = I0;
        obeads0[noverlaps][1] = J0;
        obeads1[noverlaps][0] = I1;
        obeads1[noverlaps][1] = J1;
      }
      omd[noverlaps] = store_ij[c][0];
      // position on bond 0
      od1[noverlaps] = store_ij[c][1];
      // position on bond 1
      od2[noverlaps] = store_ij[c][2];
      // normalized minimal distance vector
      nx12[noverlaps] = store_ij[c][3];
      ny12[noverlaps] = store_ij[c][4];
      nz12[noverlaps] = store_ij[c][5];
      oin = 1.0 - omd[noverlaps] / (r0[type[I0]] + r0[type[I1]]);
      max_overlap = MAX(max_overlap, oin);
      // store max overlap
      if (max_overlap == oin) {
        din = sqrt(dijsq);
        mi = I;
        mj = J;
        m0 = I0;
        M0 = J0;
        m1 = I1;
        M1 = J1;
      }
      noverlaps++;
    }
  }

  if (noverlaps > 0) {
    sort_overlap_index(noverlaps, nlocal, ioverlaps, loverlaps, false);
    // "deja vu"
    deja_vu_collisions(loverlaps, od1, od2, deja_vu, noverlaps);
    build_U_collisions(noverlaps, loverlaps, od1, od2, v, nx12, ny12, nz12, Dv,
                       Dv0);
    lgraph = build_collisions_graphs(noverlaps, loverlaps, od1, od2, pgraph,
                                     ligraph);
    // resize igraph ?
    resize_igraph(lgraph, ligraph, size_igraph, igraph, operg);
    graph_to_overlap(lgraph, noverlaps, operg, loverlaps, od1, od2, pgraph,
                     igraph, ligraph);
    if ((2 * operg + 2) > nperc) {
      nperc = 2 * operg + 2;
      memory->grow(next_constraint, (C + MO + Mo) * nperc,
                   "fix:next_constraint");
      memory->grow(JJT, (C + MO + Mo) * nperc, "fix:JJT");
      memory->grow(JJTc_nzero, (C + MO + Mo) * nperc, "fix:JJTc_nzero");
    }
    // build and resolve
    Jo_linear_chain_velocity_based(noverlaps, loverlaps, od1, od2, nx12, ny12,
                                   nz12, Jcoll, Jcoll_nzero, 0);
    next_overlaps(lgraph, operg, igraph, ligraph, next_constraint, lnext, nperc,
                  C);
    build_JinvMJT(next_constraint, lnext, nperc, Jcoll, Jcoll_nzero, JJT,
                  JJTc_nzero, lJJT_nzero, Lcoll, Bcoll, 0, noverlaps, ms);
    iwhile = 0;
    while (1) {
      add_collisions_to_bs(noverlaps, loverlaps, od1, od2, v, Dv, nx12, ny12,
                           nz12, 1. / dt, Bcoll, sLcoll, 0);
      if (iwhile > 0)
        guess_solution(next_constraint, lnext, nperc, JJT, lJJT_nzero, Lcoll,
                       Bcoll, C, noverlaps);
      // isor =
      // SOR_JinvMJT(JJT,JJTc_nzero,lJJT_nzero,Lcoll,sLcoll,Bcoll,nperc,noverlaps,0,wsor,nsor);
      isor =
          gSOR_JinvMJT(JJT, JJTc_nzero, lJJT_nzero, Lcoll, sLcoll, Bcoll, nperc,
                       noverlaps, wsor, nsor, lgraph, igraph, ligraph, operg);
      if ((update->ntimestep % 1000) == 0 || idebug == 1) {
        printf("%i overlap(s) max(overlap)=%f max(v)*dt=%f isor=%i\n",
               noverlaps, max_overlap, maxv * update->dt, isor);
        printf("%i graph(s) %i overlap(s) per graph\n", lgraph, operg);
        printf("while=%i\n", iwhile);
      }
      // get the forces and add it to the velocities
      JTlambda(Lcoll, Jcoll, Jcoll_nzero, JTl, noverlaps, C);
      add_JTl_to_velocities(dt, ms, v, JTl);
      check_relative_velocity_collisions(noverlaps, loverlaps, od1, od2, nx12,
                                         ny12, nz12, v, Dv, Dv0, vtol, vtol,
                                         vtol, b0, b1, b2, b3);
      if ((b0 || (b1 && iwhile > 0)) && b2 && (b3 || iwhile > 0))
        break;
      iwhile++;
      if (iwhile >= 0)
        break;
    }
    // clean
    if (idebug == 1)
      printf("clean ...\n");
    std::fill_n(Dv, MO, 0.0);
    std::fill_n(Dv0, MO, 0.0);
    std::fill(Lcoll + C, Lcoll + C + MO + Mo, 0.0);
    std::fill(sLcoll + C, sLcoll + C + MO + Mo, 0);
    std::fill(Bcoll + C, Bcoll + C + MO + Mo, 0.0);
    std::fill(Jcoll + 6 * C, Jcoll + 6 * C + 12 * (MO + Mo), 0.0);
    std::fill(Jcoll_nzero + 6 * C, Jcoll_nzero + 6 * C + 12 * (MO + Mo), -1);
    clear_igraph(lgraph, ligraph, igraph, operg);
    clear_JinvMJT(JJT, JJTc_nzero, lJJT_nzero, lnext, nperc, noverlaps, C);
    clear_overlaps();
    if (idebug == 1)
      printf("clean done\n");
    // does the max overlap exceed threshold ?
    if (max_overlap > oexit) {
      printf("\n%i: exit\n", update->ntimestep);
      printf("(b) %i %i %i %i\n", m0, M0, m1, M1);
      printf("(s) %i %i -> %f(%f), dt=%e\n", mi, mj, din, max_overlap,
             update->dt);
      printf("    from %i to next     %i\n", mi, next_bond[mi]);
      printf("    from %i to previous %i\n", mi, previous_bond[mi]);
      printf("    from %i to next     %i\n", mj, next_bond[mj]);
      printf("    from %i to previous %i\n", mj, previous_bond[mj]);
      printf("max(l)=%f, dt*max(v)=%f\n", maxl, (update->dt) * maxv);
      for (int t = 0; t < atom->ntypes; t++)
        printf("r[%i]=%f\n", type[t], r0[type[t]]);
      error->all(FLERR, "overlap too big, exit");
    }
  }
}

/* -------------------------------------------------------------------------
   maxsize of any atom's restart data
   ------------------------------------------------------------------------- */
int FixVREVC::maxsize_restart() { return 3; }

/* -------------------------------------------------------------------------
   size of atom nlocal's restart data
   ------------------------------------------------------------------------- */
int FixVREVC::size_restart(int /*nlocal*/) { return 3; }

/* -------------------------------------------------------------------------
   pack global state of Fix
   ------------------------------------------------------------------------- */
void FixVREVC::write_restart(FILE *fp) {
  int n = 0;
  double list[1];
  list[n++] = comm->cutghostuser;
  if (comm->me == 0) {
    int size = n * sizeof(double);
    fwrite(&size, sizeof(int), 1, fp);
    fwrite(list, sizeof(double), n, fp);
  }
}

/* -------------------------------------------------------------------------
   use info from restart file to restart the Fix
   ------------------------------------------------------------------------- */
void FixVREVC::restart(char *buf) {
  int n = 0;
  double *list = (double *)buf;
  comm->cutghostuser = static_cast<double>(list[n++]);
}
