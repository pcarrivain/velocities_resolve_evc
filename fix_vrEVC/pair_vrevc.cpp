/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------
   Contributing authors: Pascal Carrivain (ENS Lyon)

   This pair style vrevc command changes relative velocity along normal
   to the contact between two bonds. This is useful for preventing the
   crossing of bonds when polymer topology matters.

   See the doc page for pair_style vrevc command for usage instructions (WIP).

   There is an example script for this package in examples/USER/vrevc.

   Please contact Pascal Carrivain for questions (p.carrivain@gmail.com).
   ------------------------------------------------------------------------- */

#include "pair_vrevc.h"
#include "atom.h"
#include "citeme.h"
#include "comm.h"
#include "error.h"
#include "fix.h"
#include "fix_vrevc.h"
#include "force.h"
#include "memory.h"
#include "modify.h"
#include "neigh_list.h"
#include "neighbor.h"
#include "output.h"
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <mpi.h>

using namespace LAMMPS_NS;

static const char cite_vrevc[] = "@Article{Carrivain2020\n"
                                 "publication is not yet available\n"
                                 "}\n\n";

static int vrevc_instance = 0;

/* ----------------------------------------------------------------------
   set size of pair comms in constructor
   ---------------------------------------------------------------------- */
PairVREVC::PairVREVC(LAMMPS *lmp) : Pair(lmp) {
  writedata = 1;
  // single_enable = 0;
  nextra = 1;

  if (lmp->citeme)
    lmp->citeme->add(cite_vrevc);

  // look for the vrevc fix id
  int ifix = -1;
  for (int i = 0; i < modify->nfix; i++) {
    if (strcmp(modify->fix[i]->style, "vrevc") == 0) {
      ifix = i;
      fix_id = modify->fix[i]->id;
      f_vrevc = (FixVREVC *)modify->fix[i];
    }
  }
  if (0) {
    if (ifix == -1) {
      // generate unique fix-id for this pair style instance
      fix_id = strdup("XX_FIX_VREVC");
      fix_id[0] = '0' + vrevc_instance / 10;
      fix_id[1] = '0' + vrevc_instance % 10;
      ++vrevc_instance;
      // create fix VREVC instance here
      char **fixarg = new char *[13];
      fixarg[0] = fix_id;
      fixarg[1] = (char *)"all";
      fixarg[2] = (char *)"vrevc";
      fixarg[3] = (char *)"w_sor";
      fixarg[4] = (char *)"1.3";
      fixarg[5] = (char *)"n_sor";
      fixarg[6] = (char *)"10000";
      fixarg[7] = (char *)"bond_radius";
      fixarg[8] = (char *)"0.1";
      fixarg[9] = (char *)"cut_global";
      fixarg[10] = (char *)"0.9";
      fixarg[11] = (char *)"debug";
      fixarg[12] = (char *)"1";
      modify->add_fix(13, fixarg);
      f_vrevc = (FixVREVC *)modify->fix[modify->nfix - 1];
      delete[] fixarg;
    } else
      error->all(
          FLERR,
          "Pair_style vrevc cannot create fix vrevc because it already exists");
  } else
    fix_id = (char *)"vrevc";
}

/* -------------------------------------------------------------------------
   allocate all arrays
   ------------------------------------------------------------------------- */
void PairVREVC::allocate() {
  allocated = 1;
  int n = atom->ntypes;
  memory->create(cut, n + 1, n + 1, "pair:cut_vrevc");
  memory->create(cutsq, n + 1, n + 1, "pair:cutsq");
  memory->create(r0, n + 1, n + 1, "pair:r0_vrevc");
  // setflag for atom types
  memory->create(setflag, n + 1, n + 1, "pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++)
      setflag[i][j] = 0;
}

/* -------------------------------------------------------------------------
   free
   ------------------------------------------------------------------------- */
PairVREVC::~PairVREVC() {
  if (allocated) {
    memory->destroy(cut);
    memory->destroy(cutsq);
    memory->destroy(r0);
    memory->destroy(setflag);
  }
  // check nfix in case all fixes have already been deleted
  if (0) {
    if (modify->fix[modify->find_fix(fix_id)])
      modify->delete_fix(fix_id);
    free(fix_id);
  }
}

/* ----------------------------------------------------------------------
   compute
   ---------------------------------------------------------------------- */
void PairVREVC::compute(int eflag, int vflag) {
  // setup energy and virial
  ev_init(eflag, vflag);
  // neighbor->build(0);
  // int inum;
  // int *ilist, *jlist, *numneigh, **firstneigh;
  // inum = list->inum;
  // ilist = list->ilist;
  // numneigh = list->numneigh;
  // firstneigh = list->firstneigh;
}

/* ----------------------------------------------------------------------
   global settings
   ---------------------------------------------------------------------- */
void PairVREVC::settings(int narg, char **arg) {
  // checks
  if (narg != 3)
    error->all(FLERR,
               "Pair_style vrevc requires cut_global, w_sor and n_sor values");
  if (force->pair_match("hybrid", 1) == NULL &&
      force->pair_match("hybrid/overlay", 1) == NULL)
    error->all(FLERR, "Cannot use pair vrevc without pair_style hybrid");
  if (atom->tag_enable == 0)
    error->all(FLERR, "Pair_style vrevc requires atom IDs");
  // get parameters
  cut_global = force->numeric(FLERR, arg[0]);
  if (cut_global < .0)
    error->all(FLERR, "Illegal cut_global value for vrevc, has to be > 0");
  w_sor = force->numeric(FLERR, arg[1]);
  if (w_sor < .0 || w_sor > 2.)
    error->all(FLERR, "Illegal w_sor value for vrevc, has to be 0 < w_sor < 2");
  n_sor = force->inumeric(FLERR, arg[2]);
  if (n_sor < 0)
    error->all(FLERR, "Illegal n_sor value for vrevc, n_sor has to be > 0");
  // reset cutoffs if explicitly set
  if (allocated) {
    int i, j;
    for (i = 1; i <= atom->ntypes; i++) {
      for (j = i; j <= atom->ntypes; j++) {
        if (setflag[i][j])
          cut[i][j] = cut_global;
      }
    }
  }
}

/* -------------------------------------------------------------------------
   set coeffs
   ------------------------------------------------------------------------- */
void PairVREVC::coeff(int narg, char **arg) {
  if (narg != 3)
    error->all(FLERR, "PairVREVC: Incorrect args for pair coeff");
  if (!allocated)
    allocate();

  double bond_radius = force->numeric(FLERR, arg[2]);
  int ilo, ihi, jlo, jhi, count = 0;
  force->bounds(FLERR, arg[0], atom->ntypes, ilo, ihi);
  force->bounds(FLERR, arg[1], atom->ntypes, jlo, jhi);
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo, i); j <= jhi; j++) {
      cut[i][j] = cut_global;
      r0[i][j] = bond_radius;
      setflag[i][j] = 1;
      count++;
    }
  }

  if (count == 0)
    error->all(FLERR, "PairVREVC: Incorrect args for pair coefficients");
}

/* ----------------------------------------------------------------------
   init specific to this pair style
   ---------------------------------------------------------------------- */
void PairVREVC::init_style() {
  if (comm->ghost_velocity == 0)
    error->all(FLERR, "Pair vrevc requires ghost atoms store velocity");
  // verify that fix VREVC is still defined and has not been changed.
  int ifix = modify->find_fix(fix_id);
  if (ifix != -1)
    if (f_vrevc != (FixVREVC *)modify->fix[ifix])
      error->all(FLERR, "Fix VREVC has been changed unexpectedly");
  int irequest = neighbor->request(this, instance_me);
  // neighbor->cutneighmax = cut_global;
  // neighbor->binsize_user = 1.2 * cut_global;
}

/* -------------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
   ------------------------------------------------------------------------- */
double PairVREVC::init_one(int i, int j) {
  if (setflag[i][j] == 0)
    error->all(FLERR, "PairVREVC: All pair coeffs are not set");
  cut[j][i] = cut[i][j];
  r0[j][i] = r0[i][j];
  return cut[i][j];
}

/* -------------------------------------------------------------------------
   proc 0 writes to data file
   ------------------------------------------------------------------------- */
void PairVREVC::write_data(FILE *fp) {
  for (int i = 1; i <= atom->ntypes; i++)
    fprintf(fp, "%d %g\n", i, r0[i][i]);
}

/* -------------------------------------------------------------------------
   proc 0 writes all pairs to data file
   ------------------------------------------------------------------------- */
void PairVREVC::write_data_all(FILE *fp) {
  for (int i = 1; i <= atom->ntypes; i++)
    for (int j = i; j <= atom->ntypes; j++)
      fprintf(fp, "%d %d %g %g\n", i, j, r0[i][j], cut[i][j]);
}

/* -------------------------------------------------------------------------
   proc 0 writes to restart file
   ------------------------------------------------------------------------- */
void PairVREVC::write_restart(FILE *fp) {
  write_restart_settings(fp);
  int i, j;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      fwrite(&setflag[i][j], sizeof(int), 1, fp);
      if (setflag[i][j]) {
        fwrite(&r0[i][j], sizeof(double), 1, fp);
        fwrite(&cut[i][j], sizeof(double), 1, fp);
      }
    }
}

/* -------------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
   ------------------------------------------------------------------------- */
void PairVREVC::read_restart(FILE *fp) {
  read_restart_settings(fp);
  allocate();

  int i, j;
  int me = comm->me;
  for (i = 1; i <= atom->ntypes; i++)
    for (j = i; j <= atom->ntypes; j++) {
      if (me == 0)
        fread(&setflag[i][j], sizeof(int), 1, fp);
      MPI_Bcast(&setflag[i][j], 1, MPI_INT, 0, world);
      if (setflag[i][j]) {
        if (me == 0) {
          printf(" i %d j %d \n", i, j);
          fread(&r0[i][j], sizeof(double), 1, fp);
          fread(&cut[i][j], sizeof(double), 1, fp);
        }
        MPI_Bcast(&r0[i][j], 1, MPI_DOUBLE, 0, world);
        MPI_Bcast(&cut[i][j], 1, MPI_DOUBLE, 0, world);
      }
    }
}

/* -------------------------------------------------------------------------
   proc 0 writes to restart file
   ------------------------------------------------------------------------- */
void PairVREVC::write_restart_settings(FILE *fp) {
  fwrite(&cut_global, sizeof(double), 1, fp);
}

/* -------------------------------------------------------------------------
   proc 0 reads from restart file, bcasts
   ------------------------------------------------------------------------- */
void PairVREVC::read_restart_settings(FILE *fp) {
  if (comm->me == 0)
    fread(&cut_global, sizeof(double), 1, fp);
  MPI_Bcast(&cut_global, 1, MPI_DOUBLE, 0, world);
}
