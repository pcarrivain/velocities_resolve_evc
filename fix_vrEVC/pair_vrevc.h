/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(vrevc, PairVREVC)

#else

#ifndef LMP_PAIR_VREVC_H
#define LMP_PAIR_VREVC_H

#include "pair.h"

namespace LAMMPS_NS {

class PairVREVC : public Pair {
public:
  PairVREVC(class LAMMPS *);
  virtual ~PairVREVC();
  virtual void compute(int, int);
  virtual void settings(int, char **);
  virtual void coeff(int, char **);
  void init_style();
  double init_one(int, int);
  virtual void write_data(FILE *);
  virtual void write_data_all(FILE *);
  virtual void write_restart(FILE *);
  virtual void read_restart(FILE *);
  virtual void write_restart_settings(FILE *);
  virtual void read_restart_settings(FILE *);

protected:
  void allocate();
  double **cut;
  double **r0;
  double cut_global;
  int n_sor = 1000;
  double w_sor = 1.3;
  class Fix *f_vrevc;
  char *fix_id;
};

} // namespace LAMMPS_NS

#endif
#endif

    /* ERROR/WARNING messages:

    */
