#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import getopt
import numpy as np

def main(argv):
    C=30
    N=100
    lbond=1.0
    rbond=0.25
    seed=1
    rerun=0
    thermostat='gjf'
    pair_style='lj'
    metrology=False
    implementation="g++_openmpi"
    options=""
    mpi_tasks=1
    OMP=1
    try:
        opts,args=getopt.getopt(argv,"hC:N:o:s:t:m:R:T:I:O:",["chains=","chain_length=","omp=","seed=","thermostat=","lbond=","rbond=","metrology=","rerun=","mpi_tasks=","implementation=","options="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('python3 make_model.py -C <number of chains along x>')
            print('                      -N <chain length>')
            print('                      -o <omp>')
            print('                      -s <seed>')
            print('                      -t <thermostat(nve,nvt,gjf)>')
            print('                      -m <metrology>')
            print('                      -R <rerun>')
            print('                      -T <mpi_tasks>')
            print('                      -I <implementation>')
            print('                      -O <options>')
            sys.exit()
        elif opt=="-N" or opt=="--chain_length":
            N=int(arg)
        elif opt=="-C" or opt=="--chains":
            C=int(arg)
        elif opt=="-o" or opt=="--omp":
            OMP=int(arg)
        elif opt=="-s" or opt=="--seed":
            seed=int(arg)
        elif opt=="-t" or opt=="--thermostat":
            thermostat=str(arg)
        elif opt=="--lbond":
            lbond=float(arg)
        elif opt=="--rbond":
            rbond=float(arg)
        elif opt in ("-c","--pair_style"):
            pair_style=str(arg)
        elif opt=="-m" or opt=="--metrology":
            metrology=bool(str(arg)=='yes')
        elif opt=="-R" or opt=="--rerun":
            rerun=int(arg)
        elif opt=="-T" or opt=="--mpi_tasks":
            mpi_tasks=int(arg)
        elif opt=="-I" or opt=="--implementation":
            implementation=arg
        elif opt=="-O" or opt=="--options":
            options=arg
    OMP_name=["","/omp"][int(OMP>1)]

    # variables for the lammps input file
    bead_density=0.85/(lbond*lbond*lbond)
    Temp=300.0# K
    fkB=1.38*1e-23# k_B scale (kg.m^2.s^-2.K^-1)
    kBT=fkB*Temp
    fE=kBT# J
    kBT=kBT/fE
    Elj=kBT# lj epsilon
    Temp/=(fE/fkB)
    mass=1.0
    kappa=0.0#m*kBT# bending strength
    Ob=180.0# bending equilibrium angle
    K_fene=(30.0*Elj)/(lbond*lbond)
    R0=1.5*lbond# R0 for the FENE potential
    vt=np.sqrt(kBT/mass)
    tau=lbond*np.sqrt(mass/Elj)
    skin=0.3*lbond
    vcut=0.5*1.5+rbond
    # chains on lattice
    xoffset=2.0*rbond
    yoffset,zoffset=xoffset,xoffset
    dC=N*lbond/C
    while dC<=rbond:
        C-=1
        dC=N*lbond/C
    Lx=2.0*xoffset+C*dC
    Ly=2.0*yoffset+C*dC
    Lz=2.0*zoffset+N*lbond
    A,B=C*C*(N+1),C*C*N

    # writing the lammps read_data file
    with open("data.fix_vrevc","w") as out_file:
        out_file.write("LAMMPS Description\n\n")
        out_file.write(str(A)+" atoms\n")
        out_file.write("1 atom types\n")
        out_file.write(str(B)+" bonds\n")
        out_file.write("1 bond types\n\n")
        Lmax=max(Lx,max(Ly,Lz))
        Lx,Ly,Lz=Lmax,Lmax,Lmax
        out_file.write(str(-0.5*Lx)+" "+str(0.5*Lx)+" xlo xhi\n")
        out_file.write(str(-0.5*Ly)+" "+str(0.5*Ly)+" ylo yhi\n")
        out_file.write(str(-0.5*Lz)+" "+str(0.5*Lz)+" zlo zhi\n\n")
        out_file.write("Masses\n\n")
        out_file.write("1 "+str(mass)+"\n\n")
        out_file.write("Atoms\n\n")
        # positions and com
        positions=np.zeros((A,3))
        index=0
        for x in range(C):
            for y in range(C):
                for n in range(N+1):
                    positions[index,:3]=xoffset+x*dC,yoffset+y*dC,lbond+n*lbond
                    index+=1
        com=np.multiply(1.0/(C*C*(N+1)),np.sum(positions,axis=0))
        # add atoms
        atoms=0
        for i,p in enumerate(positions):
            out_file.write(str(i+1)+" 0 1 "+str(p[0]-com[0])+" "+str(p[1]-com[1])+" "+str(p[2]-com[2])+"\n")
            atoms+=1
        if atoms!=A:
            print(str(atoms)+"/"+str(A)+" atoms created")
        out_file.write("\nBonds\n\n")
        bonds=0
        for x in range(C):
            for y in range(C):
                atoms=x*C*(N+1)+y*(N+1)#C*C*(N+1)
                for n in range(N+1-1):
                    out_file.write(str(bonds+1)+" 1 "+str(atoms+n+1)+" "+str(atoms+n+2)+"\n")
                    bonds+=1
        if bonds!=B:
            print(str(bonds)+" bonds created")

    with open("in.fix_vrevc","w") as out_file:
        out_file.write("# "+str(C*C)+" chains\n")
        out_file.write("# "+str(N)+" bonds per chain\n")
        out_file.write("# fraction of occupied space="+str(bonds*np.pi*rbond*rbond*lbond/(Lx*Ly*Lz))+"\n")
        out_file.write("units lj\n")
        out_file.write("atom_style molecular\n")
        out_file.write("boundary pp pp pp\n")
        out_file.write("newton on on\n")
        out_file.write("\natom_modify sort 1000 1.51\n")
        out_file.write("comm_modify mode single cutoff 5.5 vel yes\n")
        out_file.write("\nread_data data.fix_vrevc\n")
        out_file.write("\nbond_style fene\n")
        out_file.write("bond_coeff * 30.0 1.5 1.0 1.0\n")
        out_file.write("special_bonds fene\n")
        out_file.write("\nneighbor 0.1 bin\n")
        out_file.write("neigh_modify every 1 delay 0 check no cluster yes once no\n")
        out_file.write("\npair_style hybrid/overlay vrevc "+str(vcut)+" 1.3 10000 gauss/cut 0.01\n")
        out_file.write("pair_coeff 1 1 gauss/cut 0.0 1.0 0.01\n")
        out_file.write("pair_coeff 1 1 vrevc "+str(rbond)+"\n")
        out_file.write("\nfix 1 all vrevc wsor 1.3 nsor 10000 rbond "+str(rbond)+" bcut "+str(vcut)+" psor 0.00001 vtol 0.001 exit 0.5 debug 0\n")
        out_file.write("\nvariable sigma equal 1.0\n")
        out_file.write("variable mass equal 1.0\n")
        out_file.write("variable rbond equal "+str(rbond)+"\n")
        out_file.write("variable kBT equal 1.0\n")
        # out_file.write("variable tau equal ${sigma}*sqrt(${mass}/${kBT})\n")
        out_file.write("variable tau equal ${rbond}*sqrt(${mass}/${kBT})\n")
        out_file.write("variable dt0 equal 0.0001*${tau}\n")
        out_file.write("variable dt1 equal 0.01*${tau}\n")
        out_file.write("variable T equal 100\n")
        out_file.write("\nthermo 1000\n")
        out_file.write("thermo_style custom step temp pe ke press atoms lx ly lz\n")
        out_file.write("\nvelocity all create 1.0 1 mom yes rot yes dist gaussian\n")
        out_file.write("velocity all scale 1.0\n")
        out_file.write("\nfix 2 all nve\n")
        out_file.write("\nvariable i loop ${T}\n")
        out_file.write("label loop\n")
        out_file.write("variable dt equal (${i}/${T})*${dt1}+((${T}-${i})/${T})*${dt0}\n")
        out_file.write("timestep ${dt}\n")
        out_file.write("fix 3 all langevin 1.0 1.0 ${tau} "+str(seed)+" gjf yes\n")
        out_file.write("run 200\n")
        out_file.write("unfix 3\n")
        out_file.write("# reset_timestep 0\n")
        out_file.write("next i\n")
        out_file.write("jump SELF loop\n")
        out_file.write("\ntimestep ${dt1}\n")
        out_file.write("fix 3 all langevin 1.0 1.0 ${tau} "+str(seed)+" gjf yes\n")
        out_file.write("restart 100000 mid-run-*.restart\n")
        out_file.write("run 1000000\n")
        out_file.write("write_restart end-run.restart\n")
        out_file.write("unfix 1\n")
        out_file.write("unfix 2\n")
        out_file.write("unfix 3\n")

if __name__=="__main__":
    main(sys.argv[1:])
