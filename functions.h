/*!
  \file functions.h distributed under MIT licence.
  \brief Header file for functions used in the calculation of minimal distance between segment [P0,P0+L0*T0] and segment [P1,P1+L1*T1].
  It also contains based-velocity functions to resolve the excluded volume constraints.
  \author Pascal Carrivain
  \date 03 december 2019
*/
#ifndef UTILITAIRES2_H
#define UTILITAIRES2_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <assert.h>
#include <vector>
#include <cfloat>

/*!
  \struct vec3
  Definition of a vector struct x,y,z.
 */
struct vec3{
  double x;/*!< x coordinate */
  double y;/*!< y coordinate */
  double z;/*!< z coordinate */
  vec3(){/*! default constructor, x=0, y=0 and z=0*/
    x=0.;
    y=0.;
    z=0.;
  };
  vec3(double x1,double y1,double z1){/*! constructor, x=x1, y=y1 and z=z1*/
    x=x1;
    y=y1;
    z=z1;
  }
};

/*! return the dot-product of u with v.
 */
double dot_product(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return the cross-product of u with itself.
 */
double dot_product2(vec3 u/**< [in] struct vec3. */);

/*! return the cross-product of u with v.
 */
vec3 cross_product(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return a*u where a is a scalar.
 */
vec3 mult3(double a/**< [in] double. */,vec3 u/**< [in] struct vec3. */);

/*! return u+v where u and v are vec3.
 */
vec3 add3(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return u-v where u and v are vec3.
 */
vec3 sub3(vec3 u/**< [in] struct vec3. */,vec3 v/**< [in] struct vec3. */);

/*! return the norm of a vec3 u.
 */
double norm3(vec3 u/**< [in] struct vec3. */);

/*! return a vec3 u normalized to 1.
 */
vec3 normalize3(vec3 u/**< [in] struct vec3. */);

/*! @brief This function returns the two points (each on one segment) such that the distance is minimal.
  This function compares the result to an "exact enumeration" too (if test_result is true).
  The number of points (along each segment) is given by the integer variable enumeration.
  The precision of the "enumeration" increases with the integer variable enumeration.
  The function uses OpenMP "section" functionality to handle the different cases.
  @param P1 start of the first bond
  @param B1 end of the first bond
  @param P2 start of the second bond
  @param B2 end of the second bond
  @param point1 closest point 1 (passed by reference)
  @param point2 closest point 2 (passed by reference)
  @param a1 distance between point1 and P1 (passed by reference)
  @param a2 distance between point2 and P2 (passed by reference)
  @param test_result does the function test the result with an "exact enumeration" ?
  @param enumeration number of points to consider in the "exact enumeration"
*/
void minDist2segments_KKT(vec3 P0,vec3 B0,vec3 P1,vec3 B1,vec3 &point1,vec3 &point2,double &a1,double &a2,bool test_result,int enumeration);

/*! @brief This function calculate the FENE forces between two points.
  The result is assigned to the passed by reference variable forces.
  E(r)=-0.5*K*R0*R0*log(1-(r*r/(R0*R0)))*step(0.99*R0-r)+4*epsilon*((sigma/r)^12-(sigma/r)^6+0.25)*step(cut_fene-r)
  F(r)=K*r/(1-r*r/(R0*R0))+24*sigma^6*epsilon*(r^6-2*sigma^6)/r^13
  @param B number of beads
  @param sigma size of the bonds
  @param epsilon wca strength
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param f0 force applied to each bead
*/
void fene(int B,double sigma,double epsilon,double L,vec3 **b0,const int *b1,vec3 **f0);

/*! @brief This function calculate the harmonic forces between two points. The result is assigned to the passed by reference variable forces.
  @param B number of beads
  @param sigma size of the bonds
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param f0 force applied to each bead
 */
void harmonic(int B,double sigma,double L,vec3 **b0,const int *b1,vec3 **f0);

/*! @brief This function resets all the forces.
  @param B number of beads
  @param f0 forces applied to each bead
 */
void reset_forces(int B,vec3 **f0);

/*! @brief This function steps the system to the next conformation (Verlet integration) and increase the size of the bond sigmatmp -> sigma.
  @param iterations number of iterations between two change of bond size
  @param B number of bonds
  @param dt time-step (dimensionless)
  @param mass mass of each bead
  @param sigmatmp length of each bond
  @param radiustmp radius of each bond
  @param sigma length of each bond
  @param radius radius of each bond
  @param L size of the box
  @param b0 start of the bonds
  @param b1 end of the bonds
  @param v0 velocity of each bead
  @param f0 forces applied to the start of each bond
 */
void initial_conformation(int iterations,int B,double dt,double mass,double sigmatmp,double radiustmp,double sigma,double radius,double L,vec3 **b0,const int *b1,vec3 **v0,vec3 **f0);

/*! @brief This function steps the system to the next conformation (Verlet integration).
  @param iterations number of iterations
  @param B number of bonds
  @param dt time-step (dimensionless)
  @param mass mass of each bead
  @param sigma length of each bond
  @param radius radius of each bond
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param v0 velocity of each bead
  @param f0 forces applied to the start of each bond (passed by reference)
  @param potential name of the potential used for the bonds ("harmonic" or "fene")
  @param name name of the file where to save the statistics about overlap(s)
 */
void system_step(int iterations,int B,double dt,double mass,double sigma,double radius,double L,vec3 **b0,const int *b1,vec3 **v0,vec3 **f0,char *potential,char *name);

/*! @brief return the kinetic energy of the system.
  @param B number of beads
  @param mass mass of the beads
  @param v0 velocity of each bead
  @param rescale rescale the velocities, false or true
*/
double kinetic_energy(int B,double mass,vec3 **v0,bool rescale);

/*! @brief COM motion (linear and angular) to (0,0,0).
  @param B number of beads
  @param b0 position of each bead
  @param v0 velocity of each bead
*/
void com_motion_to_0(int B,vec3 **b0,vec3 **v0);

/*! @brief return the distance (between point u and point v) under periodic box conditions.
  @param u first vector
  @param v second vector
  @param L size of the box
*/
double distance_with_PBC(vec3 u,vec3 v,double L);

/*! @brief return the position wrapped in to the box.
  @param p position
  @param L size of the box
*/
vec3 to_the_box(vec3 p,double L);

/*! @brief this function returns projected bonds onto the simulation box.
  @param B number of beads
  @param b0 position of each bead
  @param b1 connectivity
  @param L size of the box
*/
void bonds_to_the_box(int B,vec3 **b0,const int *b1,double L);

/*! @brief this function stores the number of overlaps in the variable n_overlaps. If X<0, the number of overlaps between [0,B[ and [0,B[.
  @param B number of bonds
  @param X overlap(s) between [0,B[ and X.
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param n_overlaps number of overlaps (passed by reference)
  @param mean_overlap overlap average in percent (passed by reference)
  @param max_overlap overlap max in percent (passed by reference)
*/
void number_of_overlaps(int B,int X,double radius,double L,vec3 *b0,const int *b1,int &n_overlaps,double &mean_overlap,double &max_overlap);

/*! @brief get the possible overlap(s).
  @param B number of bonds
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_possible_overlaps number of possible overlap(s)
*/
void get_the_possible_overlaps(int B,double radius,double L,vec3 *b0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int &n_possible_overlaps);

/*! @brief get the overlap(s).
  @param B number of bonds
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_possible_overlaps number of possible overlap(s)
  @param n_overlaps number of overlap(s)
  @param mean_overlap overlap average in pourcent
  @param max_overlap overlap max in pourcent
*/
void get_the_overlaps(int B,double radius,double L,vec3 *b0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_possible_overlaps,int &n_overlaps,double &mean_overlap,double &max_overlap);

/*! @brief resolves the excluded volume constraints with velocity-based method.
  @param B number of bonds
  @param v0 velocity of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_overlaps number of overlap(s)
  @param test does the function test for relative velocity along the normal to the contact ?
*/
void velocities_resolve_EVC(int B,vec3 **v0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_overlaps,bool test);

/*! @brief resolves the excluded volume constraints with velocity-based method.
  @param B number of bonds
  @param rdt reduced time-step dt/2*mass
  @param v0 velocity of each bead
  @param b1 connectivity
  @param f0 forces applied to the end of the start of the bonds (passed by reference)
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_overlaps number of overlaps
  @param test does the function test for relative velocity along the normal to the contact ?
*/
void fvelocities_resolve_EVC(int B,double rdt,vec3 **v0,const int *b1,vec3 **f0,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_overlaps,bool test);

/*! @brief return 1 if a crossing happened during the last time-step, 0 otherwize.
  @param n_overlaps_previous number of overlap(s) from the previous step
  @param o1 index of the bond 1 (previous step)
  @param o2 index of the bond 2 (previous step)
  @param o12 minimal distance vector (previous step)
  @param n_overlaps_current number of overlap(s) from the current step
  @param O1 index of the bond 1 (current step)
  @param O2 index of the bond 2 (current step)
  @param O12 minimal distance vector (current step)
*/
int number_of_crossings(int n_overlaps_previous,int **o1,int **o2,vec3 **o12,int n_overlaps_current,int **O1,int **O2,vec3 **O12);

/*! @brief return the square root of the quadratic average bond size.
  @param B number of beads
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
*/
double average_bond_size(int B,double L,vec3 **b0,const int *b1);

#endif
