/*!
  \file velocities_resolve_EVC.cpp is distributed under MIT licence.
  \brief Main code used as an example on how to resolve excluded volume constraints with a based-velocity method.
  It is B bonds inside a box with periodic bounday conditions. The bonds sizes are controlled by a FENE or
  harmonic potential. It uses Velocity-Verlet integrator and rescale kinetic energy every 20 time-steps while
  the center-of-mass motion is reseted every 10 time-steps.
  \author Pascal Carrivain
  \date 03 December 2019
*/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <random>
#include <vector>
#include <getopt.h>
#include "functions.h"
#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;

double unif1,unif2;/*!< uniform random variables [0,1] */
double sigma=1.0;/*!< size of the bonds */
double radius=0.25*sigma;/*!< radius of the bonds */
double phi=0.7;/*!< fraction of the space occupied by the bonds */
double mass=1.0;/*!< mass of the beads */
double kBT=1.0;/*!< Boltzmann constant times the temperature */
const double Pi=acos(-1.);/*!< Pi */
int arg_i;/*!< variable for the getopt switch */
int seed=1;/*!< seed for the pRNGs */
int enumeration=10000;/*!< the enumeration length */
int B=200;/*!< number of bonds */
int iterations=1000000;/*!< number of iterations */
int n_overlaps;/*!< number of overlaps */
int retour;
double mean_overlap;/*!< average of overlaps */
double max_overlap;/*!< max(overlaps) */
char potential[100],name[100];
bool connectivity=false;/*!< polymer ? */

int main(int argc,char **argv){
#ifdef _OPENMP
  // omp_set_num_threads(min(8,(int)(std::thread::hardware_concurrency())));
  // printf("%i\n",omp_get_max_threads());
  omp_set_num_threads(omp_get_max_threads());
#endif
  /*! harmonic is the default bond potential
   */
  retour=sprintf(potential,"%s","harmonic");
  /*! getopt definition to get the arguments through the command line
   */
  const option long_opts[]={
    {"phi",required_argument,nullptr,'r'},
    {"bonds",required_argument,nullptr,'B'},
    {"seed",required_argument,nullptr,'s'},
    {"enumeration",required_argument,nullptr,'p'},
    {"iterations",required_argument,nullptr,'i'},
    {"potential",required_argument,nullptr,'P'},
    {"connectivity",required_argument,nullptr,'c'},
    {"help",optional_argument,nullptr,'h'}
  };
  while((arg_i=getopt_long(argc,argv,"l:d:r:B:s:p:i:P:c:h",long_opts,nullptr))!=-1){
    switch(arg_i){
    case 'l':
      sigma=atof(optarg);
      break;
    case 'd':
      radius=.5*atof(optarg);
      break;
    case 'r':
      phi=atof(optarg);
      break;
    case 'B':
      B=(int)atof(optarg);
      break;
    case 's':
      seed=(int)atof(optarg);
      break;
    case 'p':
      enumeration=(int)atof(optarg);
      break;
    case 'i':
      iterations=(int)atof(optarg);
      break;
    case 'P':
      retour=sprintf(potential,"%s",optarg);
      break;
    case 'c':
      connectivity=(bool)(strcmp(optarg,"yes")==0);
      break;
    case 'h':
      printf("velocities resolve excluded volume constraints usage :\n");
      printf("      This example generates 'B' bonds (capped cylinders with mass=1) inside a periodic box starting from small bond size to slowly increase to the size we asked for.");
      printf("      The length of the cylinder is given by '-l <size of the bonds>' while the diameter of the cap is given by '-d <diameter of the bonds>'.\n");
      printf("      If connectivity is 'no' the example runs a gas of 'B' bonds.\n");
      printf("      If connectivity is 'yes' the example runs a polymer system of 'B' bonds.\n");
      printf("      After this initial conformation step, we run a dynamic with a velocity Verlet integrator and kBT=1.");
      printf("      ./velocities_resolve_EVC -l <size of the bonds> -d <diameter of the bonds>\n");
      printf("                               -r <phi> -B <number of bonds> -s <seed> -i <iterations>\n");
      printf("                               -p <enumeration> -P <potential> -c <connectivity>\n");
      printf("      Example 1:\n");
      printf("      ./velocities_resolve_EVC -l 1.0 -d 0.5 -r 0.7 -B 200 -s 1 -i 1000000 -p 10000 -P fene -c yes\n");
      printf("      Example 2:\n");
      printf("      ./velocities_resolve_EVC -l 1.0 -d 0.5 -r 0.7 -B 200 -s 1 -i 1000000 -p 10000 -P harmonic -c no\n");
      printf("      Every step we check if two bonds cross themselves.\n");
      printf("      Every 1000 steps, we print the average overlap, the maximum overlap as well as the number of overlaps.\n");
      return 0;
      break;
    default:
      break;
    }
  }

  /*! temporary size of the bonds
   */
  double sigmatmp=.1*sigma;
  /*! temporary radius of the bonds
   */
  double radiustmp=sigmatmp*(radius/sigma);
  /*! size of the box from phi
   */
  double L=cbrt((B*(Pi*radius*radius*sigma+4.*Pi*radius*radius*radius/3.))/phi);
  /*! time step
   */
  double dt=.001*sigmatmp*sqrt(mass/kBT);
  /*! positions, velocities and forces
   */
  int beads=(B+1)*((int)connectivity)+(2*B)*(1-(int)connectivity);
  vec3 *b0=new vec3[beads]();/*!< position of each bead */
  int *b1=new int[beads]();/*!< connectivity */
  vec3 *v0=new vec3[beads]();/*!< velocity of each bead */
  vec3 *f0=new vec3[beads]();/*!< force applied to each bead */
  /*! Initialisation of the random generator
   */
  // std::mt19937_64 e1(seed);
  std::random_device e1;
  std::uniform_real_distribution<double> uniform_real_dist{0.0,1.0};
  std::normal_distribution<double> Nd{0.0,1.0};
  /*! Random generation of the bonds
   */
  if(connectivity){
    // polymer
    for(int b=0;b<B;b++){
      /*! Start of the segment "b", uniformly distributed inside a box of edge L.
       */
      b0[b]=mult3(.5*L-1.01*(sigmatmp+radiustmp),vec3(1.-2.*uniform_real_dist(e1),1.-2.*uniform_real_dist(e1),1.-2.*uniform_real_dist(e1)));
      /*! End of the segment "b", uniformly distributed inside a sphere of radius sigma.
       */
      unif1=2.*Pi*uniform_real_dist(e1);
      unif2=acos(min(1.,max(-1.,2.*uniform_real_dist(e1)-1.)));
      b0[b+1]=add3(b0[b],mult3(sigmatmp,vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2))));
      /*! connectivity
       */
      b1[b]=b+1;
      number_of_overlaps(b,b,radiustmp,L,b0,b1,n_overlaps,mean_overlap,max_overlap);
      if(n_overlaps>0) b--;
    }
    b1[B-1]=-1;
  }else{
    // gas
    for(int b=0;b<B;b++){
      /*! Start of the segment "b", uniformly distributed inside a box of edge L.
       */
      b0[2*b]=mult3(.5*L-1.01*(sigmatmp+radiustmp),vec3(1.-2.*uniform_real_dist(e1),1.-2.*uniform_real_dist(e1),1.-2.*uniform_real_dist(e1)));
      /*! End of the segment "b", uniformly distributed inside a sphere of radius sigma.
       */
      unif1=2.*Pi*uniform_real_dist(e1);
      unif2=acos(min(1.,max(-1.,2.*uniform_real_dist(e1)-1.)));
      b0[2*b+1]=add3(b0[2*b],mult3(sigmatmp,vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2))));
      /*! connectivity
       */
      b1[2*b]=2*b+1;
      b1[2*b+1]=2*b;
      number_of_overlaps(b,b,radiustmp,L,b0,b1,n_overlaps,mean_overlap,max_overlap);
      if(n_overlaps>0) b--;
    }
  }
  /*! Boltzmann distribution for the velocities
   */
  for(int b=0;b<beads;b++) v0[b]=vec3(sqrt(kBT/mass)*Nd(e1),sqrt(kBT/mass)*Nd(e1),sqrt(kBT/mass)*Nd(e1));
  retour=sprintf(name,"stats_overlaps_B%i_l%.2f_r%.2f_phi%.2f.in",B,sigma,radius,phi);
  FILE *fStats=fopen(name,"w");
  retour=fprintf(fStats,"iteration n_overlaps_per_bond mean_overlap max_overlap\n");
  fclose(fStats);
  /*! change the size of the bond
   */
  printf("change the bond size ...\n");
  initial_conformation(10,beads,dt,mass,sigmatmp,radiustmp,sigma,radius,L,&b0,b1,&v0,&f0);
  /*! loop over the total number of iterations
   */
  printf("run the system for %i iterations ...\n",10000);
  system_step(10000,beads,.001*sigma*sqrt(mass/kBT),mass,sigma,radius,L,&b0,b1,&v0,&f0,potential,name);
  printf("run the system for %i iterations ...\n",iterations);
  system_step(iterations,beads,.001*sigma*sqrt(mass/kBT),mass,sigma,radius,L,&b0,b1,&v0,&f0,potential,name);
  delete[] b0;
  delete[] b1;
  delete[] v0;
  delete[] f0;
  return 0;
}
