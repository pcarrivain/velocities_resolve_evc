# velocities_resolve_EVC

I would like to introduce a velocity-based method to resolve the excluded volume
constraint for linear or circular polymer made of consecutive beads linked with
elastic bond. Here, I consider that two consecutive beads form a capped-cylinder.
The capped-cylinder is used for the excluded volume.
Indeed, the geometry simplify the computation of the
[minimal distance](https://gitlab.com/pcarrivain/mindist2segments_kkt) between
two bonds. This velocity-based algorithm comes from the **physics engines**.
The **physics engines** use rigid body that are described by a position and a
frame while the polymer physics field uses beads linked by elastic bond.
Here, I propose to modify the method to work with beads.

The GitLab repository is very simple: **functions.h** is header file for the
**functions.cpp** that is the source file where you can find the code for the
**velocities_resolve_EVC** functionality. The file **velocities_resolve_EVC.cpp**
is an example on how to use it. The file **Makefile** is the one I used to
compile the code.
I use *-fopenmp* and *-pg* for profiling with
[gprof](http://sourceware.org/binutils/docs-2.18/gprof/index.html).
A detailed description of the mathematics behing **velocities_resolve_EVC** can
be found in
[here](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/velocities_resolve_EVC.pdf).
The
[maxima file](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/collision.wxmx)
is the simple case of a collision response between two bonds.

# Implementation for **LAMMPS**

I write a new
[fix named fix_vrevc](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc)
for LAMMPS to resolve excluded volume constraints.
I used the **lammps-7Aug19** version with the following command lines.
```bash
cp -r lammps-7Aug19 lammps-7Aug19_serial;
cd lammps-7Aug19_serial/src;
make clean-all;
make yes-molecule;
make yes-opt;
make yes-user-misc;
make serial;
lmp_serial -in in.fix_vrevc;
```
or:
```bash
cp -r lammps-7Aug19 lammps-7Aug19_omp;
cd lammps-7Aug19_omp/src;
make clean-all;
make yes-molecule;
make yes-opt;
make yes-user-omp;
make yes-user-misc;
make omp;
env OMP_NUM_THREADS=8 lmp_omp -sf omp -in in.fix_vrevc;
```
to use OpenMP implementation.
You need to copy the
[header pair_vrevc.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/pair_vrevc.h),
[source pair_vrevc.cpp](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/pair_vrevc.cpp),
[header fix_vrevc.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/fix_vrevc.h),
[source fix_vrevc.cpp](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/fix_vrevc.cpp)
files to the
**lammps-7Aug19_serial/src/USER-MISC/**
and **lammps-7Aug19_omp/src/USER-MISC/** folders.
You need to copy
[style_fix.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/style_fix.h),
[style_pair.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/style_pair.h),
[modify.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/modify.h),
[packages_fix.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/packages_fix.h)
and
[packages_pair.h](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/packages_pair.h)
to the **lammps-7Aug19_serial/src/**
and **lammps-7Aug19_omp/src/** folders.

I provide
[in file](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/in.fix_vrevc)
and
[data file](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/data.fix_vrevc)
that correspond to a system of 361 chains and 10 bonds per chain.
The bond length $`\sigma`$ is 1 while the bond radius $`r`$ is 0.25.
We use bond potential described by
[Grest and Kremer](journals.aps.org/pra/abstract/10.1103/PhysRevA.33.3628).
The fraction of occupied space is almost 0.5.
The cutoff is half of max bond length plus bond radius (that is roughly 1.0 here).
The reason behind this choice is that a sphere of radius this value
is enough to determine an hit box of half the bond.
All these values can be find in the header of the
[in file](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/in.fix_vrevc)
for LAMMPS.
I use
[make_model_chain.py](https://gitlab.com/pcarrivain/velocities_resolve_evc/-/blob/master/fix_vrevc/make_model_chain.py)
to create both in file and data file.
```python
python3.7 make_model_chain.py -C 19 -N 10 --lbond=1.0 --rbond=0.25 --seed=5
```
In the example, the time-step $`\Delta t`$ is slowly increase until it reaches:
```math
\Delta t=0.01\sigma\sqrt{\frac{m}{k_BT}}
```
We run Langevin dynamics with
[GJF method](https://lammps.sandia.gov/doc/fix_langevin.html)
with a damping parameter one hundred times the time-step.
In order to use the present fix please consider the following lines:
```bash
pair_style hybrid/overlay vrevc 1.0 1.3 10000 gauss/cut 0.01
pair_coeff 1 1 gauss/cut 0.0 1.0 0.01
pair_coeff 1 1 vrevc 0.25
fix 1 all vrevc wsor 1.3 nsor 10000 psor 0.000001 vtol 0.001 rbond 0.25 bcut 1.0 exit 0.5
```
1. wsor is Successive-Over-Relaxation (SOR) parameter
2. nsor is the number of Successive-Over-Relaxation iterations
3. psor is the tolerance for the Successive-Over-Relaxation algorithm
4. vtol is the tolerance for the relative velocity check
5. rbond is radius of the bonds
6. bcut is the cutoff
7. exit (percentage of bond radius) is a parameter to decide if overlap is too big

Implementation of different bond radius as-well-as different
bond mass is *Work-In-Progress*.

# Simple case: two bonds overlap

We consider we have two bonds $`A_1B_1`$ (length $`l_1`$)
and $`A_2B_2`$ (length $`l_2`$).
The two points associated with the
[minimal distance](https://gitlab.com/pcarrivain/mindist2segments_kkt)
are $`M_1`$ and $`M_2`$.
We define $`d_1=\frac{\|M_1-A_1\|}{l_1}`$ and $`d_2=\frac{\|M_2-A_2\|}{l_2}`$
that are the positions along the bonds such that the distance is minimal.
The velocity of point $`M_i`$ is $`(1-d_i)v_{Ai}+d_iv_{Bi}`$.
Therefore, the relative velocity along the normal
to the contact $`\underline{n}_{12}`$ is:
```math
\left((1-d_1)v_{A1}+d_1v_{B1}-(1-d_2)v_{A2}-d_2V_{B2}\right)^T\underline{n}_{12}
```
If the previous dot product is negative the two bonds will continue to
overlap during the next time-step of molecular dynamics.
However, the sign can be flipped for the two bonds to bounce
on each other surface.
The previous equation can be expressed as matricial product:
```math
\underline{\underline{J}}\underline{V}\ge 0
```
Of note, matrix $`\underline{\underline{J}}`$ is simply:
```math
\begin{pmatrix}
(1-d_1)\underline{n}_{12} & d_1\underline{n}_{12} & -(1-d_2)\underline{n}_{12} & -d_2\underline{n}_{12}
\end{pmatrix}
```
and $`\underline{V}`$ is a vector made of bead velocities.
Of note, rigid body dynamics uses linear velocity $`\underline{v}`$:
```math
\underline{v}=\frac{1}{2}\left(v_A+v_B\right)
```
and angular velocity $`\underline{\omega}`$:
```math
\underline{\omega}\times\underline{AB}=v_A-v_B
```
to solve the problem.

Here, we use the velocity of the four overlapping beads.

# General case

To resolve the excluded volume constraints we need to solve a problem like
$`\underline{\underline{A}}\underline{\lambda}=\underline{b}`$
with conditions on $`\underline{l}\ge 0`$.
The condition means two objects cannot move in the direction along the contact.
Therefore, we need to reverse the relative velocity
$`\Delta\underline{v}^T\underline{n}\rightarrow -\Delta\underline{v}^T\underline{n}`$
during one time-step $`\Delta\rightarrow -\Delta`$.
We apply an impulsion $`\lambda\underline{n}`$ to the first bond
and an impulsion $`-\lambda\underline{n}`$ to the second bond ($`\lambda>0`$).
The normal $`\underline{n}`$ point toward the first bond.
The forces can be written as $`\underline{\underline{J}}\underline{\lambda}`$.

Using semi-implicit Euler integration:
```math
\underline{V}(t+dt)=\underline{V}(t)+\frac{dt}{m}\underline{\underline{J}}\underline{\lambda}
```
we ask to reverse the relative velocity in one time-step $`dt`$ of molecular dynamics.
```math
\underline{\underline{J}}\underline{V}(t+dt)=-\Delta
```

To solve the problem we choose the
[Successive-Over-Relaxation method](https://books.google.fr/books?hl=fr&lr=&id=D5LOBQAAQBAJ&oi=fnd&pg=PP1&dq=Iterative+Solution+of+Large+Linear+Systems&ots=gjtO8jnY_o&sig=jBWlhBEPV6cmK3PCnJ5AL5z7Sn0#v=onepage&q=Iterative%20Solution%20of%20Large%20Linear%20Systems&f=false).
This numerical algorithm developped by
[David M. Young](en.wikipedia.org/wiki/David_M._Young_Jr.) needs two parameters.
The first one *nsor* is the number of iterations.
The second one *wsor* needs to be greater than 0 and lesser than 2
for the algorithm to converge.
[It has been shown](https://www.springer.com/gp/book/9783540663218) by Richard S. Varga that:
```math
\omega_{\text{opt}}=\frac{2}{1+\sqrt{1-\mu^2}}
```
speed-up the convergence of the SOR algorithm.
The variable $`\mu`$ is the spectral radius of the Jacobi iteration matrix:
```math
\underline{\underline{I}}-\underline{\underline{D}}_A\underline{\underline{A}}
```
The fix computes the spectral radius with the help of the [Power Iteration algorithm](https://en.wikipedia.org/wiki/Power_iteration).
If $`\omega_{\text{opt}}`$ exists it overwrites the *wsor* argument.
For every SOR iteration we take the maximum between solution and 0.
The process stops when the relative velocity $`\Delta\underline{v}`$
along the normal to the contact $`\underline{n}`$
is close enough to the reversed value.
The *exit* argument is a way to test the algorithm.
If the maximal overlap value (in percentage) is too big the code exit.