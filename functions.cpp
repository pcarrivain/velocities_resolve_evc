/*!
  \file functions.cpp distributed under MIT licence.
  \brief Source file for functions used in the calculation of minimal distance between segment [P0,P0+L0*T0] and segment [P1,P1+L1*T1].
  It also contains based-velocity functions to resolve the excluded volume constraints.
  \author Pascal Carrivain
  \date 03 december 2019
*/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <assert.h>
#include <vector>
#include <cfloat>
#include "functions.h"

using namespace std;

/*! @brief return the dot-product of u with v.
 */
double dot_product(vec3 u,vec3 v){
  return u.x*v.x+u.y*v.y+u.z*v.z;
}

/*! @brief return the cross-product of u with itself.
 */
double dot_product2(vec3 u){
  return u.x*u.x+u.y*u.y+u.z*u.z;
}

/*! @brief return the cross-product of u with v.
 */
vec3 cross_product(vec3 u,vec3 v){
  double a=u.x,b=u.y;
  u.x=b*v.z-u.z*v.y;
  u.y=u.z*v.x-a*v.z;
  u.z=a*v.y-b*v.x;
  return u;
}

/*! @brief return a*u where a is a scalar.
 */
vec3 mult3(double a,vec3 u){
  u.x*=a;
  u.y*=a;
  u.z*=a;
  return u;
}

/*! @brief return u+v where u and v are vec3.
 */
vec3 add3(vec3 u,vec3 v){
  u.x+=v.x;
  u.y+=v.y;
  u.z+=v.z;
  return u;
}

/*! @brief return u-v where u and v are vec3.
 */
vec3 sub3(vec3 u,vec3 v){
  u.x-=v.x;
  u.y-=v.y;
  u.z-=v.z;
  return u;
}

/*! @brief return the norm of a vec3 u.
 */
double norm3(vec3 u){
  return sqrt(u.x*u.x+u.y*u.y+u.z*u.z);
}

/*! @brief return a vec3 u normalized to 1.
 */
vec3 normalize3(vec3 u){
  double n=sqrt(u.x*u.x+u.y*u.y+u.z*u.z);
  if(n>0.){
    u.x/=n;
    u.y/=n;
    u.z/=n;
  }
  return u;
}

/*! @brief This function returns the two points (each on one segment) such that the distance is minimal.
  This function compares the result to an "exact enumeration" too (if test_result is true).
  The number of points (along each segment) is given by the integer variable enumeration.
  The precision of the "enumeration" increases with the integer variable enumeration.
  The function uses OpenMP "section" functionality to handle the different cases.
  @param P1 start of the first bond
  @param B1 end of the first bond
  @param P2 start of the second bond
  @param B2 end of the second bond
  @param point1 closest point 1 (passed by reference)
  @param point2 closest point 2 (passed by reference)
  @param a1 distance between point1 and P1 (passed by reference)
  @param a2 distance between point2 and P2 (passed by reference)
  @param test_result does the function test the result with an "exact enumeration" ?
  @param enumeration number of points to consider in the "exact enumeration"
 */
void minDist2segments_KKT(vec3 P1,vec3 B1,vec3 P2,vec3 B2,vec3 &point1,vec3 &point2,double &a1,double &a2,bool test_result,int enumeration){
  double i_star=-1.;
  double j_star=-1.;
  double r_star_min=-1.;
  double s_star_min=-1.;
  double ri,si;
  /*! min of d^2 over the nine solutions
  */
  double d2_min_KKT=DBL_MAX,d2;
  /*! Tangent vectors of each of the two segments
   */
  vec3 T1=sub3(B1,P1);
  vec3 T2=sub3(B2,P2);
  /*! Segment length of each of the two segments
   */
  double l1=norm3(T1);
  double l2=norm3(T2);
  /*! Normalized tangent vectors of each of the two segments
   */
  T1=normalize3(T1);
  T2=normalize3(T2);
  /*! Dot product between tangent segments
   */
  double cos01=dot_product(T1,T2);
  /* Distance vector between origin point of the two segments.
   */
  vec3 DELTA=sub3(P1,P2);
  /*! Solutions for r and s inside the feasible region.
   */
  /*! solution 1 is not valid if the two segments are parallel.
   */
  if(cos01>-.99999 && cos01<.99999){
    ri=(cos01*dot_product(T2,DELTA)-dot_product(T1,DELTA))/(1.-cos01*cos01);
    si=(dot_product(T2,DELTA)-cos01*dot_product(T1,DELTA))/(1.-cos01*cos01);
    if(!(ri<0. || ri>l1 || si<0. || si>l2)){
      d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
      if(d2<d2_min_KKT){
	d2_min_KKT=d2;
	r_star_min=ri;
	s_star_min=si;
	a1=r_star_min/l1;
	a2=s_star_min/l2;
      }
    }
  }else{
    ri=-1.;
    si=-1.;
  }
  /*! Solutions for r and s outside the feasible region.
   */
  /*! solution 2
   */
  ri=0.;
  si=dot_product(T2,DELTA);
  if(!(si<0. || si>l2)){
    d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
    if(d2<d2_min_KKT){
      d2_min_KKT=d2;
      r_star_min=ri;
      s_star_min=si;
      a1=0.;
      a2=s_star_min/l2;
    }
  }
  /*! solution 3
   */
  ri=-dot_product(T1,DELTA);
  si=0.;
  if(!(ri<0. || ri>l1)){
    d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
    if(d2<d2_min_KKT){
      d2_min_KKT=d2;
      r_star_min=ri;
      s_star_min=si;
      a1=r_star_min/l1;
      a2=0.;
    }
  }
  /*! solution 4
   */
  ri=0.;
  si=0.;
  d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
  if(d2<d2_min_KKT){
    d2_min_KKT=d2;
    r_star_min=ri;
    s_star_min=si;
    a1=0.;
    a2=0.;
  }
  /*! solution 5
   */
  ri=l1;
  si=l1*cos01+dot_product(T2,DELTA);
  if(!(si<0. || si>l2)){
    d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
    if(d2<d2_min_KKT){
      d2_min_KKT=d2;
      r_star_min=ri;
      s_star_min=si;
      a1=1.;
      a2=s_star_min/l2;
    }
  }
  /*! solution 6
   */
  ri=l1;
  si=0.;
  d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
  if(d2<d2_min_KKT){
    d2_min_KKT=d2;
    r_star_min=ri;
    s_star_min=si;
    a1=1.;
    a2=0.;
  }
  /*! solution 7
   */
  ri=l2*cos01-dot_product(T1,DELTA);
  si=l2;
  if(!(ri<0. || ri>l1)){
    d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
    if(d2<d2_min_KKT){
      d2_min_KKT=d2;
      r_star_min=ri;
      s_star_min=si;
      a1=r_star_min/l1;
      a2=1.;
    }
  }
  /*! solution 8
   */
  ri=0.;
  si=l2;
  d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
  if(d2<d2_min_KKT){
    d2_min_KKT=d2;
    r_star_min=ri;
    s_star_min=si;
    a1=0.;
    a2=1.;
  }
  /*! solution 9
   */
  ri=l1;
  si=l2;
  d2=dot_product2(add3(DELTA,sub3(mult3(ri,T1),mult3(si,T2))));
  if(d2<d2_min_KKT){
    d2_min_KKT=d2;
    r_star_min=ri;
    s_star_min=si;
    a1=1.;
    a2=1.;
  }
  /*! Enumerate all the possible distances (segment 0,segment 1) and keep the star one
   */
  double d2_min_enum=DBL_MAX;
  vec3 P_STAR;
  if(test_result){
    for(int i=0;i<=enumeration;i++){
      P_STAR=add3(P1,mult3(i*l1/enumeration,T1));
      for(int j=0;j<=enumeration;j++){
	d2=dot_product2(sub3(P_STAR,add3(P1,mult3(j*l2/enumeration,T2))));
	if(d2<d2_min_enum){
	  d2_min_enum=d2;
	  i_star=((double)i/(double)enumeration)*l1;
	  j_star=((double)j/(double)enumeration)*l2;
	}
      }
    }
    printf("The minimal distance between segment 1 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n",P1.x,P1.y,P1.z,B1.x,B1.y,B1.z);
    printf("and segment 2 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n",P2.x,P2.y,P2.z,B2.x,B2.y,B2.z);
    printf("is :\n");
    printf("KKT conditions : position along segment 1 %f, position along segment 2 %f, minimal distance %f\n",r_star_min/l1,s_star_min/l2,d2_min_KKT);
    printf("enumeration : position along segment 1 %f, position along segment 2 %f, minimal distance %f\n",i_star/l1,j_star/l2,d2_min_enum);
    /*! Check for differences between KKT conditions and enumeration results.
     */
    if(fabs(i_star-r_star_min)>(l1/enumeration) || fabs(j_star-s_star_min)>(l2/enumeration) || fabs(d2_min_enum-d2_min_KKT)>(.001*d2_min_enum)){
      printf("warning : the difference between KKT and enumeration solutions is greater than the enumeration increment step %f.\n",1./(double)enumeration);
      printf("          it is %e %s of difference between KKT conditions and enumeration method.\n\n",fabs(d2_min_enum-d2_min_KKT)/d2_min_enum,"%");
    }else printf("\n");
  }
  /*! result (passed by reference).
   */
  point1=add3(P1,mult3(r_star_min,T1));
  point2=add3(P2,mult3(s_star_min,T2));
}

/*! @brief This function calculate the FENE forces between two points.
  The result is assigned to the passed by reference variable forces.
  E(r)=-0.5*K*R0*R0*log(1-(r*r/(R0*R0)))*step(0.99*R0-r)+4*epsilon*((sigma/r)^12-(sigma/r)^6+0.25)*step(cut_fene-r)
  F(r)=K*r/(1-r*r/(R0*R0))+24*sigma^6*epsilon*(r^6-2*sigma^6)/r^13
  @param B number of beads
  @param sigma size of the bonds
  @param epsilon wca strength
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param f0 force applied to each bead
 */
void fene(int B,double sigma,double epsilon,double L,vec3 **b0,const int *b1,vec3 **f0){
  double distance;
  double distance2;
  double distance6;
  double sigma2=sigma*sigma;
  double sigma6=sigma2*sigma2*sigma2;
  double K=30.0*epsilon/sigma2;
  double R0=1.5*sigma;
  vec3 force;
  double cut=pow(2.,1./6.)*sigma;
  int b1b;
  // loop over the bonds
#pragma omp parallel for private(distance,distance2,distance6,force,b1b)
  for(int b=0;b<B;b++){
    b1b=b1[b];
    // connectivity ?
    if(b<b1b){
      distance=distance_with_PBC((*b0)[b],(*b0)[b1b],L);
      distance2=distance*distance;
      distance6=distance2*distance2*distance2;
      force=mult3((int)(distance<R0)*K*distance/(1.-distance2/(R0*R0))+(int)(distance<cut)*24.*sigma6*epsilon*(distance6-2.*sigma6)/(distance6*distance6*distance),normalize3(sub3((*b0)[b],(*b0)[b1b])));
      (*f0)[b]=sub3((*f0)[b],force);
      (*f0)[b1b]=add3((*f0)[b1b],force);
    }
  }
}

/*! @brief This function calculate the harmonic forces between two points. The result is assigned to the passed by reference variable forces.
  @param B number of beads
  @param sigma size of the bonds
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param f0 force applied to each bead
 */
void harmonic(int B,double sigma,double L,vec3 **b0,const int *b1,vec3 **f0){
  double kBT=1.;
  double K=3.*kBT/(sigma*sigma);
  vec3 force;
  int b1b;
  // loop over the bonds
#pragma omp parallel for private(force,b1b)
  for(int b=0;b<B;b++){
    b1b=b1[b];
    // connectivity ?
    if(b<b1b){
      force=mult3(K*distance_with_PBC((*b0)[b],(*b0)[b1b],L),normalize3(sub3((*b0)[b],(*b0)[b1b])));
      (*f0)[b]=sub3((*f0)[b],force);
      (*f0)[b1b]=add3((*f0)[b1b],force);
    }
  }
}
  
/*! @brief This function resets all the forces.
  @param B number of beads
  @param f0 forces applied to each bead
 */
void reset_forces(int B,vec3 **f0){
#pragma omp parallel for
  for(int b=0;b<B;b++) (*f0)[b]=vec3(0.,0.,0.);
}

/*! @brief This function steps the system to the next conformation (Verlet integration) and increase the size of the bond sigmatmp -> sigma.
  @param iterations number of iterations between two change of bond size
  @param B number of beads
  @param dt time-step (dimensionless)
  @param mass mass of each bead
  @param sigmatmp length of each bond
  @param radiustmp radius of each bond
  @param sigma length of each bond
  @param radius radius of each bond
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
  @param v0 velocity of each bead
  @param f0 force applied to each bead
 */
void initial_conformation(int iterations,int B,double dt,double mass,double sigmatmp,double radiustmp,double sigma,double radius,double L,vec3 **b0,const int *b1,vec3 **v0,vec3 **f0){
  bool is_openmp=false;
#ifdef _OPENMP
  is_openmp=true;
#endif
  double coeff=.5*dt/mass;
  double kBT=1.;
  // variables for the overlaps
  double mean_overlap,max_overlap;
  int MR=6*B,np_overlaps,np_Overlaps,n_overlaps,n_Overlaps;
  // overlap(s) before the step
  int *overlap_1=new int[MR]();
  int *overlap_2=new int[MR]();
  double *overlap_d1=new double[MR]();
  double *overlap_d2=new double[MR]();
  vec3 *overlap_12=new vec3[MR]();
  // overlap(s) after the step
  int *Overlap_1=new int[MR]();
  int *Overlap_2=new int[MR]();
  double *Overlap_d1=new double[MR]();
  double *Overlap_d2=new double[MR]();
  vec3 *Overlap_12=new vec3[MR]();
  // possible overlap(s)
  int *pOverlap_1=new int[MR]();
  int *pOverlap_2=new int[MR]();
  double *pOverlap_d1=new double[MR]();
  double *pOverlap_d2=new double[MR]();
  vec3 *pOverlap_12=new vec3[MR]();
  int counts=0;
  double dsigma=.00001*(sigma-sigmatmp);
  // change the size of the bonds
  while(sigmatmp!=sigma){
    // get the possible overlap(s) before the step
    get_the_possible_overlaps(B,radiustmp,L,*b0,b1,&pOverlap_1,&pOverlap_2,&pOverlap_d1,&pOverlap_d2,&pOverlap_12,np_overlaps);
    // copy the possible overlap(s)
    if(np_overlaps<32 || !is_openmp){
      for(int i=0;i<np_overlaps;i++){
	overlap_1[i]=pOverlap_1[i];
	overlap_2[i]=pOverlap_2[i];
	overlap_d1[i]=pOverlap_d1[i];
	overlap_d2[i]=pOverlap_d2[i];
	overlap_12[i]=pOverlap_12[i];
      }
    }else{
#pragma omp parallel for
      for(int i=0;i<np_overlaps;i++){
	overlap_1[i]=pOverlap_1[i];
	overlap_2[i]=pOverlap_2[i];
	overlap_d1[i]=pOverlap_d1[i];
	overlap_d2[i]=pOverlap_d2[i];
	overlap_12[i]=pOverlap_12[i];
      }
    }
    // get the overlap(s) before the step
    get_the_overlaps(B,radiustmp,L,*b0,b1,&overlap_1,&overlap_2,&overlap_d1,&overlap_d2,&overlap_12,np_overlaps,n_overlaps,mean_overlap,max_overlap);
    // iterations
    reset_forces(B,f0);
    for(int i=0;i<iterations;i++){
      // forces
      fene(B,sigmatmp,kBT,L,b0,b1,f0);
      // new velocities & positions
#pragma omp parallel for
      for(int b=0;b<B;b++){
	(*v0)[b]=add3((*v0)[b],mult3(coeff,(*f0)[b]));
	(*f0)[b]=vec3(0.,0.,0.);
	(*b0)[b]=add3((*b0)[b],mult3(dt,(*v0)[b]));
      }
      // new forces
      fene(B,sigmatmp,kBT,L,b0,b1,f0);
      // new velocities
#pragma omp parallel for
      for(int b=0;b<B;b++){
	(*v0)[b]=add3((*v0)[b],mult3(coeff,(*f0)[b]));
	(*f0)[b]=vec3(0.,0.,0.);
      }
      // get the possible overlap(s) every 10 steps (including step 0)
      if(i%4==0) get_the_possible_overlaps(B,radiustmp,L,*b0,b1,&pOverlap_1,&pOverlap_2,&pOverlap_d1,&pOverlap_d2,&pOverlap_12,np_Overlaps);
      // copy the possible overlap(s)
      if(np_Overlaps<32 || !is_openmp){
	for(int j=0;j<np_Overlaps;j++){
	  Overlap_1[j]=pOverlap_1[j];
	  Overlap_2[j]=pOverlap_2[j];
	  Overlap_d1[j]=pOverlap_d1[j];
	  Overlap_d2[j]=pOverlap_d2[j];
	  Overlap_12[j]=pOverlap_12[j];
	}
      }else{
#pragma omp parallel for
	for(int j=0;j<np_Overlaps;j++){
	  Overlap_1[j]=pOverlap_1[j];
	  Overlap_2[j]=pOverlap_2[j];
	  Overlap_d1[j]=pOverlap_d1[j];
	  Overlap_d2[j]=pOverlap_d2[j];
	  Overlap_12[j]=pOverlap_12[j];
	}
      }
      // get the overlap(s)
      get_the_overlaps(B,radiustmp,L,*b0,b1,&Overlap_1,&Overlap_2,&Overlap_d1,&Overlap_d2,&Overlap_12,np_Overlaps,n_Overlaps,mean_overlap,max_overlap);
      // number of crossing(s), exit if it is > 0
      if(number_of_crossings(n_overlaps,&overlap_1,&overlap_2,&overlap_12,n_Overlaps,&Overlap_1,&Overlap_2,&Overlap_12)>0){
      	printf("crossing detected, exit.\n");
      	exit(-1);
      }
      // copy the overlap(s)
      if(n_Overlaps<32 || !is_openmp){
	for(int j=0;j<n_Overlaps;j++){
	  overlap_1[j]=Overlap_1[j];
	  overlap_2[j]=Overlap_2[j];
	  overlap_12[j]=Overlap_12[j];
	}
      }else{
#pragma omp parallel for
	for(int j=0;j<n_Overlaps;j++){
	  overlap_1[j]=Overlap_1[j];
	  overlap_2[j]=Overlap_2[j];
	  overlap_12[j]=Overlap_12[j];
	}
      }
      n_overlaps=n_Overlaps;
      // resolve the excluded volume constraints
      velocities_resolve_EVC(B,v0,b1,&Overlap_1,&Overlap_2,&Overlap_d1,&Overlap_d2,&Overlap_12,n_Overlaps,false);
      // COM motion to 0,0,0
      if(i%10==0) com_motion_to_0(B,b0,v0);
      // Rescale the kinetic energy
      if(i%10==0) kinetic_energy(B,mass,v0,true);
      // Print some informations
      if(counts%10000==0){
	printf("iteration(initial phase) %i : sqrt(<l^2>)/l=%f K/<K>=%f\n",counts,average_bond_size(B,L,b0,b1)/sigma,kinetic_energy(B,mass,v0,false)/(3.*B*kBT));
	printf("%f %f\n",radiustmp/radius,sigmatmp/sigma);
	if(n_overlaps>0){
	  printf("%i overlap(s) mean=%f max=%f\n",n_overlaps,mean_overlap,max_overlap);
	  number_of_overlaps(B,-1,radiustmp,L,*b0,b1,n_overlaps,mean_overlap,max_overlap);
	  printf("%i overlap(s) mean=%f max=%f\n\n",n_overlaps,mean_overlap,max_overlap);
	}
      }
      counts++;
    }
    // change the size
    sigmatmp=fmin(sigma,sigmatmp+dsigma);
    radiustmp=sigmatmp*(radius/sigma);
    dt=.001*sigmatmp*sqrt(mass/kBT);
  }
  // cleanup
  delete[] overlap_1;
  delete[] overlap_2;
  delete[] overlap_d1;
  delete[] overlap_d2;
  delete[] overlap_12;
  delete[] Overlap_1;
  delete[] Overlap_2;
  delete[] Overlap_d1;
  delete[] Overlap_d2;
  delete[] Overlap_12;
  delete[] pOverlap_1;
  delete[] pOverlap_2;
  delete[] pOverlap_d1;
  delete[] pOverlap_d2;
  delete[] pOverlap_12;
}

/*! This function steps the system to the next conformation (Verlet integration).
  @param iterations number of iterations
  @param B number of bonds
  @param dt time-step (dimensionless)
  @param mass mass of each bead
  @param sigma length of each bond
  @param radius radius of each bond
  @param L size of the box
  @param b0 start of the bonds (passed by reference)
  @param b1 end of the bonds (passed by reference)
  @param v0 velocity of each bead
  @param f0 force applied to each bead
  @param potential name of the potential used for the bonds ("harmonic" or "fene")
  @param name name of the file where to save the statistics about overlap(s)
 */
void system_step(int iterations,int B,double dt,double mass,double sigma,double radius,double L,vec3 **b0,const int *b1,vec3 **v0,vec3 **f0,char *potential,char *name){
  bool is_openmp=false;
#ifdef _OPENMP
  is_openmp=true;
#endif
  double coeff=.5*dt/mass;
  double kBT=1.;
  FILE *fStats;
  // variables for the overlaps
  double mean_overlap,max_overlap;
  int MR=6*B,np_overlaps,np_Overlaps,n_overlaps,n_Overlaps;
  // overlap(s) before the step
  int *overlap_1=new int[MR]();
  int *overlap_2=new int[MR]();
  double *overlap_d1=new double[MR]();
  double *overlap_d2=new double[MR]();
  vec3 *overlap_12=new vec3[MR]();
  // overlap(s) after the step
  int *Overlap_1=new int[MR]();
  int *Overlap_2=new int[MR]();
  double *Overlap_d1=new double[MR]();
  double *Overlap_d2=new double[MR]();
  vec3 *Overlap_12=new vec3[MR]();
  // possible overlap(s) after the step
  int *pOverlap_1=new int[MR]();
  int *pOverlap_2=new int[MR]();
  double *pOverlap_d1=new double[MR]();
  double *pOverlap_d2=new double[MR]();
  vec3 *pOverlap_12=new vec3[MR]();
  // get the possible overlap(s) before the step
  get_the_possible_overlaps(B,radius,L,*b0,b1,&pOverlap_1,&pOverlap_2,&pOverlap_d1,&pOverlap_d2,&pOverlap_12,np_overlaps);
  // copy the possible overlap(s)
  if(np_overlaps<32 || !is_openmp){
    for(int i=0;i<np_overlaps;i++){
      overlap_1[i]=pOverlap_1[i];
      overlap_2[i]=pOverlap_2[i];
      overlap_d1[i]=pOverlap_d1[i];
      overlap_d2[i]=pOverlap_d2[i];
      overlap_12[i]=pOverlap_12[i];
    }
  }else{
#pragma omp parallel for
    for(int i=0;i<np_overlaps;i++){
      overlap_1[i]=pOverlap_1[i];
      overlap_2[i]=pOverlap_2[i];
      overlap_d1[i]=pOverlap_d1[i];
      overlap_d2[i]=pOverlap_d2[i];
      overlap_12[i]=pOverlap_12[i];
    }
  }
  // get the overlap(s) before the step
  get_the_overlaps(B,radius,L,*b0,b1,&overlap_1,&overlap_2,&overlap_d1,&overlap_d2,&overlap_12,np_overlaps,n_overlaps,mean_overlap,max_overlap);
  // iterations
  reset_forces(B,f0);
  for(int i=0;i<iterations;i++){
    // forces
    if(strcmp(potential,"fene")==0) fene(B,sigma,kBT,L,b0,b1,f0);
    if(strcmp(potential,"harmonic")==0) harmonic(B,sigma,L,b0,b1,f0);
    // new velocities & positions
#pragma omp parallel for
    for(int b=0;b<B;b++){
      (*v0)[b]=add3((*v0)[b],mult3(coeff,(*f0)[b]));
      (*f0)[b]=vec3(0.,0.,0.);
      (*b0)[b]=add3((*b0)[b],mult3(dt,(*v0)[b]));
    }
    // new forces
    if(strcmp(potential,"fene")==0) fene(B,sigma,kBT,L,b0,b1,f0);
    if(strcmp(potential,"harmonic")==0) harmonic(B,sigma,L,b0,b1,f0);
    // new velocities
#pragma omp parallel for
    for(int b=0;b<B;b++){
      (*v0)[b]=add3((*v0)[b],mult3(coeff,(*f0)[b]));
      (*f0)[b]=vec3(0.,0.,0.);
    }
    // get the possible overlap(s) every 10 steps (including step 0)
    if(i%10==0) get_the_possible_overlaps(B,radius,L,*b0,b1,&pOverlap_1,&pOverlap_2,&pOverlap_d1,&pOverlap_d2,&pOverlap_12,np_Overlaps);
    // copy the possible overlap(s)
    if(np_Overlaps<32 || !is_openmp){
      for(int j=0;j<np_Overlaps;j++){
	Overlap_1[j]=pOverlap_1[j];
	Overlap_2[j]=pOverlap_2[j];
	Overlap_d1[j]=pOverlap_d1[j];
	Overlap_d2[j]=pOverlap_d2[j];
	Overlap_12[j]=pOverlap_12[j];
      }
    }else{
#pragma omp parallel for
      for(int j=0;j<np_Overlaps;j++){
	Overlap_1[j]=pOverlap_1[j];
	Overlap_2[j]=pOverlap_2[j];
	Overlap_d1[j]=pOverlap_d1[j];
	Overlap_d2[j]=pOverlap_d2[j];
	Overlap_12[j]=pOverlap_12[j];
      }
    }
    // get the overlap(s)
    get_the_overlaps(B,radius,L,*b0,b1,&Overlap_1,&Overlap_2,&Overlap_d1,&Overlap_d2,&Overlap_12,np_Overlaps,n_Overlaps,mean_overlap,max_overlap);
    // number of crossing(s), exit if it is > 0
    if(number_of_crossings(n_overlaps,&overlap_1,&overlap_2,&overlap_12,n_Overlaps,&Overlap_1,&Overlap_2,&Overlap_12)>0){
      printf("crossing detected, exit.\n");
      exit(-1);
    }
    // copy the overlap(s)
    if(n_Overlaps<32 || !is_openmp){
      for(int j=0;j<n_Overlaps;j++){
	overlap_1[j]=Overlap_1[j];
	overlap_2[j]=Overlap_2[j];
	overlap_12[j]=Overlap_12[j];
      }
    }else{
#pragma omp parallel for
      for(int j=0;j<n_Overlaps;j++){
	overlap_1[j]=Overlap_1[j];
	overlap_2[j]=Overlap_2[j];
	overlap_12[j]=Overlap_12[j];
      }
    }
    n_overlaps=n_Overlaps;
    // resolve the excluded volume constraints
    if(strcmp(potential,"fene")==0) fene(B,sigma,kBT,L,b0,b1,f0);
    if(strcmp(potential,"harmonic")==0) harmonic(B,sigma,L,b0,b1,f0);
    fvelocities_resolve_EVC(B,coeff,v0,b1,f0,&Overlap_1,&Overlap_2,&Overlap_d1,&Overlap_d2,&Overlap_12,n_Overlaps,false);
    reset_forces(B,f0);
    // velocities_resolve_EVC(B,v0,b1,&Overlap_1,&Overlap_2,&Overlap_d1,&Overlap_d2,&Overlap_12,n_Overlaps,false);
    // COM motion to 0,0,0
    if(i%10==0) com_motion_to_0(B,b0,v0);
    // Rescale the kinetic energy
    if(i%10==0) kinetic_energy(B,mass,v0,true);
    // Write data about overlaps
    if(i%10000==0 && i!=0){
      printf("iteration %i : sqrt(<l^2>)/l=%f K/<K>=%f\n",i,average_bond_size(B,L,b0,b1)/sigma,kinetic_energy(B,mass,v0,false)/(3.*B*kBT));
      if(n_overlaps>0){
	printf("%i overlap(s) mean=%f max=%f\n",n_overlaps,mean_overlap,max_overlap);
	number_of_overlaps(B,-1,radius,L,*b0,b1,n_overlaps,mean_overlap,max_overlap);
	printf("%i overlap(s) mean=%f max=%f\n\n",n_overlaps,mean_overlap,max_overlap);
	fStats=fopen(name,"a");
	fprintf(fStats,"%i %f %f %f\n",i,n_overlaps/(double)B,mean_overlap,max_overlap);
	fclose(fStats);
      }
    }
  }
  // cleanup
  delete[] overlap_1;
  delete[] overlap_2;
  delete[] overlap_d1;
  delete[] overlap_d2;
  delete[] overlap_12;
  delete[] Overlap_1;
  delete[] Overlap_2;
  delete[] Overlap_d1;
  delete[] Overlap_d2;
  delete[] Overlap_12;
  delete[] pOverlap_1;
  delete[] pOverlap_2;
  delete[] pOverlap_d1;
  delete[] pOverlap_d2;
  delete[] pOverlap_12;
}

/*! @brief return the kinetic energy of the system.
  @param B number of beads
  @param mass mass of the beads
  @param v0 velocity of each bead
  @param rescale rescale the velocities, false or true
*/
double kinetic_energy(int B,double mass,vec3 **v0,bool rescale){
  double K=.0,kBT=1.;
  for(int b=0;b<B;b++) K+=dot_product2((*v0)[b]);
  K*=.5*mass;
  double rK=sqrt((3.*(2*B)*kBT/2.)/K);
  // rescaling ?
  if(rescale){
#pragma omp parallel for
    for(int b=0;b<B;b++) (*v0)[b]=mult3(rK,(*v0)[b]);
  }
  return K;
}

/*! @brief COM motion (linear and angular) to (0,0,0).
  @param B number of beads
  @param b0 position of each beads
  @param v0 velocity of each bead
*/
void com_motion_to_0(int B,vec3 **b0,vec3 **v0){
  vec3 v_com=vec3(0.,0.,0.);
  double I3[9],inv_I3[9];
  double cx=0.,cy=0.,cz=0.,d0;
  double dx,dy,dz;
  double wx,wy,wz;
  // com motion to (0,0,0)
  for(int b=0;b<B;b++){
    cx+=(*b0)[b].x;
    cy+=(*b0)[b].y;
    cz+=(*b0)[b].z;
    v_com.x+=(*v0)[b].x;
    v_com.y+=(*v0)[b].y;
    v_com.z+=(*v0)[b].z;
  }
  cx/=(2*B);
  cy/=(2*B);
  cz/=(2*B);
  v_com.x/=(2*B);
  v_com.y/=(2*B);
  v_com.z/=(2*B);
#pragma omp parallel for
  for(int b=0;b<B;b++) (*v0)[b]=sub3((*v0)[b],v_com);
  // inertia
  for(int i=0;i<9;i++) I3[i]=0.;
  for(int b=0;b<B;b++){
    I3[0]+=pow((*b0)[b].y-cy,2.)+pow((*b0)[b].z-cz,2.);
    I3[1]-=((*b0)[b].y-cy)*((*b0)[b].x-cx);
    I3[2]-=((*b0)[b].z-cz)*((*b0)[b].x-cx);
    I3[4]+=pow((*b0)[b].x-cx,2.)+pow((*b0)[b].z-cz,2.);
    I3[5]-=((*b0)[b].z-cz)*((*b0)[b].y-cy);
    I3[8]+=pow((*b0)[b].x-cx,2.)+pow((*b0)[b].y-cy,2.);
  }
  inv_I3[0]=I3[5]*I3[5]-I3[4]*I3[8];
  inv_I3[1]=I3[1]*I3[8]-I3[2]*I3[5];
  inv_I3[2]=I3[2]*I3[4]-I3[1]*I3[5];
  inv_I3[3]=inv_I3[1];
  inv_I3[4]=I3[2]*I3[2]-I3[0]*I3[8];
  inv_I3[5]=I3[0]*I3[5]-I3[1]*I3[2];
  inv_I3[6]=inv_I3[2];
  inv_I3[7]=inv_I3[5];
  inv_I3[8]=I3[1]*I3[1]-I3[0]*I3[4];
  d0=I3[0]*I3[5]*I3[5]+I3[1]*I3[1]*I3[8]-2.*I3[1]*I3[2]*I3[5]+I3[2]*I3[2]*I3[4]-I3[0]*I3[4]*I3[8];
  // angular motion to (0,0,0)
  if(d0!=0.){
    // angular momentum
    dx=0.;
    dy=0.;
    dz=0.;
    for(int b=0;b<B;b++){
      dx+=((*b0)[b].y-cy)*(*v0)[b].z-((*b0)[b].z-cz)*(*v0)[b].y;
      dy+=((*b0)[b].z-cz)*(*v0)[b].x-((*b0)[b].x-cx)*(*v0)[b].z;
      dz+=((*b0)[b].x-cx)*(*v0)[b].y-((*b0)[b].y-cy)*(*v0)[b].x;
    }
    // w=I^-1l
    wx=(inv_I3[0]*dx+inv_I3[1]*dy+inv_I3[2]*dz)/d0;
    wy=(inv_I3[3]*dx+inv_I3[4]*dy+inv_I3[5]*dz)/d0;
    wz=(inv_I3[6]*dx+inv_I3[7]*dy+inv_I3[8]*dz)/d0;
#pragma omp parallel for
    for(int b=0;b<B;b++){
      (*v0)[b].x-=(wy*((*b0)[b].z-cz)-wz*((*b0)[b].y-cy));
      (*v0)[b].y-=(wz*((*b0)[b].x-cx)-wx*((*b0)[b].z-cz));
      (*v0)[b].z-=(wx*((*b0)[b].y-cy)-wy*((*b0)[b].x-cx));
    }
  }
}

/*! @brief return the distance (between point u and point v) under periodic box conditions.
  @param u first vector
  @param v second vector
  @param L size of the box
*/
double distance_with_PBC(vec3 u,vec3 v,double L){
  double dx=u.x-v.x;
  double dy=u.y-v.y;
  double dz=u.z-v.z;
  dx-=L*nearbyint(dx/L);
  dy-=L*nearbyint(dy/L);
  dz-=L*nearbyint(dz/L);
  return sqrt(dx*dx+dy*dy+dz*dz);
}

/*! @brief return the position wrapped in to the box.
  @param p position
  @param L size of the box
*/
vec3 to_the_box(vec3 p,double L){
  return vec3(p.x-L*nearbyint(p.x/L),p.y-L*nearbyint(p.y/L),p.z-L*nearbyint(p.z/L));
}

/*! @brief this function returns the bonds projected onto the simulation box.
  @param B number of beads
  @param b0 position of each bead
  @param b1 connectivity
  @param L size of the box
*/
void bonds_to_the_box(int B,vec3 **b0,const int *b1,double L){
  vec3 pm,t0tmp,t1tmp;
  int b1b;
#pragma omp parallel for private(pm,t0tmp,t1tmp,b1b)
  for(int b=0;b<B;b++){
    b1b=b1[b];
    if(b<b1b){
      pm=mult3(.5,add3((*b0)[b],(*b0)[b1b]));
      t0tmp=sub3((*b0)[b],pm);
      t1tmp=sub3((*b0)[b1b],pm);
      pm=to_the_box(pm,L);
      (*b0)[b]=add3(pm,t0tmp);
      (*b0)[b1b]=add3(pm,t1tmp);
    }
  }
}

/*! @brief this function stores the number of overlaps in the variable n_overlaps. If X<0, the number of overlaps between [0,B[ and [0,B[.
  @param B number of bonds
  @param X overlap(s) between [0,B[ and X.
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param n_overlaps number of overlaps (passed by reference)
  @param mean_overlap overlap average in percent (passed by reference)
  @param max_overlap overlap max in percent (passed by reference)
*/
void number_of_overlaps(int B,int X,double radius,double L,vec3 *b0,const int *b1,int &n_overlaps,double &mean_overlap,double &max_overlap){
  vec3 point1,point2;
  double min_distance2,nx,ny,nz,a1,a2;
  vec3 pb0,pb1,pc0,pc1,pc0tmp,pc1tmp;
  // bond to the box
  bonds_to_the_box(B,&b0,b1,L);
  // overlap(s) ?
  n_overlaps=0;
  mean_overlap=0.;
  max_overlap=0.;
  if(X<0){
    for(int b=0;b<(B-1);b++){
      pb0=b0[b];
      pb1=b0[b1[b]];
      for(int c=(b+1);c<B;c++){
	pc0=b0[c];
	pc1=b0[b1[c]];
	// look for the images
	min_distance2=DBL_MAX;
	for(int x=-1;x<2;x++){
	  for(int y=-1;y<2;y++){
	    for(int z=-1;z<2;z++){
	      pc0tmp=add3(pc0,vec3(x*L,y*L,z*L));
	      pc1tmp=add3(pc1,vec3(x*L,y*L,z*L));
	      minDist2segments_KKT(pb0,pb1,pc0tmp,pc1tmp,point1,point2,a1,a2,false,10000);
	      nx=point1.x-point2.x;
	      ny=point1.y-point2.y;
	      nz=point1.z-point2.z;
	      if((nx*nx+ny*ny+nz*nz)<min_distance2) min_distance2=nx*nx+ny*ny+nz*nz;
	    }
	  }
	}
	// overlap ?
	if(min_distance2<(4.*radius*radius)){
	  n_overlaps++;
	  mean_overlap+=(1.-sqrt(min_distance2)/(2.*radius));
	  max_overlap=max(max_overlap,1.-sqrt(min_distance2)/(2.*radius));
	}
      }
    }
  }else{
    pb0=b0[X];
    pb1=b0[b1[X]];
    for(int c=0;c<B;c++){
      if(X!=c){
	pc0=b0[c];
	pc1=b0[b1[c]];
	// look for the images
	min_distance2=DBL_MAX;
	for(int x=-1;x<2;x++){
	  for(int y=-1;y<2;y++){
	    for(int z=-1;z<2;z++){
	      pc0tmp=add3(pc0,vec3(x*L,y*L,z*L));
	      pc1tmp=add3(pc1,vec3(x*L,y*L,z*L));
	      minDist2segments_KKT(pb0,pb1,pc0tmp,pc1tmp,point1,point2,a1,a2,false,10000);
	      nx=point1.x-point2.x;
	      ny=point1.y-point2.y;
	      nz=point1.z-point2.z;
	      if((nx*nx+ny*ny+nz*nz)<min_distance2) min_distance2=nx*nx+ny*ny+nz*nz;
	    }
	  }
	}
	// overlap ?
	if(min_distance2<(4.*radius*radius)){
	  n_overlaps++;
	  mean_overlap+=(1.-sqrt(min_distance2)/(2.*radius));
	  max_overlap=max(max_overlap,1.-sqrt(min_distance2)/(2.*radius));
	}
      }
    }
  }
  mean_overlap/=n_overlaps;
}

/*! @brief get the possible overlap(s).
  @param B number of beads
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_possible_overlaps number of overlaps
*/
void get_the_possible_overlaps(int B,double radius,double L,vec3 *b0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int &n_possible_overlaps){
  bool is_openmp=false;
#ifdef _OPENMP
  is_openmp=true;
#endif
  vec3 point1,point2;
  const double alpha=2.1;
  int bx,by,bz,bxtmp,bytmp,bztmp,index,indextmp,tmpint,inf_x,inf_y,inf_z,sup_x,sup_y,sup_z,cc,b1b;
  double cx,cy,cz,nx,ny,nz,x_offset,y_offset,z_offset,radius_eff,size_block=0.,lb,lc,s1,s2;
  vec3 pb0,pb1,pm,pc0tmp,pc1tmp;
  // bond to the box
  bonds_to_the_box(B,&b0,b1,L);
  // block size of the space partition
  for(int b=0;b<(B-1);b++) size_block=fmax(size_block,distance_with_PBC(b0[b],b0[b1[b]],L)+alpha*radius);
  // size of the blocks, L/l=i+r but we want L/(l+x)=I
  int l_blocks=int(L/size_block);
  size_block=L/l_blocks;
  double inv_size_block=1./size_block;
  // where the bonds are ?
  double extent=-.5*L;
  int n_blocks=l_blocks*l_blocks*l_blocks;
  int bonds_per_block=(int)(1.1*max(1,B/n_blocks));
  int *n_bonds_in_block=new int[n_blocks]();
  for(int b=0;b<(B-1);b++){
    // mid of the bond (already in the box)
    b1b=b1[b];
    // connectivity ?
    if(b<b1b){
      pm=mult3(.5,add3(b0[b],b0[b1b]));
      bx=int((pm.x-extent)*inv_size_block);
      by=int((pm.y-extent)*inv_size_block);
      bz=int((pm.z-extent)*inv_size_block);
      index=bx*l_blocks*l_blocks+by*l_blocks+bz;
      n_bonds_in_block[index]++;
      // resize the partition ?
      if(n_bonds_in_block[index]>=bonds_per_block){
	bonds_per_block++;
	std::fill_n(n_bonds_in_block,n_blocks,0);
	b=-1;
      }
    }
  }
  // ???
  std::fill_n(n_bonds_in_block,n_blocks,0);
  int *block_to_bond=new int[n_blocks*bonds_per_block]();
  int *a1=new int[bonds_per_block]();
  int *a2=new int[bonds_per_block]();
  double *ad1=new double[bonds_per_block]();
  double *ad2=new double[bonds_per_block]();
  vec3 *a12=new vec3[bonds_per_block]();
  // loop over the active blocks
  n_possible_overlaps=0;
  for(int b=0;b<(B-1);b++){
    b1b=b1[b];
    // connectivity ?
    if(b<b1b){
      pb0=b0[b];
      pb1=b0[b1b];
      lb=norm3(sub3(pb0,pb1));
      // mid of the bond (already in the box)
      pm=mult3(.5,add3(pb0,pb1));
      bx=int((pm.x-extent)*inv_size_block);
      by=int((pm.y-extent)*inv_size_block);
      bz=int((pm.z-extent)*inv_size_block);
      index=bx*l_blocks*l_blocks+by*l_blocks+bz;    
      block_to_bond[index*bonds_per_block+n_bonds_in_block[index]]=b;
      n_bonds_in_block[index]++;
      // center of the current active block
      cx=extent+(bx+.5)*size_block;
      cy=extent+(by+.5)*size_block;
      cz=extent+(bz+.5)*size_block;
      // inf or sup part of the block
      radius_eff=(1.+alpha-2.)*radius;
      inf_x=(int)((pb0.x+radius_eff)<cx && (pb1.x+radius_eff)<cx);
      inf_y=(int)((pb0.y+radius_eff)<cy && (pb1.y+radius_eff)<cy);
      inf_z=(int)((pb0.z+radius_eff)<cz && (pb1.z+radius_eff)<cz);
      sup_x=(int)((pb0.x-radius_eff)>cx && (pb1.x-radius_eff)>cx);
      sup_y=(int)((pb0.y-radius_eff)>cy && (pb1.y-radius_eff)>cy);
      sup_z=(int)((pb0.z-radius_eff)>cz && (pb1.z-radius_eff)>cz);
      // loop over the neighborhood
      for(int x=sup_x-1;x<(2-inf_x);x++){
	bxtmp=bx+x;
	x_offset=((bxtmp>=l_blocks)-(bxtmp<0))*L;
	bxtmp+=((bxtmp<0)-(bxtmp>=l_blocks))*l_blocks;
	for(int y=sup_y-1;y<(2-inf_y);y++){
	  bytmp=by+y;
	  y_offset=((bytmp>=l_blocks)-(bytmp<0))*L;
	  bytmp+=((bytmp<0)-(bytmp>=l_blocks))*l_blocks;
	  for(int z=sup_z-1;z<(2-inf_z);z++){
	    bztmp=bz+z;
	    z_offset=((bztmp>=l_blocks)-(bztmp<0))*L;
	    bztmp+=((bztmp<0)-(bztmp>=l_blocks))*l_blocks;
	    indextmp=bxtmp*l_blocks*l_blocks+bytmp*l_blocks+bztmp;
	    // loop over the bonds in the block
	    tmpint=n_bonds_in_block[indextmp]-(index==indextmp);
	    if(tmpint<32 || !is_openmp){
	      for(int j=0;j<tmpint;j++){
		cc=block_to_bond[indextmp*bonds_per_block+j];
		pc0tmp=add3(b0[cc],vec3(x_offset,y_offset,z_offset));
		pc1tmp=add3(b0[b1[cc]],vec3(x_offset,y_offset,z_offset));
		lc=norm3(sub3(pc0tmp,pc1tmp));
		// calculate the minimal distance if ...
		if(dot_product2(sub3(pm,mult3(.5,add3(pc0tmp,pc1tmp))))<=((.5*(lb+lc)+2.*alpha*radius)*(.5*(lb+lc)+2.*alpha*radius))){
		  // minimal distance
		  minDist2segments_KKT(pc0tmp,pc1tmp,pb0,pb1,point1,point2,s1,s2,false,10000);
		  nx=point1.x-point2.x;
		  ny=point1.y-point2.y;
		  nz=point1.z-point2.z;
		  // overlap ?
		  if((nx*nx+ny*ny+nz*nz)<(alpha*radius*alpha*radius)){
		    // we always consider index first bond < index second bond
		    (*o1)[n_possible_overlaps]=cc;
		    (*o2)[n_possible_overlaps]=b;
		    (*od1)[n_possible_overlaps]=s1;
		    (*od2)[n_possible_overlaps]=s2;
		    (*o12)[n_possible_overlaps]=normalize3(sub3(point1,point2));
		    n_possible_overlaps++;
		  }
		}
	      }
	    }else{
#pragma omp parallel for private(cc,pc0tmp,pc1tmp,point1,point2,s1,s2,lc)
	      for(int j=0;j<tmpint;j++){
		cc=block_to_bond[indextmp*bonds_per_block+j];
		pc0tmp=add3(b0[cc],vec3(x_offset,y_offset,z_offset));
		pc1tmp=add3(b0[b1[cc]],vec3(x_offset,y_offset,z_offset));
		lc=norm3(sub3(pc0tmp,pc1tmp));
		// calculate the minimal distance if ...
		//if(dot_product2(sub3(pm,mult3(.5,add3(pc0tmp,pc1tmp))))<=((.5*(lb+lc)+2.*alpha*radius)*(.5*(lb+lc)+2.*alpha*radius))){
		// minimal distance
		minDist2segments_KKT(pc0tmp,pc1tmp,pb0,pb1,point1,point2,s1,s2,false,10000);
		// we always consider index first bond < index second bond
		a1[j]=cc;
		a2[j]=b;
		ad1[j]=s1;
		ad2[j]=s2;
		a12[j]=sub3(point1,point2);
		//}
	      }
	      for(int j=0;j<tmpint;j++){
		// overlap ?
		if(dot_product2(a12[j])<(alpha*radius*alpha*radius)){
		  (*o1)[n_possible_overlaps]=a1[j];
		  (*o2)[n_possible_overlaps]=a2[j];
		  (*od1)[n_possible_overlaps]=ad1[j];
		  (*od2)[n_possible_overlaps]=ad2[j];
		  (*o12)[n_possible_overlaps]=normalize3(a12[j]);
		  n_possible_overlaps++;
		}
	      }
	    }
	  }// Fi 'z'
	}// Fi 'y'
      }// Fi 'x'
    }
  }
  // ???
  delete[] a1;
  delete[] a2;
  delete[] ad1;
  delete[] ad2;
  delete[] a12;
  delete[] block_to_bond;
  delete[] n_bonds_in_block;
}

/*! @brief get the overlap(s) from the result of the function 'get_the_possible_overlaps'.
  @param B number of beads
  @param radius radius of the bonds
  @param L box size
  @param b0 position of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_possible_overlaps number of possible overlap(s)
  @param n_overlaps number of overlap(s)
  @param mean_overlap overlap average in pourcent
  @param max_overlap overlap max in pourcent
*/
void get_the_overlaps(int B,double radius,double L,vec3 *b0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_possible_overlaps,int &n_overlaps,double &mean_overlap,double &max_overlap){
  bool is_openmp=false;
#ifdef _OPENMP
  is_openmp=true;
#endif
  int i1,i2;
  double lb,lc,min_distance,*min_distances=new double[n_possible_overlaps](),a1,a2;
  const double inv_diameter=1./(2.*radius);
  vec3 point1,point2,pb0,pm,pb1,pc0,pc1,pc0tmp,pc1tmp,offset;
  // bonds to the box
  bonds_to_the_box(B,&b0,b1,L);
  // loop over the possible overlaps
  mean_overlap=0.;
  max_overlap=0.;
  n_overlaps=0;
#pragma omp parallel for private(i1,i2,pb0,pb1,pc0,pc1,offset,pm,pc0tmp,pc1tmp,lb,lc,point1,point2,a1,a2,min_distance)
  for(int i=0;i<n_possible_overlaps;i++){
    i1=(*o1)[i];
    i2=(*o2)[i];
    pb0=b0[i1];
    pb1=b0[b1[i1]];
    pm=mult3(.5,add3(pb0,pb1));
    lb=norm3(sub3(pb0,pb1));
    pc0=b0[i2];
    pc1=b0[b1[i2]];
    lc=norm3(sub3(pc0,pc1));
    // loop over the images and find the minimal distance
    min_distances[i]=DBL_MAX;
    for(int x=-1;x<2;x++){
      offset.x=x*L;
      for(int y=-1;y<2;y++){
	offset.y=y*L;
	for(int z=-1;z<2;z++){
	  offset.z=z*L;
	  pc0tmp=add3(pc0,offset);
	  pc1tmp=add3(pc1,offset);
	  // calculate the minimal distance if ...
	  if(dot_product2(sub3(pm,mult3(.5,add3(pc0tmp,pc1tmp))))<=((.5*(lb+lc)+2.*radius)*(.5*(lb+lc)+2.*radius))){
	    minDist2segments_KKT(pb0,pb1,pc0tmp,pc1tmp,point1,point2,a1,a2,false,10000);
	    min_distance=norm3(sub3(point1,point2));
	    // overlap and minimal distance amongst the minimal distances ?
	    if(min_distance<min_distances[i] && (min_distance*inv_diameter)<1.){
	      min_distances[i]=min_distance;
	      (*od1)[i]=a1;
	      (*od2)[i]=a2;
	      (*o12)[i]=normalize3(sub3(point1,point2));
	    }
	  }
	}
      }
    }
  }
  // minimal distance amongst the minimal distances
  if(n_possible_overlaps<32 || !is_openmp){
    for(int i=0;i<n_possible_overlaps;i++){
      min_distance=min_distances[i];
      if(min_distance!=DBL_MAX){
	(*o1)[n_overlaps]=(*o1)[i];
	(*o2)[n_overlaps]=(*o2)[i];
	(*od1)[n_overlaps]=(*od1)[i];
	(*od2)[n_overlaps]=(*od2)[i];
	(*o12)[n_overlaps]=(*o12)[i];
	n_overlaps++;
	mean_overlap+=(1.-min_distance*inv_diameter);
	max_overlap=fmax(max_overlap,1.-min_distance*inv_diameter);
	if(max_overlap>.5){
	  printf("maximum overlap (%i/%i) between %i and %i is greater than 50 percent, exit.\n",i,n_possible_overlaps,(*o1)[i],(*o2)[i]);
	  exit(-1);
	}
      }
    }
  }else{
    // OpenMP version
    int *a1=new int[n_possible_overlaps]();
    int *a2=new int[n_possible_overlaps]();
    double *ad1=new double[n_possible_overlaps]();
    double *ad2=new double[n_possible_overlaps]();
    vec3 *a12=new vec3[n_possible_overlaps]();
    for(int i=0;i<n_possible_overlaps;i++){
      min_distance=min_distances[i];
      if(min_distance!=DBL_MAX){
	a1[n_overlaps]=(*o1)[i];
	a2[n_overlaps]=(*o2)[i];
	ad1[n_overlaps]=(*od1)[i];
	ad2[n_overlaps]=(*od2)[i];
	a12[n_overlaps]=(*o12)[i];
	n_overlaps++;
	mean_overlap+=(1.-min_distance*inv_diameter);
	max_overlap=fmax(max_overlap,1.-min_distance*inv_diameter);
	if(max_overlap>.5){
	  printf("(OpenMP version) maximum overlap (%i/%i) between %i and %i is greater than 50 percent, exit.\n",i,n_possible_overlaps,(*o1)[i],(*o2)[i]);
	  exit(-1);
	}
      }
    }
#pragma omp parallel for
    for(int i=0;i<n_overlaps;i++){
      (*o1)[i]=a1[i];
      (*o2)[i]=a2[i];
      (*od1)[i]=ad1[i];
      (*od2)[i]=ad2[i];
      (*o12)[i]=a12[i];
    }
    delete[] a1;
    delete[] a2;
    delete[] ad1;
    delete[] ad2;
    delete[] a12;
  }
  mean_overlap/=n_overlaps;
  delete[] min_distances;
}

/*! @brief resolves the excluded volume constraints with velocity-based method.
  @param B number of beads
  @param v0 velocity of each bead
  @param b1 connectivity
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_overlaps number of overlaps
  @param test does the function test for relative velocity along the normal to the contact ?
*/
void velocities_resolve_EVC(int B,vec3 **v0,const int *b1,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_overlaps,bool test){
  double abs_tol=1e-6,rel_tol=1e-4;
  double d0,d1,d2,d3,d4,U,nx,ny,nz,z1,z2,w_sor=1.3;
  int max_i_sor=0,i_sor;//l_overlaps_01=0;
  int P=2*B;
  int nb,nc,n_overlaps_per_graph=0,i0,i1,i2,i3,i4,m1,m2,m3,m4,l_graph=0;
  bool refus;
  int *sort_p=new int[P];
  std::fill_n(sort_p,P,-1);
  int *p_graph=new int[P]();
  std::fill_n(p_graph,P,-1);
  int *p_sort=new int[4*n_overlaps]();
  double *Bcoll=new double[n_overlaps]();
  double *fcoll=new double[n_overlaps]();
  double *Fcoll=new double[n_overlaps]();
  bool *deja_vu=new bool[n_overlaps]();
  // int *overlap_01=new int[n_overlaps]();
  int *l_overlaps=new int[n_overlaps]();
  int *l_i_graph=new int[n_overlaps]();
  // ordering the overlap index and check it
  vector<pair<int,int>> overlap_i(n_overlaps);
  for(int i=0;i<n_overlaps;i++) overlap_i[i]=std::make_pair(B*(*o1)[i]+(*o2)[i],i);
  sort(overlap_i.begin(),overlap_i.end());
  for(int i=0;i<n_overlaps;i++) l_overlaps[i]=std::get<1>(overlap_i[i]);
  for(int i=0;i<(n_overlaps-1);i++) if(std::get<0>(overlap_i[i])>std::get<0>(overlap_i[i+1])){printf("order overlap index\n");exit(-1);}
  overlap_i.resize(0);
  // J*V
  // l_overlaps_01=0;
  // it is of no use for small number of overlaps
#pragma omp parallel for private(i0,i1,i2,d1,d2,U)
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    i1=(*o1)[i0];
    i2=(*o2)[i0];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    U=dot_product((*o12)[i0],sub3(add3(mult3(1.-d1,(*v0)[i1]),mult3(d1,(*v0)[b1[i1]])),add3(mult3(1.-d2,(*v0)[i2]),mult3(d2,(*v0)[b1[i2]]))));
    Bcoll[i]=U*(1-2*(U<0.))-U;
    deja_vu[i0]=false;
    // if(d1==0. || d1==1. || d2==0. || d2==1.){
    //   overlap_01[l_overlaps_01]=i0;
    //   l_overlaps_01++;
    // }
  }
  // // "already seen" ?
  // // (a,b) ((m1,m1+1),(m2,m2+1))
  // // (c,d) ((m3,m3+1),(m4,m4+1))
  // for(int i=0;i<(l_overlaps_01-1);i++){
  //   i0=overlap_01[i];
  //   nb=arm_to_ichr[i_bchr[dans_1[i0]]];
  //   nc=arm_to_ichr[i_bchr[dans_2[i0]]];
  //   m1=2*(*o1)[i0];
  //   m2=2*(*o2)[i0];
  //   d1=dans_d1[i0];
  //   d2=dans_d2[i0];
  //   for(int j=(i+1);j<l_overlaps_01;j++){
  //     i1=overlap_01[j];
  //     if(nb==arm_to_ichr[i_bchr[dans_1[i1]]] && nc==arm_to_ichr[i_bchr[dans_2[i1]]]){
  // 	m3=2*(*o1)[i1];
  // 	m4=2*(*o2)[i1];
  // 	d3=dans_d1[i1];
  // 	d4=dans_d2[i1];
  // 	// a+1==c, b==d ou b+1==d, a==c (un point et un corps) ?
  // 	deja_vu[i1]|=((((m1+1)==m3 && d1==1. && d3==0.) || (m1==(m3+1) && d1==0. && d3==1.)) && (((m2+1)==m4 && d2==1. && d4==0.) || (m2==(m4+1) && d2==0. && d4==1.)));
  // 	deja_vu[i1]|=((((m1+1)==m3 && d1==1. && d3==0.) || (m1==(m3+1) && d1==0. && d3==1.)) && (m2==m4 && d2==d4));
  // 	deja_vu[i1]|=((((m2+1)==m4 && d2==1. && d4==0.) || (m2==(m4+1) && d2==0. && d4==1.)) && (m1==m3 && d1==d3));
  //     }
  //   }
  // }
  // delete[] overlap_01;
  // delete the "deja vu"
  i1=0;
  for(int i=0;i<n_overlaps;i++) i1+=(int)(!deja_vu[i]);
  i0=n_overlaps;
  nb=0;
  while(nb<i0){
    if(deja_vu[l_overlaps[nb]]){
      for(int i=nb;i<(i0-1);i++) l_overlaps[i]=l_overlaps[i+1];
      i0--;
    }else nb++;
  }
  n_overlaps=i0;
  if(i1!=n_overlaps){
    printf("error : delete the 'deja vu' %i/%i\n\n",i1,n_overlaps);
    exit(-1);
  }
  // collision(s) graphs
  int Pcoll=0;
  int Rcoll=n_overlaps;
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    i1=(*o1)[i0];
    i2=(*o2)[i0];
    m1=2*i1;
    m2=2*i2;
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    // does the current overlap belong to already existing collision graph ?
    if(p_graph[m1+(d1==1.)]==-1 && p_graph[m1+1-(d1==0.)]==-1 && p_graph[m2+(d2==1.)]==-1 && p_graph[m2+1-(d2==0.)]==-1){
      if(d1<1.) p_graph[m1]=l_graph;
      if(d1>0.)	p_graph[m1+1]=l_graph;
      if(d2<1.) p_graph[m2]=l_graph;
      if(d2>0.)	p_graph[m2+1]=l_graph;
      // find all the children of the current overlap : build the new collision graph.
      while(1){
	refus=true;
	for(int j=(i+1);j<n_overlaps;j++){
	  nc=l_overlaps[j];
	  i3=(*o1)[nc];
	  i4=(*o2)[nc];
	  m3=2*i3;
	  m4=2*i4;
	  d3=(*od1)[nc];
	  d4=(*od2)[nc];
	  if(p_graph[m3+(d3==1.)]==l_graph || p_graph[m3+1-(d3==0.)]==l_graph || p_graph[m4+(d4==1.)]==l_graph || p_graph[m4+1-(d4==0.)]==l_graph){
	    if(p_graph[m3+(d3==1.)]==-1){
	      refus=false;
	      p_graph[m3+(d3==1.)]=l_graph;
	    }
	    if(p_graph[m3+1-(d3==0.)]==-1){
	      refus=false;
	      p_graph[m3+1-(d3==0.)]=l_graph;
	    }
	    if(p_graph[m4+(d4==1.)]==-1){
	      refus=false;
	      p_graph[m4+(d4==1.)]=l_graph;
	    }
	    if(p_graph[m4+1-(d4==0.)]==-1){
	      refus=false;
	      p_graph[m4+1-(d4==0.)]=l_graph;
	    }
	  }
	}
	if(refus) break;
      }// Elihw
      // count the number of overlaps for the current graph.
      for(int j=0;j<n_overlaps;j++){
	nc=l_overlaps[j];
	m3=2*(*o1)[nc];
	m4=2*(*o2)[nc];
	d3=(*od1)[nc];
	d4=(*od2)[nc];
	l_i_graph[l_graph]+=(p_graph[m3+(d3==1.)]==l_graph || p_graph[m3+1-(d3==0.)]==l_graph || p_graph[m4+(d4==1.)]==l_graph || p_graph[m4+1-(d4==0.)]==l_graph);
      }
      l_graph++;
    }else{
      if(p_graph[m1+(d1==1.)]==-1 || p_graph[m1+1-(d1==0.)]==-1 || p_graph[m2+(d2==1.)]==-1 || p_graph[m2+1-(d2==0.)]==-1){
	printf("error : %i : %i %i %i %i, %i %i %i %i (%f,%f) / -1\n",i,p_graph[m1+(d1==1.)],p_graph[m1+1-(d1==0.)],p_graph[m2+(d2==1.)],p_graph[m2+1-(d2==0.)],m1,m1+1,m2,m2+1,d1,d2);
	exit(-1);
      }
    }
    // bijection index bead - order
    if(sort_p[m1]==-1){
      sort_p[m1]=Pcoll;
      p_sort[Pcoll]=m1;
      Pcoll++;
    }
    if(sort_p[m1+1]==-1){
      sort_p[m1+1]=Pcoll;
      p_sort[Pcoll]=m1+1;
      Pcoll++;
    }
    if(sort_p[m2]==-1){
      sort_p[m2]=Pcoll;
      p_sort[Pcoll]=m2;
      Pcoll++;
    }
    if(sort_p[m2+1]==-1){
      sort_p[m2+1]=Pcoll;
      p_sort[Pcoll]=m2+1;
      Pcoll++;
    }
  }
  // overlap(s) per graph
  for(int i=0;i<l_graph;i++){
    n_overlaps_per_graph=max(n_overlaps_per_graph,l_i_graph[i]);
    l_i_graph[i]=0;
  }
  // index of the overlap(s) for each collision graph
  int *i_graph=new int[l_graph*n_overlaps_per_graph]();
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    m1=2*(*o1)[i0];
    m2=2*(*o2)[i0];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    i1=p_graph[m1+(d1==1.)];
    i2=p_graph[m1+1-(d1==0.)];
    i3=p_graph[m2+(d2==1.)];
    i4=p_graph[m2+1-(d2==0.)];
    i_graph[i1*n_overlaps_per_graph+l_i_graph[i1]]=i;
    l_i_graph[i1]++;
    if(i1!=i2){
      i_graph[i2*n_overlaps_per_graph+l_i_graph[i2]]=i;
      l_i_graph[i2]++;
    }
    if(i1!=i3 && i2!=i3){
      i_graph[i3*n_overlaps_per_graph+l_i_graph[i3]]=i;
      l_i_graph[i3]++;
    }
    if(i1!=i4 && i2!=i4 && i3!=i4){
      i_graph[i4*n_overlaps_per_graph+l_i_graph[i4]]=i;
      l_i_graph[i4]++;
    }
  }
  // sort the overlap index for each graph and check it
  for(int g=0;g<l_graph;g++){
    std::sort(i_graph+g*n_overlaps_per_graph,i_graph+g*n_overlaps_per_graph+l_i_graph[g]);
    for(int i=0;i<(l_i_graph[g]-1);i++){
      if(i_graph[g*n_overlaps_per_graph+i]>i_graph[g*n_overlaps_per_graph+i+1]){
	printf("error : order graph\n");
	exit(-1);
      }
    }
  }
  // resolve the excluded volume constraints
  double *Jcoll=new double[Rcoll*3*Pcoll]();
  double *JTl=new double[3*Pcoll]();
  int *Jcoll_nzero=new int[12*Rcoll]();
  double *JJT=new double[Rcoll*Rcoll]();
  // build the matrix J
#pragma omp parallel for private(i0,d1,d2,nx,ny,nz,m1,m2,i1,i2,i3,i4)
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    nx=(*o12)[i0].x;
    ny=(*o12)[i0].y;
    nz=(*o12)[i0].z;
    m1=2*(*o1)[i0];
    m2=2*(*o2)[i0];
    i1=3*sort_p[m1];
    i2=3*sort_p[m1+1];
    i3=3*sort_p[m2];
    i4=3*sort_p[m2+1];
    Jcoll[i*(3*Pcoll)+i1]=(1.-d1)*nx;
    Jcoll[i*(3*Pcoll)+i1+1]=(1.-d1)*ny;
    Jcoll[i*(3*Pcoll)+i1+2]=(1.-d1)*nz;
    Jcoll[i*(3*Pcoll)+i2]=d1*nx;
    Jcoll[i*(3*Pcoll)+i2+1]=d1*ny;
    Jcoll[i*(3*Pcoll)+i2+2]=d1*nz;
    Jcoll[i*(3*Pcoll)+i3]=(d2-1.)*nx;
    Jcoll[i*(3*Pcoll)+i3+1]=(d2-1.)*ny;
    Jcoll[i*(3*Pcoll)+i3+2]=(d2-1.)*nz;
    Jcoll[i*(3*Pcoll)+i4]=-d2*nx;
    Jcoll[i*(3*Pcoll)+i4+1]=-d2*ny;
    Jcoll[i*(3*Pcoll)+i4+2]=-d2*nz;
    Jcoll_nzero[12*i]=i*(3*Pcoll)+i1;
    Jcoll_nzero[12*i+1]=i*(3*Pcoll)+i1+1;
    Jcoll_nzero[12*i+2]=i*(3*Pcoll)+i1+2;
    Jcoll_nzero[12*i+3]=i*(3*Pcoll)+i2;
    Jcoll_nzero[12*i+4]=i*(3*Pcoll)+i2+1;
    Jcoll_nzero[12*i+5]=i*(3*Pcoll)+i2+2;
    Jcoll_nzero[12*i+6]=i*(3*Pcoll)+i3;
    Jcoll_nzero[12*i+7]=i*(3*Pcoll)+i3+1;
    Jcoll_nzero[12*i+8]=i*(3*Pcoll)+i3+2;
    Jcoll_nzero[12*i+9]=i*(3*Pcoll)+i4;
    Jcoll_nzero[12*i+10]=i*(3*Pcoll)+i4+1;
    Jcoll_nzero[12*i+11]=i*(3*Pcoll)+i4+2;
  }
  // build of J*J^T
  for(int g=0;g<l_graph;g++){
    // graph "g"
    i2=l_i_graph[g];
    for(int i=0;i<i2;i++){
      i0=i_graph[g*n_overlaps_per_graph+i];
      for(int j=i;j<i2;j++){
	i1=i_graph[g*n_overlaps_per_graph+j];
	d0=0.;
	for(int k=0;k<12;k++){
	  nb=Jcoll_nzero[12*i0+k]-i0*3*Pcoll;
	  d0+=Jcoll[i0*3*Pcoll+nb]*Jcoll[i1*3*Pcoll+nb];
	}
	JJT[i0*Rcoll+i1]=d0;
	JJT[i1*Rcoll+i0]=d0;
      }
      // build of L
      fcoll[i0]=fmax(0.,Bcoll[i0]/JJT[i0*Rcoll+i0]);
    }
    // Successive-Over-Relaxation (J*J^T*L=B with L>=0) and loop over collisions graphs
    i_sor=0;
    while(i_sor<100){
      for(int i=0;i<i2;i++){
	i0=i_graph[g*n_overlaps_per_graph+i];
	d0=0.;
	for(int j=0;j<i;j++){
	  i1=i_graph[g*n_overlaps_per_graph+j];
	  d0+=JJT[i0*Rcoll+i1]*Fcoll[i1];
	}
	d1=0.;
	for(int j=(i+1);j<i2;j++){
	  i1=i_graph[g*n_overlaps_per_graph+j];
	  d1+=JJT[i0*Rcoll+i1]*fcoll[i1];
	}
	Fcoll[i0]=fmax(0.,(1.-w_sor)*fcoll[i0]+w_sor*(Bcoll[i0]-d0-d1)/JJT[i0*Rcoll+i0]);
      }
      // stop condition
      refus=false;
      for(int i=0;i<i2;i++){
	i0=i_graph[g*n_overlaps_per_graph+i];
	z1=fcoll[i0];
	z2=Fcoll[i0];
	refus=(refus || (fabs(z2-z1)>(abs_tol+rel_tol*fabs(z1))));
	fcoll[i0]=z2;
      }
      i_sor++;
      if(!refus) break;
    }//elihw
    max_i_sor=max(max_i_sor,i_sor);
  }// Rof "g"
  // add the forces : J^T*lambda
  for(int i=0;i<n_overlaps;i++){
    d0=Fcoll[i];
    for(int j=0;j<12;j++){
      i0=Jcoll_nzero[12*i+j];
      JTl[i0-i*3*Pcoll]+=Jcoll[i0]*d0;
    }
  }
  // change the velocities
#pragma omp parallel for private(i0,i1)
  for(int i=0;i<Pcoll;i++){
    i0=p_sort[i];
    i1=(i0-i0%2)/2;
    (*v0)[i1]=add3((*v0)[i1],vec3(JTl[3*i],JTl[3*i+1],JTl[3*i+2]));
  }
  // U>0 test
  if(test){
    for(int i=0;i<n_overlaps;i++){
      i0=l_overlaps[i];
      i1=(*o1)[i0];
      i2=(*o2)[i0];
      d1=(*od1)[i0];
      d2=(*od2)[i0];
      U=dot_product((*o12)[i0],sub3(add3(mult3(1.-d1,(*v0)[i1]),mult3(d1,(*v0)[b1[i1]])),add3(mult3(1.-d2,(*v0)[i2]),mult3(d2,(*v0)[b1[i2]]))));
      if(U<0.){
	printf("----------------- %i %i : %f(%f), %i, %i overlap(s) %i graph(s)\n",i1,i2,U,Bcoll[i],max_i_sor,n_overlaps,l_graph);
	for(int g=0;g<l_graph;g++){
	  printf("\n%i\n",l_i_graph[g]);
	  for(int j=0;j<l_i_graph[g];j++){
	    i0=i_graph[g*n_overlaps_per_graph+j];
	    i0=l_overlaps[i0];
	    i1=(*o1)[i0];
	    i2=(*o2)[i0];
	    printf("(U<0) %i : %i(%i %i : %f) %i(%i %i : %f)\n",g,i1,p_graph[2*i1],p_graph[2*i1+1],(*od1)[i0],i2,p_graph[2*i2],p_graph[2*i2+1],(*od2)[i0]);
	  }
	}
	exit(-1);
      }
    }
  }
  // cleanup
  delete[] deja_vu;
  delete[] i_graph;
  delete[] l_i_graph;
  delete[] l_overlaps;
  delete[] p_sort;
  delete[] sort_p;
  delete[] Bcoll;
  delete[] fcoll;
  delete[] Fcoll;
  delete[] Jcoll;
  delete[] JTl;
  delete[] Jcoll_nzero;
  delete[] JJT;
  delete[] p_graph;
}

/*! @brief resolves the excluded volume constraints with velocity-based method.
  @param B number of beads
  @param rdt reduced time-step dt/2*mass
  @param v0 velocity of each bead
  @param b1 connectivity
  @param f0 force applied to each bead
  @param o1 first bond from the overlap
  @param o2 second bond from the overlap
  @param od1 minimal distance position on bond 1
  @param od2 minimal distance position on bond 2
  @param o12 minimal distance vector from bond 2 to bond 1
  @param n_overlaps number of overlaps
  @param test does the function test for relative velocity along the normal to the contact ?
*/
void fvelocities_resolve_EVC(int B,double rdt,vec3 **v0,const int *b1,vec3 **f0,int **o1,int **o2,double **od1,double **od2,vec3 **o12,int n_overlaps,bool test){
  double d0,d1,d2,d3,d4,U,F,nx,ny,nz,z1,z2,w_sor=1.3;
  int max_i_sor=0,i_sor;//l_overlaps_01=0;
  int P=2*B;
  int nb,nc,n_overlaps_per_graph=0,i0,i1,i2,i3,i4,m1,m2,m3,m4,l_graph=0;
  bool refus;
  int *sort_p=new int[P];
  std::fill_n(sort_p,P,-1);
  int *p_graph=new int[P]();
  std::fill_n(p_graph,P,-1);
  int *p_sort=new int[4*n_overlaps]();
  double *Bcoll=new double[n_overlaps]();
  double *fcoll=new double[n_overlaps]();
  double *Fcoll=new double[n_overlaps]();
  bool *deja_vu=new bool[n_overlaps]();
  // int *overlap_01=new int[n_overlaps]();
  int *l_overlaps=new int[n_overlaps]();
  int *l_i_graph=new int[n_overlaps]();
  // ordering the overlap index and check it
  vector<pair<int,int>> overlap_i(n_overlaps);
  for(int i=0;i<n_overlaps;i++) overlap_i[i]=std::make_pair(B*(*o1)[i]+(*o2)[i],i);
  sort(overlap_i.begin(),overlap_i.end());
  for(int i=0;i<n_overlaps;i++) l_overlaps[i]=std::get<1>(overlap_i[i]);
  for(int i=0;i<(n_overlaps-1);i++) if(std::get<0>(overlap_i[i])>std::get<0>(overlap_i[i+1])){printf("order overlap index\n");exit(-1);}
  overlap_i.resize(0);
  // J*(V+rdt*F)
  // l_overlaps_01=0;
  // it is of no use for small number of overlaps
#pragma omp parallel for private(i0,i1,i2,d1,d2,U)
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    i1=(*o1)[i0];
    i2=(*o2)[i0];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    U=dot_product((*o12)[i0],sub3(add3(mult3(1.-d1,(*v0)[i1]),mult3(d1,(*v0)[b1[i1]])),add3(mult3(1.-d2,(*v0)[i2]),mult3(d2,(*v0)[b1[i2]]))));
    F=dot_product((*o12)[i0],sub3(add3(mult3(1.-d1,(*f0)[i1]),mult3(d1,(*f0)[b1[i1]])),add3(mult3(1.-d2,(*f0)[i2]),mult3(d2,(*f0)[b1[i2]]))));
    Bcoll[i]=U*(1-2*(U<0.))-U-rdt*F;
    deja_vu[i0]=false;
    // if(d1==0. || d1==1. || d2==0. || d2==1.){
    //   overlap_01[l_overlaps_01]=i0;
    //   l_overlaps_01++;
    // }
  }
  // // "already seen" ?
  // // (a,b) ((m1,m1+1),(m2,m2+1))
  // // (c,d) ((m3,m3+1),(m4,m4+1))
  // for(int i=0;i<(l_overlaps_01-1);i++){
  //   i0=overlap_01[i];
  //   nb=arm_to_ichr[i_bchr[dans_1[i0]]];
  //   nc=arm_to_ichr[i_bchr[dans_2[i0]]];
  //   m1=2*(*o1)[i0];
  //   m2=2*(*o2)[i0];
  //   d1=dans_d1[i0];
  //   d2=dans_d2[i0];
  //   for(int j=(i+1);j<l_overlaps_01;j++){
  //     i1=overlap_01[j];
  //     if(nb==arm_to_ichr[i_bchr[dans_1[i1]]] && nc==arm_to_ichr[i_bchr[dans_2[i1]]]){
  // 	m3=2*(*o1)[i1];
  // 	m4=2*(*o2)[i1];
  // 	d3=dans_d1[i1];
  // 	d4=dans_d2[i1];
  // 	// a+1==c, b==d ou b+1==d, a==c (un point et un corps) ?
  // 	deja_vu[i1]|=((((m1+1)==m3 && d1==1. && d3==0.) || (m1==(m3+1) && d1==0. && d3==1.)) && (((m2+1)==m4 && d2==1. && d4==0.) || (m2==(m4+1) && d2==0. && d4==1.)));
  // 	deja_vu[i1]|=((((m1+1)==m3 && d1==1. && d3==0.) || (m1==(m3+1) && d1==0. && d3==1.)) && (m2==m4 && d2==d4));
  // 	deja_vu[i1]|=((((m2+1)==m4 && d2==1. && d4==0.) || (m2==(m4+1) && d2==0. && d4==1.)) && (m1==m3 && d1==d3));
  //     }
  //   }
  // }
  // delete[] overlap_01;
  // delete the "deja vu"
  i1=0;
  for(int i=0;i<n_overlaps;i++) i1+=(int)(!deja_vu[i]);
  i0=n_overlaps;
  nb=0;
  while(nb<i0){
    if(deja_vu[l_overlaps[nb]]){
      for(int i=nb;i<(i0-1);i++) l_overlaps[i]=l_overlaps[i+1];
      i0--;
    }else nb++;
  }
  n_overlaps=i0;
  if(i1!=n_overlaps){
    printf("error : delete the 'deja vu' %i/%i\n\n",i1,n_overlaps);
    exit(-1);
  }
  // collision(s) graphs
  int Pcoll=0;
  int Rcoll=n_overlaps;
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    i1=(*o1)[i0];
    i2=(*o2)[i0];
    m1=2*i1;
    m2=2*i2;
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    // does the current overlap belong to already existing collision graph ?
    if(p_graph[m1+(d1==1.)]==-1 && p_graph[m1+1-(d1==0.)]==-1 && p_graph[m2+(d2==1.)]==-1 && p_graph[m2+1-(d2==0.)]==-1){
      if(d1<1.) p_graph[m1]=l_graph;
      if(d1>0.)	p_graph[m1+1]=l_graph;
      if(d2<1.) p_graph[m2]=l_graph;
      if(d2>0.)	p_graph[m2+1]=l_graph;
      // find all the children of the current overlap : build the new collision graph.
      while(1){
	refus=true;
	for(int j=(i+1);j<n_overlaps;j++){
	  nc=l_overlaps[j];
	  i3=(*o1)[nc];
	  i4=(*o2)[nc];
	  m3=2*i3;
	  m4=2*i4;
	  d3=(*od1)[nc];
	  d4=(*od2)[nc];
	  if(p_graph[m3+(d3==1.)]==l_graph || p_graph[m3+1-(d3==0.)]==l_graph || p_graph[m4+(d4==1.)]==l_graph || p_graph[m4+1-(d4==0.)]==l_graph){
	    if(p_graph[m3+(d3==1.)]==-1){
	      refus=false;
	      p_graph[m3+(d3==1.)]=l_graph;
	    }
	    if(p_graph[m3+1-(d3==0.)]==-1){
	      refus=false;
	      p_graph[m3+1-(d3==0.)]=l_graph;
	    }
	    if(p_graph[m4+(d4==1.)]==-1){
	      refus=false;
	      p_graph[m4+(d4==1.)]=l_graph;
	    }
	    if(p_graph[m4+1-(d4==0.)]==-1){
	      refus=false;
	      p_graph[m4+1-(d4==0.)]=l_graph;
	    }
	  }
	}
	if(refus) break;
      }// Elihw
      // count the number of overlaps for the current graph.
      for(int j=0;j<n_overlaps;j++){
	nc=l_overlaps[j];
	m3=2*(*o1)[nc];
	m4=2*(*o2)[nc];
	d3=(*od1)[nc];
	d4=(*od2)[nc];
	l_i_graph[l_graph]+=(p_graph[m3+(d3==1.)]==l_graph || p_graph[m3+1-(d3==0.)]==l_graph || p_graph[m4+(d4==1.)]==l_graph || p_graph[m4+1-(d4==0.)]==l_graph);
      }
      l_graph++;
    }else{
      if(p_graph[m1+(d1==1.)]==-1 || p_graph[m1+1-(d1==0.)]==-1 || p_graph[m2+(d2==1.)]==-1 || p_graph[m2+1-(d2==0.)]==-1){
	printf("error : %i : %i %i %i %i, %i %i %i %i (%f,%f) / -1\n",i,p_graph[m1+(d1==1.)],p_graph[m1+1-(d1==0.)],p_graph[m2+(d2==1.)],p_graph[m2+1-(d2==0.)],m1,m1+1,m2,m2+1,d1,d2);
	exit(-1);
      }
    }
    // bijection index bead - order
    if(sort_p[m1]==-1){
      sort_p[m1]=Pcoll;
      p_sort[Pcoll]=m1;
      Pcoll++;
    }
    if(sort_p[m1+1]==-1){
      sort_p[m1+1]=Pcoll;
      p_sort[Pcoll]=m1+1;
      Pcoll++;
    }
    if(sort_p[m2]==-1){
      sort_p[m2]=Pcoll;
      p_sort[Pcoll]=m2;
      Pcoll++;
    }
    if(sort_p[m2+1]==-1){
      sort_p[m2+1]=Pcoll;
      p_sort[Pcoll]=m2+1;
      Pcoll++;
    }
  }
  // overlap(s) per graph
  for(int i=0;i<l_graph;i++){
    n_overlaps_per_graph=max(n_overlaps_per_graph,l_i_graph[i]);
    l_i_graph[i]=0;
  }
  // index of the overlap(s) for each collision graph
  int *i_graph=new int[l_graph*n_overlaps_per_graph]();
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    m1=2*(*o1)[i0];
    m2=2*(*o2)[i0];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    i1=p_graph[m1+(d1==1.)];
    i2=p_graph[m1+1-(d1==0.)];
    i3=p_graph[m2+(d2==1.)];
    i4=p_graph[m2+1-(d2==0.)];
    i_graph[i1*n_overlaps_per_graph+l_i_graph[i1]]=i;
    l_i_graph[i1]++;
    if(i1!=i2){
      i_graph[i2*n_overlaps_per_graph+l_i_graph[i2]]=i;
      l_i_graph[i2]++;
    }
    if(i1!=i3 && i2!=i3){
      i_graph[i3*n_overlaps_per_graph+l_i_graph[i3]]=i;
      l_i_graph[i3]++;
    }
    if(i1!=i4 && i2!=i4 && i3!=i4){
      i_graph[i4*n_overlaps_per_graph+l_i_graph[i4]]=i;
      l_i_graph[i4]++;
    }
  }
  // sort the overlap index for each graph and check it
  for(int g=0;g<l_graph;g++){
    std::sort(i_graph+g*n_overlaps_per_graph,i_graph+g*n_overlaps_per_graph+l_i_graph[g]);
    for(int i=0;i<(l_i_graph[g]-1);i++){
      if(i_graph[g*n_overlaps_per_graph+i]>i_graph[g*n_overlaps_per_graph+i+1]){
	printf("error : order graph\n");
	exit(-1);
      }
    }
  }
  // resolve the excluded volume constraints
  double *Jcoll=new double[Rcoll*3*Pcoll]();
  double *JTl=new double[3*Pcoll]();
  int *Jcoll_nzero=new int[12*Rcoll]();
  double *JJT=new double[Rcoll*Rcoll]();
  // build the matrix J
#pragma omp parallel for private(i0,d1,d2,nx,ny,nz,m1,m2,i1,i2,i3,i4)
  for(int i=0;i<n_overlaps;i++){
    i0=l_overlaps[i];
    d1=(*od1)[i0];
    d2=(*od2)[i0];
    nx=(*o12)[i0].x;
    ny=(*o12)[i0].y;
    nz=(*o12)[i0].z;
    m1=2*(*o1)[i0];
    m2=2*(*o2)[i0];
    i1=3*sort_p[m1];
    i2=3*sort_p[m1+1];
    i3=3*sort_p[m2];
    i4=3*sort_p[m2+1];
    Jcoll[i*(3*Pcoll)+i1]=(1.-d1)*nx;
    Jcoll[i*(3*Pcoll)+i1+1]=(1.-d1)*ny;
    Jcoll[i*(3*Pcoll)+i1+2]=(1.-d1)*nz;
    Jcoll[i*(3*Pcoll)+i2]=d1*nx;
    Jcoll[i*(3*Pcoll)+i2+1]=d1*ny;
    Jcoll[i*(3*Pcoll)+i2+2]=d1*nz;
    Jcoll[i*(3*Pcoll)+i3]=(d2-1.)*nx;
    Jcoll[i*(3*Pcoll)+i3+1]=(d2-1.)*ny;
    Jcoll[i*(3*Pcoll)+i3+2]=(d2-1.)*nz;
    Jcoll[i*(3*Pcoll)+i4]=-d2*nx;
    Jcoll[i*(3*Pcoll)+i4+1]=-d2*ny;
    Jcoll[i*(3*Pcoll)+i4+2]=-d2*nz;
    Jcoll_nzero[12*i]=i*(3*Pcoll)+i1;
    Jcoll_nzero[12*i+1]=i*(3*Pcoll)+i1+1;
    Jcoll_nzero[12*i+2]=i*(3*Pcoll)+i1+2;
    Jcoll_nzero[12*i+3]=i*(3*Pcoll)+i2;
    Jcoll_nzero[12*i+4]=i*(3*Pcoll)+i2+1;
    Jcoll_nzero[12*i+5]=i*(3*Pcoll)+i2+2;
    Jcoll_nzero[12*i+6]=i*(3*Pcoll)+i3;
    Jcoll_nzero[12*i+7]=i*(3*Pcoll)+i3+1;
    Jcoll_nzero[12*i+8]=i*(3*Pcoll)+i3+2;
    Jcoll_nzero[12*i+9]=i*(3*Pcoll)+i4;
    Jcoll_nzero[12*i+10]=i*(3*Pcoll)+i4+1;
    Jcoll_nzero[12*i+11]=i*(3*Pcoll)+i4+2;
  }
  // build of J*J^T
  for(int g=0;g<l_graph;g++){
    // graph "g"
    i2=l_i_graph[g];
    for(int i=0;i<i2;i++){
      i0=i_graph[g*n_overlaps_per_graph+i];
      for(int j=i;j<i2;j++){
	i1=i_graph[g*n_overlaps_per_graph+j];
	d0=0.;
	for(int k=0;k<12;k++){
	  nb=Jcoll_nzero[12*i0+k]-i0*3*Pcoll;
	  d0+=Jcoll[i0*3*Pcoll+nb]*Jcoll[i1*3*Pcoll+nb];
	}
	JJT[i0*Rcoll+i1]=d0;
	JJT[i1*Rcoll+i0]=d0;
      }
      // build of L
      fcoll[i0]=fmax(0.,Bcoll[i0]/JJT[i0*Rcoll+i0]);
    }
    // Successive-Over-Relaxation (J*J^T*L=B with L>=0) and loop over collisions graphs
    i_sor=0;
    while(i_sor<100){
      for(int i=0;i<i2;i++){
	i0=i_graph[g*n_overlaps_per_graph+i];
	d0=0.;
	for(int j=0;j<i;j++){
	  i1=i_graph[g*n_overlaps_per_graph+j];
	  d0+=JJT[i0*Rcoll+i1]*Fcoll[i1];
	}
	d1=0.;
	for(int j=(i+1);j<i2;j++){
	  i1=i_graph[g*n_overlaps_per_graph+j];
	  d1+=JJT[i0*Rcoll+i1]*fcoll[i1];
	}
	Fcoll[i0]=fmax(0.,(1.-w_sor)*fcoll[i0]+w_sor*(Bcoll[i0]-d0-d1)/JJT[i0*Rcoll+i0]);
      }
      // stop condition
      refus=false;
      for(int i=0;i<i2;i++){
	i0=i_graph[g*n_overlaps_per_graph+i];
	z1=fcoll[i0];
	z2=Fcoll[i0];
	refus=(refus || (fabs(z2-z1)>(1e-06+1e-04*fabs(z1))));
	fcoll[i0]=z2;
      }
      i_sor++;
      if(!refus) break;
    }//elihw
    max_i_sor=max(max_i_sor,i_sor);
  }// Rof "g"
  // add the forces : J^T*lambda
  for(int i=0;i<n_overlaps;i++){
    d0=Fcoll[i];
    for(int j=0;j<12;j++){
      i0=Jcoll_nzero[12*i+j];
      JTl[i0-i*3*Pcoll]+=Jcoll[i0]*d0;
    }
  }
  // change the velocities
#pragma omp parallel for private(i0,i1)
  for(int i=0;i<Pcoll;i++){
    i0=p_sort[i];
    i1=(i0-i0%2)/2;
    (*v0)[i1]=add3((*v0)[i1],vec3(JTl[3*i],JTl[3*i+1],JTl[3*i+2]));
  }
  // U>0 test
  if(test){
    for(int i=0;i<n_overlaps;i++){
      i0=l_overlaps[i];
      i1=(*o1)[i0];
      i2=(*o2)[i0];
      d1=(*od1)[i0];
      d2=(*od2)[i0];
      U=dot_product((*o12)[i0],sub3(add3(mult3(1.-d1,(*v0)[i1]),mult3(d1,(*v0)[b1[i1]])),add3(mult3(1.-d2,(*v0)[i2]),mult3(d2,(*v0)[b1[i2]]))));
      if(U<0.){
	printf("----------------- %i %i : %f(%f), %i, %i overlap(s) %i graph(s)\n",i1,i2,U,Bcoll[i],max_i_sor,n_overlaps,l_graph);
	for(int g=0;g<l_graph;g++){
	  printf("\n%i\n",l_i_graph[g]);
	  for(int j=0;j<l_i_graph[g];j++){
	    i0=i_graph[g*n_overlaps_per_graph+j];
	    i0=l_overlaps[i0];
	    i1=(*o1)[i0];
	    i2=(*o2)[i0];
	    printf("(U<0) %i : %i(%i %i : %f) %i(%i %i : %f)\n",g,i1,p_graph[2*i1],p_graph[2*i1+1],(*od1)[i0],i2,p_graph[2*i2],p_graph[2*i2+1],(*od2)[i0]);
	  }
	}
	exit(-1);
      }
    }
  }
  // cleanup
  delete[] deja_vu;
  delete[] i_graph;
  delete[] l_i_graph;
  delete[] l_overlaps;
  delete[] p_sort;
  delete[] sort_p;
  delete[] Bcoll;
  delete[] fcoll;
  delete[] Fcoll;
  delete[] Jcoll;
  delete[] JTl;
  delete[] Jcoll_nzero;
  delete[] JJT;
  delete[] p_graph;
}

/*! @brief return 1 if a crossing happened during the last time-step, 0 otherwize.
  @param n_overlaps_previous number of overlap(s) from the previous step
  @param n_overlaps_current number of overlap(s) from the current step
  @param o1 index of the bond 1 (previous step)
  @param o2 index of the bond 2 (previous step)
  @param o12 minimal distance vector (previous step)
  @param O1 index of the bond 1 (current step)
  @param O2 index of the bond 2 (current step)
  @param O12 minimal distance vector (current step)
 */
int number_of_crossings(int n_overlaps_previous,int **o1,int **o2,vec3 **o12,int n_overlaps_current,int **O1,int **O2,vec3 **O12){
  bool is_openmp=false;
#ifdef _OPENMP
  is_openmp=true;
#endif
  int i1,i2,n_crossings=0;
  vec3 o12_i;
  // double loop over previous and current overlap(s)
  for(int i=0;i<n_overlaps_previous;i++){
    i1=(*o1)[i];
    i2=(*o2)[i];
    o12_i=(*o12)[i];
    if(n_overlaps_current<32 || !is_openmp){
      for(int j=0;j<n_overlaps_current;j++){
	if(i1==(*O1)[j] && i2==(*O2)[j]){
	  if(dot_product(o12_i,(*O12)[j])<0.) return 1;
	}
      }
    }else{
#pragma omp parallel for
      for(int j=0;j<n_overlaps_current;j++){
	if(i1==(*O1)[j] && i2==(*O2)[j]) n_crossings+=(dot_product(o12_i,(*O12)[j])<0.);
      }
      if(n_crossings>0) return 1;
    }
  }
  return 0;
}

/*! @brief return the square root of the quadratic average bond size.
  @param B number of bonds
  @param L size of the box
  @param b0 position of each bead
  @param b1 connectivity
*/
double average_bond_size(int B,double L,vec3 **b0,const int *b1){
  double average_distance2=0.,distance;
  int b1b;
  for(int b=0;b<(B-1);b++){
    b1b=b1[b];
    // connectivity
    if(b<b1b){
      distance=distance_with_PBC((*b0)[b],(*b0)[b1b],L);
      average_distance2+=distance*distance;
    }
  }
  return sqrt(average_distance2/(double)B);
}
