CXX=g++
SRC_DIR=${PWD}
# CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s
# LDFLAGS=
CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s -fopenmp
LDFLAGS=-fopenmp
SRC_CPP=velocities_resolve_EVC.cpp
OBJ=$(SRC_CPP:.cpp=.o) functions.o
EXECUTABLE=$(SRC_CPP:.cpp=)

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJ)
	$(CXX) -no-pie -pg -o $(EXECUTABLE) $(OBJ) $(LDFLAGS)

functions.o: $(SRC_DIR)/functions.cpp $(SRC_DIR)/functions.h
	$(CXX) -pg -o functions.o -c $(SRC_DIR)/functions.cpp $(CXXFLAGS)

$(SRC_CPP:.cpp=.o): ${SRC_DIR}/$(SRC_CPP) $(SRC_DIR)/functions.cpp $(SRC_DIR)/functions.h
	$(CXX) -pg -o $(SRC_CPP:.cpp=.o) -c $(SRC_CPP) $(CXXFLAGS)

mrproper:
	clear
	rm -f *~
	sleep 1
	rm -f *.o
	sleep 1
	rm -f *.out
	sleep 1
	rm -f $(EXECUTABLE)
	sleep 1

# CXX=g++
# SRC_DIR=${PWD}
# # CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s
# CXXFLAGS=-Wextra -Wall -pedantic -g -std=c++0x -O3 -s -fopenmp
# # CXXFLAGS=-Wextra -Wall -pedantic -g -O3 -s -mtune=native -fopenmp
# LDFLAGS=-fopenmp
# COMMON_SOURCES=functions.cpp
# COMMON_OBJECTS=${COMMON_SOURCES:.cpp=.o}
# SOURCES0=velocities_resolve_EVC.cpp
# OBJECTS0=$(SOURCES0:.cpp=.o)
# SOURCES1=pvelocities_resolve_EVC.cpp
# OBJECTS1=$(SOURCES1:.cpp=.o)
# EXECUTABLE0=$(SOURCES0:.cpp=)
# EXECUTABLE1=$(SOURCES1:.cpp=)

# .PHONY: all test0 test1

# all: test0 test1

# test0: $(EXECUTABLE0)

# test1: $(EXECUTABLE1)

# $(EXECUTABLE0): $(COMMON_OBJECTS) $(OBJECTS0)
# 	$(CXX) $(LDFLAGS) $^ -o $@

# $(EXECUTABLE1): $(COMMON_OBJECTS) $(OBJECTS1)
# 	$(CXX) $(LDFLAGS) $^ -o $@

# .cpp .o:
# 	$(CXX) $(CXXFLAGS) $< -pg -o $@

# mrproper:
# 	clear;
# 	rm -f *~;
# 	sleep 1;
# 	rm -f *.o;
# 	sleep 1;
# 	rm -f *.out;
# 	sleep 1;
# 	rm -f $(EXECUTABLE0) $(EXECUTABLE1);
# 	sleep 1;
